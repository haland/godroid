package uis.bachelor.android.menu_settings.model.interfaces;
/**
 * Interface used to during settings sending.
 * @author Eirik Heskja
 *
 */
public interface CalibrationCompleteInterface {
	/**
	 * Will tell any listening object that calibration of the NXT is complete.
	 */
	public void onCompleteCalibration();

}
