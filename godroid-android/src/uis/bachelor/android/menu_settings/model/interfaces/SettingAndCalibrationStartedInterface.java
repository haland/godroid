package uis.bachelor.android.menu_settings.model.interfaces;

/**
 * Interface used during settings sending.
 * @author Eirik Heskja
 *
 */
public interface SettingAndCalibrationStartedInterface {
	
	/**
	 * Will be used to notify listening objects that setup has started.
	 */
	public void onSetupStarted();

}
