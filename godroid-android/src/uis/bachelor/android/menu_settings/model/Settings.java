package uis.bachelor.android.menu_settings.model;

import standard.Values;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import array.SearchArray;

/**
 * Defines methods used for settings
 * @author Chris H�land
 */

public class Settings extends SearchArray {
	/**
	 * Illegal value returned if sensor port or motor port is not found
	 */
	private static final int ILLEGAL = -1;
	
	
	/**
	 * The Android value for motor port A
	 */
	private static final String PORT_A = "motorA";
	
	/**
	 * The Android value for motor port B
	 */
	private static final String PORT_B = "motorB";
	
	/**
	 * The Android value for motor port C
	 */
	private static final String PORT_C = "motorC";
	

	/**
	 * The Android value for sensor port 1
	 */
	private static final String PORT_1 = "sensor1";
	
	/**
	 * The Android value for sensor port 2
	 */
	private static final String PORT_2 = "sensor2";
	
	/**
	 * The Android value for sensor port 3
	 */
	private static final String PORT_3 = "sensor3";
	
	/**
	 * The Android value for sensor port 4
	 */
	private static final String PORT_4 = "sensor4";
	
	
	/**
	 * The programs shared preferences
	 */
	private SharedPreferences preferences;

	/**
	 * Creates a setting
	 * @param context - The context
	 */
	public Settings(Context context){
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	/**
	 * Gets a setting from the shared preference of the Android program
	 * @param key - The key used searching for the settings value 
	 * @param defaultValue - The default value to return, if key is not found
	 * @return The settings value
	 */
	public Object getSetting(String key, Object defaultValue) {
		if (defaultValue instanceof Integer)
			return preferences.getInt(key, (Integer) defaultValue);
		if (defaultValue instanceof Boolean)
			return preferences.getBoolean(key, (Boolean) defaultValue);
		if (defaultValue instanceof String)
			return preferences.getString(key, (String) defaultValue);
		return null;
	}
	
	/**
	 * Converts the Android motor port value into the motor port value used by the NXT
	 * @param MOTOR_PORT - The Android motor port value
	 * @return The NXT motor port value
	 */
	public int convertMotorPortToInteger(String MOTOR_PORT){
		if (MOTOR_PORT.equals(PORT_A))
			return Values.MOTOR_PORT_A;
		if (MOTOR_PORT.equals(PORT_B))
			return Values.MOTOR_PORT_B;
		if (MOTOR_PORT.equals(PORT_C))
			return Values.MOTOR_PORT_C;
		return ILLEGAL;
	}

	/**
	 * Converts the Android sensor port value into the sensor port value used by the NXT 
	 * @param SENSOR_PORT - The Android sensor port value
	 * @return The NXT sensor port value
	 */
	public int convertSensorPortToInteger(String SENSOR_PORT){
		if (SENSOR_PORT.equals(PORT_1))
			return Values.SENSOR_PORT_1;
		if (SENSOR_PORT.equals(PORT_2))
			return Values.SENSOR_PORT_2;
		if (SENSOR_PORT.equals(PORT_3))
			return Values.SENSOR_PORT_3;
		if (SENSOR_PORT.equals(PORT_4))
			return Values.SENSOR_PORT_4;
		return ILLEGAL;
	}
	
	/**
	 * Converts a boolean variable into an integer variable
	 * @param variable - The boolean variable
	 * @return The integer representative value
	 */
	public int convertBooleanToInteger(boolean variable){
		return (variable) ? Values.BOOLEAN_TRUE : Values.BOOLEAN_FALSE;
	}
}