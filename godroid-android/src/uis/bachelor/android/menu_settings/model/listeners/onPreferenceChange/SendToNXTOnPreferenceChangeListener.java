package uis.bachelor.android.menu_settings.model.listeners.onPreferenceChange;

import uis.bachelor.android.menu_settings.model.SettingsSender;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

/**
 * Class used to send settings to the NXT.
 * @author Eirik Heskja
 *
 */
public class SendToNXTOnPreferenceChangeListener implements OnSharedPreferenceChangeListener {
	
	/**
	 * The {@link SettingsSender} object used to send objects.
	 */
	private SettingsSender settings;
	
	/**
	 * Boolean varaible indicating that the changed settings is a NXT setting.
	 */
	private boolean send;
	
	/**
	 * Default constructor. Instantiates the settings object.
	 * @param context The context holding the settings.
	 */
	public SendToNXTOnPreferenceChangeListener(Context context) {
		settings = SettingsSender.getInstance(context);
	}

	/**
	 * Will be executed when a {@link SharedPreferences} has changed.
	 */
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (isNXTSetting(key))
			send = true;
		else
			send = false;
	}

	/**
	 * Checks if the key is a NXT setting.
	 * @param key The key to check
	 * @return return true if the key is defined as a nxt setting.
	 */
	private boolean isNXTSetting(String key) {
		return key.equals("left") || key.equals("center") || key.equals("right") || 
				key.equals("centerMotorDir") || key.equals("leftMotorDir") || 
				key.equals("rightMotorDir") || key.equals("gyroPort") || 
				key.equals("accelPort") || key.equals("detectionPort") || 
				key.equals("degToZero") || key.equals("zeroToSegway") || 
				key.equals("segwayZero") || key.equals("segwayToNormal");
	}

	/**
	 * Will send the settings.
	 */
	public void send() {
		if (send)
			settings.send();
	}

}
