package uis.bachelor.android.menu_settings.model.listeners.onClick;

import uis.bachelor.android.R;
import uis.bachelor.android.common.LanguageActivity;
import uis.bachelor.android.common.views.CustomDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;

/**
 * Class used to clear default preferences.
 * @author Eirik Heskja
 *
 */
public class DefaultPreferenceOnClickListener implements OnPreferenceClickListener {
	/**
	 * The dialog to show if the user clicks on "clear defaults" in the settings screen.
	 */
	private CustomDialog dialog;

	/**
	 * Default constructor. Will instantiate the {@link CustomDialog}.
	 * @param activity The activity holding the dialog
	 * @param listener The listener that caused the event.
	 */
	public DefaultPreferenceOnClickListener(final Activity activity, final OnSharedPreferenceChangeListener listener) {
		dialog = new CustomDialog(activity, activity.getString(R.string.sure), activity.getString(R.string.delete));
		dialog.setPositiveButton(activity.getString(R.string.yes), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences manager = PreferenceManager.getDefaultSharedPreferences(activity);
				manager.unregisterOnSharedPreferenceChangeListener(listener);
				boolean remember = manager.getBoolean("remember", true);
				boolean rememberDefault = manager.getBoolean("rememberDefault", true);
				
				String lang = manager.getString(LanguageActivity.lang_key, "English");
				boolean defaultLang = manager.getBoolean("default_language", false);
				
				manager.edit().clear().commit();
				PreferenceManager.setDefaultValues(activity, R.xml.preference, true);
				if (remember)
					manager.edit().putString(LanguageActivity.lang_key, lang).commit();
				if (remember && rememberDefault)
					manager.edit().putBoolean("default_language", defaultLang).commit();
				
				DefaultPreferenceOnClickListener.this.dialog.dismiss();
				activity.finish();
				manager.registerOnSharedPreferenceChangeListener(listener);
			}
		});
		dialog.setNegativeButton(activity.getString(R.string.no), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				DefaultPreferenceOnClickListener.this.dialog.dismiss();
			}
		});
	}

	/**
	 * Will show the dialog on click.
	 */
	@Override
	public boolean onPreferenceClick(Preference preference) {
		dialog.show();
		return false;
	}

}
