package uis.bachelor.android.menu_settings.model.partial;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;
import android.content.Context;

/**
 * Settings for the degree motor
 * @author Chris H�land
 */

public class DegreeMotorSettings extends Settings {
	/**
	 * String used for getting the degree motor port setting on the Android 
	 */
	public static final String DEGREE_MOTOR_PORT = "center";
	
	/**
	 * The default value for the degree motor port
	 */
	public static final String DEGREE_MOTOR_DEFAULT_VALUE_PORT = "motorB";

	
	/**
	 * String used for getting the degree motors direction
	 */
	public static final String DEGREE_MOTOR_DIRECTION = "centerMotorDir";
	
	/**
	 * The default value for the degree motor direction
	 */
	public static final boolean DEGREE_MOTOR_DEFAULT_VALUE_DIRECTION = true;
	
	
	/**
	 * String used for getting the degree motors degrees to move from calibration zero to zero
	 */
	public static final String DEGREE_MOTOR_CALIBRATION_ZERO_TO_ZERO = "degToZero";
	
	/**
	 * The default value for degree motors degrees to move from calibration zero to zero
	 */
	public static final String DEGREE_MOTOR_DEFAULT_VALUE_CALIBRATION_ZERO_TO_ZERO = "0";
	
	/**
	 * Creates the degree motor settings
	 * @param context - The context
	 */
	public DegreeMotorSettings(Context context) {
		super(context);

		int port = convertMotorPortToInteger((String) 
				getSetting(DEGREE_MOTOR_PORT, DEGREE_MOTOR_DEFAULT_VALUE_PORT));
		int direction = convertBooleanToInteger((Boolean)
				getSetting(DEGREE_MOTOR_DIRECTION, DEGREE_MOTOR_DEFAULT_VALUE_DIRECTION));
		int calibrateZeroToZero = Integer.parseInt((String)
				getSetting(DEGREE_MOTOR_CALIBRATION_ZERO_TO_ZERO, DEGREE_MOTOR_DEFAULT_VALUE_CALIBRATION_ZERO_TO_ZERO));
		
		set(Parameters.TYPE, Values.SETTING_MIDDLE_MOTOR);
		set(Parameters.MOTOR_PORT, port);
		set(Parameters.MOTOR_DIRECTION, direction);
		set(Parameters.TRANSFORMATION_ZERO_DEGREES, calibrateZeroToZero);
	}
}