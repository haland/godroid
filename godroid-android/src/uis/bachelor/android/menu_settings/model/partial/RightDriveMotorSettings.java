package uis.bachelor.android.menu_settings.model.partial;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;
import android.content.Context;

/**
 * Settings for the right motor
 * @author Chris H�land
 */

public class RightDriveMotorSettings extends Settings {
	/**
	 * String used for getting the right motor port setting on the Android
	 */
	public static final String RIGHT_DRIVE_MOTOR_PORT = "right";
	
	/**
	 * The default value for the right motor port
	 */
	public static final String RIGHT_DRIVE_MOTOR_DEFAULT_VALUE_PORT = "motorC";

	
	/**
	 * String used for getting the right motors direction
	 */
	public static final String RIGHT_DRIVE_MOTOR_DIRECTION = "rightMotorDir";
	
	/**
	 * The default value for the right motor direction
	 */
	public static final boolean RIGHT_DRIVE_MOTOR_DEFAULT_VALUE_DIRECTION = true;
	
	
	/**
	 * Creates the right motor settings
	 * @param context - The context
	 */
	public RightDriveMotorSettings(Context context) {
		super(context);

		int port = convertMotorPortToInteger((String) 
				getSetting(RIGHT_DRIVE_MOTOR_PORT, RIGHT_DRIVE_MOTOR_DEFAULT_VALUE_PORT));
		int direction = convertBooleanToInteger((Boolean)
				getSetting(RIGHT_DRIVE_MOTOR_DIRECTION, RIGHT_DRIVE_MOTOR_DEFAULT_VALUE_DIRECTION));
		
		set(Parameters.TYPE, Values.SETTING_MOTOR_RIGHT);
		set(Parameters.MOTOR_PORT, port);
		set(Parameters.MOTOR_DIRECTION, direction);
	}
}