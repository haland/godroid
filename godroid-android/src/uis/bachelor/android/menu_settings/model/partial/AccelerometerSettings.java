package uis.bachelor.android.menu_settings.model.partial;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;
import android.content.Context;

/**
 * Settings for the accelerometer
 * @author Chris H�land
 */

public class AccelerometerSettings extends Settings {
	/**
	 * String used for getting the accelerometer port setting on the Android
	 */
	public static final String ACCELEROMETER_PORT = "accelPort";
	
	/**
	 * The default value for the accelerometer port
	 */
	public static final String DEFAULT_VALUE = "sensor1";
	
	/**
	 * Creates the accelerometer settings
	 * @param context - The context
	 */
	public AccelerometerSettings(Context context) {
		super(context);
		
		int port = convertSensorPortToInteger((String) 
				getSetting(ACCELEROMETER_PORT, DEFAULT_VALUE));
		
		set(Parameters.TYPE, Values.SETTING_ACCELEROMETER);
		set(Parameters.ACCELEROMETER_PORT, port);
	}
}
