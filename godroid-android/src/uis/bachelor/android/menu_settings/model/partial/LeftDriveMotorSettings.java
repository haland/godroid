package uis.bachelor.android.menu_settings.model.partial;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;
import android.content.Context;

/**
 * Settings for the left motor
 * @author Chris H�land
 */

public class LeftDriveMotorSettings extends Settings {
	/**
	 * String used for getting the left motor port setting on the Android 
	 */
	public static final String LEFT_DRIVE_MOTOR_PORT = "left";
	
	/**
	 * The default value for the left motor port
	 */
	public static final String LEFT_DRIVE_MOTOR_DEFAULT_VALUE_PORT = "motorA";

	
	/**
	 * String used for getting the left motors direction
	 */
	public static final String LEFT_DRIVE_MOTOR_DIRECTION = "leftMotorDir";
	
	/**
	 * The default value for the left motor direction
	 */
	public static final boolean LEFT_DRIVE_MOTOR_DEFAULT_VALUE_DIRECTION = true;
	
	/**
	 * Creates the left motor settings
	 * @param context - The context
	 */
	public LeftDriveMotorSettings(Context context) {
		super(context);
		
		int port = convertMotorPortToInteger((String) 
				getSetting(LEFT_DRIVE_MOTOR_PORT, LEFT_DRIVE_MOTOR_DEFAULT_VALUE_PORT));
		int direction = convertBooleanToInteger((Boolean)
				getSetting(LEFT_DRIVE_MOTOR_DIRECTION, LEFT_DRIVE_MOTOR_DEFAULT_VALUE_DIRECTION));
		
		set(Parameters.TYPE, Values.SETTING_MOTOR_LEFT);
		set(Parameters.MOTOR_PORT, port);
		set(Parameters.MOTOR_DIRECTION, direction);
	}
}
