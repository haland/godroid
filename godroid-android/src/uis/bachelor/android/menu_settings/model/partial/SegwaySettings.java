package uis.bachelor.android.menu_settings.model.partial;

import android.content.Context;
import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;

/**
 * Settings for Segway
 * @author Chris H�land
 */

public class SegwaySettings extends Settings {
	/**
	 * String used for getting the gyroscope fall value on the Android
	 */
	private static final String GYROSCOPE_FALL_VALUE = "gyroFallValue";
	
	/**
	 * The default gyroscope fall value
	 */
	private static final String GYROSCOPE_FALL_DEFAULT_VALUE = "-20";
	
	private static final String ACCELEROMETER_REACH_VALUE = "accelerometerReachValue";
	
	private static final String ACCELEROMETER_REACH_DEFAULT_VALUE = "86";
	
	/**
	 * Creates the segway settings 
	 * @param context - The context
	 */
	public SegwaySettings(Context context) {
		super(context);
		
		int fallValue = Integer.valueOf((String)getSetting(GYROSCOPE_FALL_VALUE, GYROSCOPE_FALL_DEFAULT_VALUE));
		int reachValue = Integer.valueOf((String)getSetting(ACCELEROMETER_REACH_VALUE, ACCELEROMETER_REACH_DEFAULT_VALUE));
		
		set(Parameters.TYPE, Values.SETTING_SEGWAY);
		set(Parameters.SEGWAY_GYROSCOPE_FALL_VALUE, fallValue);
		set(Parameters.SEGWAY_ACCELEROMETER_REACH_VALUE, reachValue);
	}
}
