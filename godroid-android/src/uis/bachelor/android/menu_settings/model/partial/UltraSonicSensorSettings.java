package uis.bachelor.android.menu_settings.model.partial;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;
import android.content.Context;

/**
 * Settings for the ultra sonic sensor
 * @author Chris H�land
 */

public class UltraSonicSensorSettings extends Settings {
	/**
	 * String used for getting the ultra sonic sensor port on the Android
	 */
	public static final String ULTRA_SONIC_SENSOR_PORT = "detectionPort";
	
	/**
	 * The default value for the ultra sonic sensor port
	 */
	public static final String DEFAULT_VALUE = "sensor2";

	/**
	 * Creates the ultra sonic sensor settings
	 * @param context - The context
	 */
	public UltraSonicSensorSettings(Context context) {
		super(context);

		int port = convertSensorPortToInteger((String) 
				getSetting(ULTRA_SONIC_SENSOR_PORT, DEFAULT_VALUE));
		
		set(Parameters.TYPE, Values.SETTING_ULTRA_SONIC_SENSOR);
		set(Parameters.DETECTION_PORT, port);
	}
}