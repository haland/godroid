package uis.bachelor.android.menu_settings.model.partial;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.menu_settings.model.Settings;
import android.content.Context;

/**
 * Settings for the gyroscope
 * @author Chris H�land
 */

public class GyroscopeSettings extends Settings {
	/**
	 * String used for getting the gyroscope port setting on the Android 
	 */
	public static final String GYROSCOPE_PORT = "gyroPort";
	
	/**
	 * The default value for the gyroscope port
	 */
	public static final String DEFAULT_VALUE = "sensor3";
	
	/**
	 * Creates the gyroscope settings
	 * @param context - The context
	 */
	public GyroscopeSettings(Context context) {
		super(context);
		
		int port = convertSensorPortToInteger((String) 
				getSetting(GYROSCOPE_PORT, DEFAULT_VALUE));
		
		set(Parameters.TYPE, Values.SETTING_GYROSCOPE);
		set(Parameters.GYRO_PORT, port);
	}
}
