package uis.bachelor.android.menu_settings.model;

import java.util.ArrayList;

import standard.Flags;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.menu_settings.model.partial.AccelerometerSettings;
import uis.bachelor.android.menu_settings.model.partial.DegreeMotorSettings;
import uis.bachelor.android.menu_settings.model.partial.GyroscopeSettings;
import uis.bachelor.android.menu_settings.model.partial.LeftDriveMotorSettings;
import uis.bachelor.android.menu_settings.model.partial.RightDriveMotorSettings;
import uis.bachelor.android.menu_settings.model.partial.SegwaySettings;
import uis.bachelor.android.menu_settings.model.partial.UltraSonicSensorSettings;
import android.content.Context;
import array.SearchArray;
import array.SearchArrayDecoder;

/**
 * The object that sends settings
 * @author Chris H�land
 */

public class SettingsSender {
	/**
	 * The instance of the settings sender
	 */
	private static SettingsSender instance;

	
	/**
	 * The context
	 */
	private Context context;
	
	/**
	 * The object that sends messages over bluetooth to the NXT
	 */
	private MessageHandler handler;
	
	/**
	 * List of all settings
	 */
	private ArrayList<Settings> settings;
	
	
	/**
	 * Creates the settings sender
	 * @param context - The context
	 */
	private SettingsSender(Context context) {
		this.context = context;
		this.handler = MessageHandler.getInstance();
	}
	
	/**
	 * Sends all the settings to the NXT
	 */
	public void send(){
		if (hasNXTConnected()) {
			prepareSendSettings();		
			
			settings = new ArrayList<Settings>();
			settings.add(new AccelerometerSettings(context));
			settings.add(new DegreeMotorSettings(context));
			settings.add(new GyroscopeSettings(context));
			settings.add(new LeftDriveMotorSettings(context));
			settings.add(new RightDriveMotorSettings(context));
			settings.add(new UltraSonicSensorSettings(context));
			settings.add(new SegwaySettings(context));
			
			for (Settings setting : settings)
				sendSetting(Flags.MSG_SETTINGS, setting);
			sendSetting(Flags.MSG_SETTINGS_COMPLETE, null);
		}
	}
	
	/**
	 * Whether a NXT is connected to the program
	 * @return Whether a NXT is connected to the program
	 */
	private boolean hasNXTConnected() {
		return handler.isConnected();
	}
	
	/**
	 * Prepares the message handler for sending settings
	 */
	private void prepareSendSettings() {
		handler.clearQue();
		handler.setIncommingMessage(SearchArrayDecoder.encode(Flags.MSG_SENDING_SETTINGS, null));
	}

	/**
	 * Sends a setting
	 * @param flag - The settings flag
	 * @param array - The settings
	 */
	private void sendSetting(int flag, SearchArray array) {
		handler.putMessage(flag, array);
	}
	
	/**
	 * Creates a instance of the settings sender
	 * @param context - The context
	 * @return The instance
	 */
	public static SettingsSender getInstance(Context context){
		if (instance == null)
			instance = new SettingsSender(context);
		return instance;
	}
}