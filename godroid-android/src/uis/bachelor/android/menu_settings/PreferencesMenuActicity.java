package uis.bachelor.android.menu_settings;

import uis.bachelor.android.R;
import uis.bachelor.android.menu_settings.model.listeners.onClick.DefaultPreferenceOnClickListener;
import uis.bachelor.android.menu_settings.model.listeners.onPreferenceChange.SendToNXTOnPreferenceChangeListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.view.View.OnClickListener;

/**
 * 
 * Activity to display Android default Preferences.
 * @author Eirik Heskja
 *
 */
public class PreferencesMenuActicity extends PreferenceActivity {
	
	/**
	 * Listener that will start sending settings if they was related to the NXT settings.
	 */
	private SendToNXTOnPreferenceChangeListener settingsSender;
	
	/**
	 *  Called when the activity is first created.
	 * 	Will remove "lang" preference add register the {@link SendToNXTOnPreferenceChangeListener} as a listener to {@link SharedPreferences} changes.
	 *	Will also add the right {@link OnClickListener} to the "Reset to default" preference. 
	 */
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    addPreferencesFromResource(R.xml.preference);
	    
	    settingsSender = new SendToNXTOnPreferenceChangeListener(this);
	    
	    PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(settingsSender);
	    PreferenceGroup screen = (PreferenceGroup) findPreference("android");
	    Preference preference = findPreference("lang");
	    screen.removePreference(preference);
	    
	    preference = findPreference("default_key");
	    preference.setOnPreferenceClickListener(new DefaultPreferenceOnClickListener(this, settingsSender));
	    
	    screen = (PreferenceGroup) findPreference("uis");
	    preference = findPreference("remember");
	    screen.removePreference(preference);
	}
	
	/**
	 * 
	 * Will run when the Preference screen is closed. Will cause the {@link SendToNXTOnPreferenceChangeListener} to send the settings.
	 * Will also unregister the {@link SendToNXTOnPreferenceChangeListener} form the {@link SharedPreferences}.
	 */
	@Override
	protected void onDestroy() {
		settingsSender.send();
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(settingsSender);
		super.onDestroy();
	}
}