package uis.bachelor.android.menu_controller;

import standard.Parameters;
import standard.Values;
import uis.bachelor.android.R;
import uis.bachelor.android.common.HandlerActivity;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.common.model.communication.interfaces.BluetoothDisconnectMessageInterface;
import uis.bachelor.android.menu_controller.model.JoystickViewOutput;
import uis.bachelor.android.menu_controller.model.RemoteControllFling;
import uis.bachelor.android.menu_controller.model.interfaces.FlingInterface;
import uis.bachelor.android.menu_controller.model.listeners.onClick.SegwayToggleOnClickListener;
import uis.bachelor.android.menu_controller.views.GestureView;
import uis.bachelor.android.menu_controller.views.ControllerLayout;
import uis.bachelor.android.menu_controller.views.joysticks.StickController;
import uis.bachelor.android.menu_controller.views.joysticks.accelerometer.AccelerometerJoystick;
import uis.bachelor.android.menu_controller.views.joysticks.touch.MultitouchJoystickView;
import uis.bachelor.android.menu_controller.views.joysticks.touch.SingletouchJoystickView;
import android.R.string;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import array.SearchArray;
/**
 * 
 * Activity used to control a NXT device.
 * Extends {@link HandlerActivity} so it can respond to different events, such as disconnect. Read more about responding to event in the {@link HandlerActivity} class.
 * @author Eirik Heskja
 *
 */
public class ControllerActivity extends HandlerActivity implements FlingInterface, BluetoothDisconnectMessageInterface {
	
	/**
	 * 
	 * The {@link String}s that holds information about warnings, such as distance from wall, battery and signalstrength. 
	 */
	private String closeString, batteryString, signalString, warning;

	/**
	 * 
	 * Boolean values that will be set to true if any of the warnings should be shown.
	 */
	private boolean close, battery, signal;

	/**
	 * Whether the robot is transforming
	 */
	private boolean transforming;

	/**
	 * 
	 */
	private Menu menu;
	
	/**
	 * 
	 * Boolean values that defines whether or not snapBack should be enabled, and if you want the infoText about speed and angle.
	 */
	private boolean snapBack;

	/**
	 * The current touch controller used. Either multitouch or singletouch
	 */
	private StickController touchcontroller;
	
	/**
	 * 
	 * The {@link AccelerometerJoystick} object. Will be added to a {@link ViewFlipper} object, to allow switching between remote controls.
	 */
	private AccelerometerJoystick accel;

	/**
	 * 
	 * Listener that will send messages to the NXT when you want to transform between Segway and normal mode.
	 */
	private SegwayToggleOnClickListener toggleListener;

	/**
	 * 
	 * The {@link FlingInterface} that contains the {@link ViewFlipper} associated with the remote controls. Could be used to "fling" over the screen and change remotes with your fingers.
	 */
	private RemoteControllFling flinger;
	
	/**
	 * 
	 * The {@link JoystickViewOutput} object that will be used to send messages to the NXT when you want to drive or steer. 
	 */
	private JoystickViewOutput output;

	/**
	 * 
	 * The layout holding everything in place. 
	 */
	private RelativeLayout layout;

	private MenuItem item;

	/**
	 * 
	 * Will be run when the {@link Activity} starts.
	 * Will instantiate all remote controls and object associated with controlling the NXT.
	 * Will also show a warning if the device does not support the appropriate MultiTouch. 
	 * Will make sure that every remote is added to the {@link ViewFlipper} object and will start the {@link JoystickViewOutput} object.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.joysticklayout);
		snapBack = getPackageManager().hasSystemFeature(PackageManager.FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT);

		output = new JoystickViewOutput();
		close = battery = signal = false;

		accel = (AccelerometerJoystick) findViewById(R.id.accelView);
		accel.setParent(this); // NEEDS TO RUN!!

		if (snapBack)
			touchcontroller = (MultitouchJoystickView) findViewById(R.id.joystickView1);
		else touchcontroller = (SingletouchJoystickView) findViewById(R.id.joystickView2);
		
		touchcontroller.setParent(this); // NEEDS TO RUN!!
		
		toggleListener = new SegwayToggleOnClickListener(this);
				
		addListeners();
		enableFling();
		updateText();

		output.start();
	}

	/**
	 * 
	 * Will make sure that the {@link MessageHandler} will send events to this {@link Activity}.
	 */
	public void addListeners() {
		MessageHandler.getInstance().addSegwayListener(toggleListener);
		MessageHandler.getInstance().addDistanceListener(this);
		MessageHandler.getInstance().addDisconnectListener(this);
	}
	/**
	 * 
	 * Will be run when the user clicks on the phones menu button. Will inflate the {@link Menu} and set the appropriate icons to the {@link MenuItem}s.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.segwaymenu, menu);
		
		this.menu = menu;
		
		menu.getItem(0).setIcon(android.R.drawable.ic_menu_always_landscape_portrait);
		menu.getItem(1).setIcon(android.R.drawable.ic_media_ff);
		
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * 
	 * Will be run when the user clicks on a {@link MenuItem} from the {@link Menu}. 
	 * If you click on the "toggle" option, the {@link SegwayToggleOnClickListener} will execute its transform method.
	 * If you click on the "swipe" option, the {@link RemoteControllFling} will show the next remote in the que.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		this.item = item;
		switch(item.getItemId()){
		case R.id.toggle: 
			toggleListener.onClick(item);
			return true; 
		case R.id.swipe:
			flinger.next();
			return true;
		}
		return false;
	}

	/**
	 * 
	 * Will make sure that every {@link string} is in the correct language.
	 */
	@Override
	protected void updateUI(){
		closeString = getString(R.string.close);
		batteryString = getString(R.string.battery);
		signalString = getString(R.string.signal);
		warning = getString(R.string.warning);
	}

	/**
	 * 
	 * Will create a new {@link RemoteControllFling} object. The {@link MultitouchJoystickView} and {@link AccelerometerJoystick} will be removed from it's default layout and added to the {@link RemoteControllFling}-objects {@link ViewFlipper} object.
	 * The {@link ViewFlipper} object will then be added to the layout.
	 */
	private void enableFling() {
		flinger = new RemoteControllFling(this);

		layout = (RelativeLayout) findViewById(R.id.frame);

		layout.removeView(findViewById(R.id.joystickView1));
		layout.removeView(findViewById(R.id.joystickView2));
		layout.removeView(accel);

		flinger.addView(touchcontroller);
		flinger.addView(accel);

		flinger.addFlingListner(this);
		flinger.addFlingListner(accel);
		flinger.addFlingListner(output);

		flinger.addFlingListner(touchcontroller);

		layout.addView(flinger.getContainer());

		fling(touchcontroller);
	}

	/**
	 * Whether the robot is transforming
	 * @return Whether the robot is transforming
	 */
	public boolean isTransforming() {
		return transforming;
	}

	public void setTransforming(boolean transforming) {
		this.transforming = transforming;
	}

	/**
	 * 
	 * Makes sure to kill all running threads and destroy all objects that's not needed when the {@link Activity} closes.
	 */
	@Override
	protected void onDestroy() {
		touchcontroller.stop();
		output.stop();
		accel.stop();
		layout.removeView(flinger.getContainer());
		flinger.destroy();
		((ControllerLayout) layout).destroySources();
		System.gc();
		super.onDestroy();
	}

	/**
	 * 
	 * Will update the info text with the correct values for speed and angle.
	 */
	@Override
	protected void updateText() {
		
	}

	
	/**
	 * 
	 * Will make sure to display the correct warning messages when a warning is detected.
	 * Can display info about distance, batterylevel and signal strength. 
	 */
	@Override
	protected void updateWarning(){
		close = battery = signal = false; //Makes sure the warnings are not displayed yet. Need to add icons
		TextView text = (TextView) findViewById(R.id.warning);
		if (close || battery || signal){
			text.setVisibility(View.VISIBLE);
			text.setText(getWarningText());
		}
		else
			text.setVisibility(View.INVISIBLE);
	}

	/**
	 * 
	 * Will return the full warning text, accumulated if there is more than one warning.
	 * @return The {@link CharSequence} to display as a warning.
	 */
	private CharSequence getWarningText() {
		String warning = this.warning;
		if (battery)
			warning += "\n"+batteryString;
		if (signal)
			warning += "\n"+signalString;
		if (close)
			warning += "\n"+closeString;
		return warning;
	}

	/**
	 * 
	 * Method that will put the {@link Boolean} variable "close" to true / false, depending on the distance to any object.
	 */
	@Override
	public void tooClose(int size) {
		if (size == Values.BOOLEAN_TRUE)
			close = true;
		else
			close = false;
	}

	/**
	 * 
	 * Method that will put the {@link Boolean} variable "battery" to true / false, depending on the battery level.
	 */
	@Override
	protected void lowBattery(int battery) {
		if (battery <= Values.WARNING_BATTERY)
			this.battery = true;
		else
			this.battery = false;
	}

	/**
	 * 
	 * Method that will put the {@link Boolean} variable "signal" to true / false, depending on the signal strength.
	 */
	@Override
	protected void lowSignal(int signal) {
		if (signal <= Values.WARNING_SIGNAL)
			this.signal = true;
		else
			this.signal = false;
	}

	/**
	 * 
	 * Method that will be executed when the user switches between remote controls. Will make sure that the info text is visible at all time.
	 */
	@Override
	public void fling(GestureView view) {
		if (view instanceof AccelerometerJoystick) {
			if (menu != null) menu.getItem(1).setTitle(getResources().getString(R.string.swipetwo));
		} else {
			if (menu != null) menu.getItem(1).setTitle(getResources().getString(R.string.swipe)); 
		}
		view.addRemoteListener(output);
	}
	
	/**
	 * 
	 * Method that will be executed when disconnected from the NXT. Will make sure to dismiss any warning messages and display a information popup telling the user that connection has been lost.
	 */
	
	@Override
	public void disconnect() {
		dismissWarning();
		dismissTransformDialog();
		super.disconnect();
	}

	/**
	 * 
	 * Method for setting any {@link Boolean} variables concerning warnings to false. 
	 */
	private void dismissWarning() {
		close = battery = signal = false;
		updateWarningMessages();
	}

	@Override
	protected void updateStatus() {
	}

	@Override
	protected void onDisconnectDialogFinish() {
		finish();
	}

	@Override
	protected void updateMenu(SearchArray values) {
		if (item != null) {
			int mode = values.get(Parameters.TYPE);
			boolean down = (mode == Values.SEGWAY_DOWN);
			item.setTitle(!down ? R.string.normal : R.string.segway);
			item.setChecked(!down);
		}
	}
}
