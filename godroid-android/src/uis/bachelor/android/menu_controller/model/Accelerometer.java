package uis.bachelor.android.menu_controller.model;

import java.util.ArrayList;

import uis.bachelor.android.menu_controller.model.interfaces.AccelerometerInterface;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Update values from the built in accelerometer when the phone is tilted and notifies observers with data from the X and Y axis.
 * @author Anders Hole
 */
public class Accelerometer implements SensorEventListener {

	/**
	 * Variables containing accelerometer data for the various axis. X = x axis(positive left), Y = y = axis(positive up).
	 */
	private float x, y;
	/**
	 * Array containing accelerometer data.
	 */
	private float values[];

	private final SensorManager sensorManager;
	private final Sensor accel;
	private ArrayList<AccelerometerInterface> observers;
	
	/**
	 * Creates the object and listens for changes in the accelerometer(Tilting of the device).
	 * @param c - {@link Context} 
	 */
	public Accelerometer(Context c){
		sensorManager = (SensorManager) c.getSystemService(Activity.SENSOR_SERVICE);

		accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_GAME);
		observers = new ArrayList<AccelerometerInterface>();

		values = new float[3];
	}
	
	/**
	 * Unregister sensor listener.
	 */
	public void unregisterListener(){
		sensorManager.unregisterListener(this);
	}


	/**
	 * Notifies observers with new accelerometer values.
	 * @param x - Value x-axis
	 * @param y - Value y-axis
	 */
	private void notifyObservers(float x, float y){
		for (AccelerometerInterface observer : observers)
			observer.tiltChanged(x, y);
	}

	/**
	 * Adds accelerometer observer.
	 * @param observer - {@link AccelerometerObserver}
	 */
	public void addObserver(AccelerometerInterface observer) {
		observers.add(observer);		
	}

	/**
	 * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
	 */
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {
		calculateCirclePosition(event);
	}

	/**
	 * Calculates a percentage of the earth's gravity.
	 */
	public void calculateCirclePosition(SensorEvent event){
		values = event.values;
		x = (values[0]/SensorManager.GRAVITY_EARTH);
		y = (values[1]/SensorManager.GRAVITY_EARTH);
		notifyObservers(x, y);
	}
}