package uis.bachelor.android.menu_controller.model;

import java.util.ArrayList;

import uis.bachelor.android.menu_controller.model.interfaces.FlingInterface;
import uis.bachelor.android.menu_controller.views.GestureView;
import uis.bachelor.android.menu_main.MainMenuActivity;
import android.R;
import android.content.Context;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ViewFlipper;

/**
 * A class holding different remote controls and listeners.
 * @author Eirik Heskja
 *
 */
public class RemoteControllFling extends SimpleOnGestureListener {
	/**
	 * The {@link ViewFlipper} object holding all the view that can be flipper trough.
	 */
	private static ViewFlipper flipper;
	
	/**
	 * int values used to define direction
	 */
	private final static int LEFT = 0, RIGHT = 1, INVALID = -1;

	/**
	 * int values used to define speed and direction
	 */
	private int dir = INVALID, speed = INVALID;

	/**
	 * If speed is lower than this value, it will not be detected.
	 */
	private int speedMargin = 1000;
	
	/**
	 * Min speed required to move left or right
	 */
	private int minSpeed = 550;

	/**
	 * Array of listeners. Used to tell the system that the view has been changed.
	 */
	private ArrayList<FlingInterface> listeners;
	
	/**
	 * The context holding the {@link ViewFlipper}.
	 */
	private Context context;

	/**
	 * Default constructor. Creates the {@link ViewFlipper} and the {@link ArrayList}.
	 * @param context
	 */
	public RemoteControllFling(Context context) {
		this.context = context;
		if (flipper == null) flipper = new ViewFlipper(this.context);
		listeners = new ArrayList<FlingInterface>();
	}

	/**
	 * Used to add a view to the {@link ViewFlipper}.
	 * @param view The view that should be added
	 */
	public void addView(View view){
		flipper.addView(view);
	}
	
	/**
	 * Used to add a listener to the list of listeners.
	 * @param listener The listeners to add.
	 */
	public void addFlingListner(FlingInterface listener){
		if (!listeners.contains(listener))
			listeners.add(listener);
	}
	
	/**
	 * Method to notify all listeners.
	 * @param view The new view visible.
	 */
	private void createEvent(GestureView view){
		for (FlingInterface listener : listeners)
			listener.fling(view);
	}

	/**
	 * Inhertied method. Will return true and start the gesture detection.
	 */
	@Override
	public boolean onDown(MotionEvent e) {
		return true;
	}

	/**
	 * Inherited method. Will detect speed and direction of the gesture and handle it.
	 */
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		dir = getDir(velocityX,velocityY);
		speed = (int) Math.abs(velocityX);

		return move(dir, speed);
	}

	/**
	 * Method to detect the movement direction
	 * @param x The speed on the X-axis
	 * @param y The speed on the Y-axis
	 * @return The int value representing the direction. Left = 0, right = 1.
	 */
	private int getDir(float x, float y) {
		y = (y < 0) ? -y : y;
		if (x < -speedMargin)
			return INVALID;
		if (x < y)
			if (x < 0)
				return RIGHT;
			else 
				return LEFT;
		return INVALID;
	}

	/**
	 * Will execute the swipe to left or right if the speed is ok. 
	 * @param dir The direction to move
	 * @param speed The speed that was used
	 * @return True if the swipe was executed
	 */
	private boolean move(int dir,int speed) {
		if (MainMenuActivity.DEBUG)
			System.out.println( (speed >= minSpeed) ? ("Moving: "+ ((dir != INVALID) ? ((dir == LEFT) ? "left" : "right" ): "none")+ " speed: "+speed) : "");
		if (speedIsOK(speed))
			switch (dir){
			case LEFT: swipeLeft(); return true;
			case RIGHT: swipeRight(); return true;
			}
		return false;
	}

	/**
	 * Checks whether or not the speed is fast enough.
	 * @param speed2 The moved speed.
	 * @return True if it was was enough.
	 */
	private boolean speedIsOK(int speed2) {
		return (speed2 >= minSpeed);
	}

	/**
	 * Slides the view out to the right using a fancy animation. 
	 * Will show the next view in the list, and notify all listeners.
	 */
	private void swipeRight() {
		flipper.setInAnimation(context, R.anim.slide_in_left);
		flipper.setOutAnimation(context, R.anim.slide_out_right);
		flipper.showNext();
		createEvent((GestureView) flipper.getCurrentView());
	}

	/**
	 * Will execute a swipe to the right.
	 */
	private void swipeLeft() {
		swipeRight();
	}

	/**
	 * Method to get the {@link ViewFlipper} object
	 * @return The object holding all the views.
	 */
	public View getContainer() {
		return flipper;
	}

	/**
	 * Public method to swipe right.
	 */
	public void next() {
		swipeRight();
	}
	
	/**
	 * Public method do remove all views from the {@link ViewFlipper}.
	 */
	public void destroy(){
		flipper.removeAllViews();
		System.runFinalization();
		System.gc();
	}

}
