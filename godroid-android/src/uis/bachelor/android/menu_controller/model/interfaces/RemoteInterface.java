package uis.bachelor.android.menu_controller.model.interfaces;
/**
 * Interface. Used in all classes that will listen for movement in any joystick.
 * @author Eirk Heskja
 *
 */
public interface RemoteInterface {
	/**
	 * Interface method. 
	 * @param speed The movement speed.
	 * @param angle The movement angle
	 */
	public void onMove(int speed, int angle);

}
