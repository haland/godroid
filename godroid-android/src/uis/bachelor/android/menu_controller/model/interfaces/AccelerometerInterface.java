package uis.bachelor.android.menu_controller.model.interfaces;

/**
 * Interface used to notify observers when the device is tilted.
 * @author Anders Hole
 */
public interface AccelerometerInterface {
	
	/**
	 * Notifies observers with new accelerometer values.
	 * @param x - Value x-axis
	 * @param y - Value y-axis
	 */
	public void tiltChanged(float x, float y);
	
}
