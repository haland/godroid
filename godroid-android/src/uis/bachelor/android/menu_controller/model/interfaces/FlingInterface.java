package uis.bachelor.android.menu_controller.model.interfaces;

import uis.bachelor.android.menu_controller.views.GestureView;
/**
 * Interface. Used during flings.
 * @author Eirik Heskja
 *
 */
public interface FlingInterface {
	/**
	 * Used when the fling is complete.
	 * @param view The new, visible view.
	 */
	public void fling(GestureView view);
}
