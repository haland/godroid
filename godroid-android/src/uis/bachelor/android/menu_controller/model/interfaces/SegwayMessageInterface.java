package uis.bachelor.android.menu_controller.model.interfaces;

import array.SearchArray;
/**
 * Interface. Used in any classes that will listen for tranformation complete messages.
 * @author Eirik Heskja
 *
 */
public interface SegwayMessageInterface {
	/**
	 * Used to notify all classes implementing this interface that transformation is complete.
	 * @param values The {@link SearchArray} holding the transform status.
	 */
	public void transformationComplete(SearchArray values);
	
}
