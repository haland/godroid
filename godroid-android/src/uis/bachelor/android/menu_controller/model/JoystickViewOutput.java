package uis.bachelor.android.menu_controller.model;

import standard.Flags;
import standard.Parameters;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.menu_controller.model.interfaces.FlingInterface;
import uis.bachelor.android.menu_controller.model.interfaces.RemoteInterface;
import uis.bachelor.android.menu_controller.views.GestureView;
import array.SearchArray;
/**
 * Runnable class that will listen for joystick movement and push all messages to the {@link MessageHandler} object.
 * @author Eirik Heskja
 *
 */
public class JoystickViewOutput implements Runnable, RemoteInterface, FlingInterface{

	/**
	 * The running thread
	 */
	private Thread thread;
	
	/**
	 * Boolean variables used in the thread.
	 */
	private boolean running, stop;
	
	/**
	 * The {@link SearchArray} holding the speed and distance parameters
	 */
	private SearchArray params;
	
	/**
	 * Boolean variable used in the thread. true if the parameters has been set but not sent.
	 */
	private boolean set;
	
	/**
	 * Int varaibles used as parameters.
	 */
	private int speed = 0, angle = 0, flag = 0;
	
	/**
	 * Default constructor. Will create the thread and {@link SearchArray}.
	 */
	public JoystickViewOutput(){
		thread = new Thread(this);
		params = new SearchArray();
		set = false;
	}
	
	/**
	 * Public method to set the outgoing parameters
	 * @param params The parameters to be sent
	 */
	public void setParams(SearchArray params){
		this.params = params;
		set = true;
	}
	
	/**
	 * Starts the thread.
	 */
	public void start(){
		if (!thread.isAlive()){
			running = true;
			thread.start();
		}
	}
	
	/**
	 * Stops the thread and sends speed and angle = 0.
	 */
	public void stop(){
		onMove(0, 0);
		stop = true;
	}
	
	/**
	 * Inherited method. Will put parameters in the messageque. Busywaiting. 
	 */
	@Override
	public void run() {
		while (running){
			while (MessageHandler.getInstance().isConnected())
				if (set){
					MessageHandler.getInstance().putMessage(flag, params);
					set = false;
					if (stop){
						running = false;
						return;
					}
				}
		}
	}
	
	/**
	 * Private method to set the parameters.
	 */
	private void sendSpeedAndAngle(){
		params.set(Parameters.DRIVE_DIRECTION, angle);
		params.set(Parameters.DRIVE_VELOCITY, speed);
		setParams(params);
	}

	/**
	 * Inherited method. Will set the correct flag and send speed and angle to the NXT.
	 */
	@Override
	public void onMove(int speed, int angle) {
		if (speed != this.speed || angle != this.angle){
			this.flag = Flags.MSG_DRIVE;
			if (speed == 0 && this.speed != 0)
				flag = Flags.MSG_STOP;
			this.speed = speed;
			this.angle= angle;
			sendSpeedAndAngle();
		}
	}
	
	/**
	 * Inherited method. Makes sure that the NXT is stopped when the remote is changed. 
	 */
	@Override
	public void fling(GestureView view) {
		onMove(0, 0);
	}

}
