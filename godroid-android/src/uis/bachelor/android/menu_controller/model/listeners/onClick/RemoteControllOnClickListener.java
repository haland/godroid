package uis.bachelor.android.menu_controller.model.listeners.onClick;

import uis.bachelor.android.R;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.common.views.CustomDialog;
import uis.bachelor.android.menu_controller.ControllerActivity;
import uis.bachelor.android.menu_main.MainMenuActivity;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
/**
 * Listener class used when a user clicks on the remote control icon in the main menu.
 * @author Eirik Heskja
 *
 */
public class RemoteControllOnClickListener implements OnClickListener {
	/**
	 * The {@link Activity} holding the button.
	 */
	private Activity activity;
	
	/**
	 * The dialog to be shown if there is no connection.
	 */
	private CustomDialog dialog;
	
	/**
	 * The intent used to start the remote control activity.
	 */
	private Intent intent;

	/**
	 * Default constructor. 
	 * @param activity The {@link Activity} holding the button.
	 */
	public RemoteControllOnClickListener(Activity activity){
		this.activity = activity;
		intent = new Intent(activity, ControllerActivity.class);
		dialog = new CustomDialog(activity, activity.getString(R.string.not_connected_error));
	}

	/**
	 * Inherited method. Will show a dialog if no connection is established, else it will start the correct intent.
	 */
	@Override
	public void onClick(View v) {
		if (!MainMenuActivity.DEBUG)
			if (!MessageHandler.getInstance().isConnected()){
				dialog.show();
				return;
			}
		activity.startActivity(intent);
	}

}
