package uis.bachelor.android.menu_controller.model.listeners.onClick;

import standard.Flags;
import standard.Parameters;
import standard.Values;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.menu_controller.ControllerActivity;
import uis.bachelor.android.menu_controller.model.interfaces.SegwayMessageInterface;
import android.app.Activity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;
import array.SearchArray;

/**
 * Interface used to toggle between normal drive mode and segway drive mode in the NXT
 * @author Eirik Heskja
 *
 */
public class SegwayToggleOnClickListener implements OnClickListener, SegwayMessageInterface{
	/**
	 * The {@link ControllerActivity} {@link Activity} holding the remote controls.
	 */
	private ControllerActivity stick;
	
	/**
	 * The parameters to be sent.
	 */
	private SearchArray param;

	/**
	 * Default constructor. Creates the {@link SearchArray} parameters object.
	 * @param context The {@link ControllerActivity} object used.
	 */
	public SegwayToggleOnClickListener(ControllerActivity context){
		this.stick = context;
		param = new SearchArray();
	}

	/**
	 * Inherited method. Used to send a transform message. The clicked view has to be checkable.
	 */
	@Override
	public void onClick(View v) {
		transform(((ToggleButton) v).isChecked());
	}
	
	/**
	 * Sets the correct parameters based on the boolean variable. Will also display a transform message.
	 * @param checked If true, the NXT will go up. Else it will go down. 
	 */
	private void transform(boolean checked){
		stick.setTransforming(true);

		if (checked)
			goUp();
		else
			goDown();
		
		stick.showTransformMessage();
	}
	
	/**
	 * Used to detect clicks on {@link MenuItem}s. Transforms the NXT according to checked state. 
	 * @param item
	 */
	public void onClick(MenuItem i){
		transform(i.isChecked());
	}
	
	/**
	 * Sets the parameters and puts them in the que.
	 */
	private void goDown() {
		param.set(Parameters.TYPE, Values.SEGWAY_DOWN);
		MessageHandler.getInstance().putMessage(Flags.MSG_SEGWAY, param);
	}
	
	/**
	 * Sets the parameters and puts them in the que.
	 */
	private void goUp() {
		param.set(Parameters.TYPE, Values.SEGWAY_UP);
		MessageHandler.getInstance().putMessage(Flags.MSG_SEGWAY, param);
	}

	/**
	 * Listening method when the transformation is complete. Sets the checked state of the item.
	 */
	@Override
	public void transformationComplete(SearchArray values) {
		stick.setTransforming(false);
	}

}
