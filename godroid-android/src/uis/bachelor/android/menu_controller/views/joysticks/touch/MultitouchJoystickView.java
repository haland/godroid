package uis.bachelor.android.menu_controller.views.joysticks.touch;

import uis.bachelor.android.R;
import uis.bachelor.android.menu_controller.ControllerActivity;
import uis.bachelor.android.menu_controller.views.joysticks.StickController;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;

/**
 * This class draws the joystick controller and calculates the speed and direction. Based upon the class Eirik Heskja previously wrote
 * @author Chris H�land
 */

public class MultitouchJoystickView extends StickController implements OnLongClickListener, OnTouchListener {
	/**
	 * The proportional size of the controller images compared to the screens height. Used for setting the
	 * speed controllers height and the direction controllers width 
	 */
	private static float CONTROLLER_SIZE = 0.8f;
	
	/**
	 * The proportional size of the controller images width/height compared to the images height/width. Used
	 * for setting the speed controllers width and the direction controllers height 
	 */
	private static float CONTROLLER_SCALE = 0.504f;
	
	/**
	 * The speed controller image
	 */
	private static Bitmap speedController;
	
	/**
	 * The direction controller image
	 */
	private static Bitmap directionController;
	
	/**
	 * The speed the robot is to travel with (in the interval -100 to 100)
	 */
	private int speed;
	
	/**
	 * The direction the robot is to travel in (in the interval -100 to 100)
	 */
	private int direction;
	
	/**
	 * The highest point on the speed controller where the handle is allowed to move (please note 
	 * that this has a low value, seeing it is closer to [0,0] than the minimum point
	 */
	private int speedHandleMax;
	
	/**
	 * The lowest point on the speed controller where the handle is allowed to move (please note 
	 * that this has a high value, seeing it is further away from [0,0] than the maximum point
	 */
	private int speedHandleMin;
	
	/**
	 * The point farthest away from the screens center on the direction controller where the handle 
	 * is allowed to move.
	 */
	private int directionHandleMax;
	
	/**
	 * The point closest to the screens center on the direction controller where the handle 
	 * is allowed to move.
	 */
	private int directionHandleMin;
	
	/**
	 * The position of the speed controller handle
	 */
	private Point speedHandle;
	
	/**
	 * The point where the speed controller is drawn from
	 */
	private Point speedControllerZero;
	
	/**
	 * The center of the speed controller
	 */
	private Point speedControllerCenter;
	
	/**
	 * The position of the direction controller handle
	 */
	private Point directionHandle;
	
	/**
	 * The point where the direction controller is drawn from
	 */
	private Point directionControllerZero;
	
	/**
	 * The center of the speed controller
	 */
	private Point directionControllerCenter;
	
	/**
	 * The upper left corner of the speed handler bounds
	 */
	private Point speedHandlerUpperLeftBound;
	
	/**
	 * The lower right corner of the speed handler bounds
	 */
	private Point speedHandlerLowerRightBound;
	
	/**
	 * The upper left corner of the direction handler bounds
	 */
	private Point directionHandlerUpperLeftBound;
	
	/**
	 * The lower right corner of the direction handler bounds
	 */
	private Point directionHandlerLowerRightBound;
	
	/**
	 * The parent class of the two controllers (joystick and accelerometer)
	 */
	private ControllerActivity parent;
	
	/**
	 * Creates the joystick controller
	 * @param context - The Context
	 * @param attrs - Set of attributes
	 */
	public MultitouchJoystickView(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (!isInEditMode()){
			setOnLongClickListener(this);
			setOnTouchListener(this);
		}
	}
	
	/**
	 * Sets the screen center, creates the speed and direction controllers and sets all variables associated
	 * with the controllers. Also the handlers are centered.
	 */
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if (speedController == null) 
			speedController = createSpeedController();
		if (directionController == null)
			directionController = createDirectionController();

		speedControllerZero = new Point((int)(width * 0.125f), 
				(int)(height * 0.1f));
		speedControllerCenter = new Point(speedControllerZero.x + speedController.getWidth()/2, 
				speedControllerZero.y + speedController.getHeight()/2);
		
		speedHandleMax = speedControllerZero.y;
		speedHandleMin = speedControllerZero.y + speedController.getHeight();
		speedHandle = new Point(speedControllerCenter.x, speedControllerCenter.y);
		
		directionControllerZero = new Point((int) (width - (directionController.getWidth() + width * 0.02225f)), 
				(int) (height/2.0f - directionController.getHeight()/2.0f));
		directionControllerCenter = new Point(directionControllerZero.x + directionController.getWidth()/2,
				directionControllerZero.y + directionController.getHeight()/2);
		
		directionHandleMin = directionControllerZero.x;
		directionHandleMax = directionControllerZero.x + directionController.getWidth();
		directionHandle = new Point(directionControllerCenter.x, directionControllerCenter.y);
		
		speedHandlerUpperLeftBound = new Point(speedControllerCenter.x - speedController.getWidth()/4, 0);
		speedHandlerLowerRightBound = new Point(speedControllerCenter.x + speedController.getWidth()/4, height);
		
		directionHandlerUpperLeftBound = new Point(directionHandleMin - directionController.getHeight()/4, directionControllerCenter.y - directionController.getHeight()/4);
		directionHandlerLowerRightBound = new Point(directionHandleMax, directionControllerCenter.y + directionController.getHeight()/4);		
	}
	
	/**
	 * Creates the speed controller
	 * @return The speed controller
	 */
	private Bitmap createSpeedController() {
		int height = (int) (this.height * CONTROLLER_SIZE);
		int width = (int) (height * CONTROLLER_SCALE);
		
		Bitmap speedControllerSource = BitmapFactory.decodeResource(getResources(), R.drawable.controlls_speed_controll);
		Bitmap speedController = Bitmap.createScaledBitmap(speedControllerSource, width, height, true);
				
		return speedController;
	}
	
	/**
	 * Creates the direction controller
	 * @return The direction controller
	 */
	private Bitmap createDirectionController() {
		int width = (int) (this.height * CONTROLLER_SIZE);
		int height = (int) (width * CONTROLLER_SCALE);
		
		Bitmap directionControllerSource = BitmapFactory.decodeResource(getResources(), R.drawable.controlls_turn_controll);
		Bitmap directionController = Bitmap.createScaledBitmap(directionControllerSource, width, height, true);
		
		return directionController;
	}
	
	/**
	 * Repaints whenever a the screen registers touch changes
	 */
	protected void onDraw(Canvas canvas) {
		canvas.save();
		
		drawControllers(canvas);
		drawHandles(canvas);

		canvas.restore();
		
		super.onDraw(canvas);
	}

	/**
	 * Draws the controllers
	 * @param canvas - The canvas to draw on
	 */
	private void drawControllers(Canvas canvas) {
		drawSpeedController(canvas);
		drawDirectionController(canvas);
	}
	
	/**
	 * Draws the speed controller
	 * @param canvas - The canvas to draw on
	 */
	private void drawSpeedController(Canvas canvas) {
		canvas.drawBitmap(speedController, speedControllerCenter.x - speedController.getWidth()/2, 
				speedControllerCenter.y - speedController.getHeight()/2, bgPaint);
	}
	
	/**
	 * Draws the direction controller
	 * @param canvas - The canvas to draw on
	 */
	private void drawDirectionController(Canvas canvas) {
		canvas.drawBitmap(directionController, directionControllerZero.x, directionControllerZero.y, bgPaint);
	}
	
	/**
	 * Draws the handles
	 * @param canvas - The canvas to draw on
	 */
	private void drawHandles(Canvas canvas) {
		drawSpeedHandle(canvas);
		drawDirectionHandle(canvas);
	}
	
	/**
	 * Draws the speed controllers handle
	 * @param canvas - The canvas to draw on
	 */
	private void drawSpeedHandle(Canvas canvas) {
		int x = speedHandle.x;
		int y = speedHandle.y;
		
		drawStrick(canvas, x, y);
	}
	
	/**
	 * Draws the direction controllers handle
	 * @param canvas - The canvas to draw on
	 */
	private void drawDirectionHandle(Canvas canvas) {
		int x = directionHandle.x;
		int y = directionHandle.y;
		
		drawStrick(canvas, x, y);
	}

	/**
     * Method that runs when a user long-clicks on the screen.
     * Will return false, indicating that the touch-event should be processed somewhere else.
     */
	public boolean onLongClick(View v) {
		return false;
	}

	/**
	 * Runs when the user touches the screen. Updates speed and direction
	 */
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		Point[] points = getPoints(event);
		
		try {
			if (!parent.isTransforming()) {
				switch (action) {
					case MotionEvent.ACTION_DOWN:
						updateHandles(points);
						break;
					case MotionEvent.ACTION_UP:
						centerHandles(points);
						break;
					case MotionEvent.ACTION_MOVE:
						updateHandles(points);
						break;
					default:
						break;
				}
			} else {
				speed = 0;
				direction = 0;
			}
			
			invalidate();
			createRemoteEvent(speed, direction);
		} catch (NullPointerException e) {
			System.out.println("The objects parent needs to be set to the used LegoJoystick!");
			e.printStackTrace();
		}
		
		return super.onTouch(v, event);
	}
	
	/**
	 * Centers the handles to their representative controllers 
	 */
	private void centerHandles() {
		centerSpeedHandler();
		centerDirectionHandler();
	}
	
	/**
	 * Centers the handles in the center of their controllers
	 * @param points 
	 */
	private void centerHandles(Point[] points) {
		for (Point point : points) {
			if (checkSpeedHandlerBounds(point))
				centerSpeedHandler();
			else if (checkDirectionHandlerBounds(point))
				centerDirectionHandler();
		}
	}
	
	/**
	 * Centers the speed controller handle in the center of the speed controller
	 */
	private void centerSpeedHandler() {
		if (speedHandle == null)
			speedHandle = new Point();
		speed = 0;
		speedHandle.set(speedControllerCenter.x, speedControllerCenter.y);
	}
	
	/**
	 * Centers the direction controller handle in the center of the direction controller
	 */
	private void centerDirectionHandler() {
		if (directionHandle == null)
			directionHandle = new Point();
		direction = 0;
		directionHandle.set(directionControllerCenter.x, directionControllerCenter.y);
	}
	
	/**
	 * Updates the position of both handles
	 * @param points - The points on the screen being touched
	 */
	private void updateHandles(Point[] points) {
		boolean speedHandleUpdated = false; 
		boolean directionHandleUpdated = false;
		
		for (Point point : points) {
			if (checkSpeedHandlerBounds(point)) { 
				updateSpeedHandle(point.y);
				speedHandleUpdated = true;
			} else if (checkDirectionHandlerBounds(point)) {
				updateDirectionHandle(point.x);
				directionHandleUpdated = true;
			}
		}
		
		if (!speedHandleUpdated) centerSpeedHandler();
		if (!directionHandleUpdated) centerDirectionHandler();
	}

	/**
	 * Updates the position of the speed controller handle and sets the speed
	 * @param y
	 */
	private void updateSpeedHandle(int y) {
		if (y > speedHandleMax && y < speedHandleMin) {
			speedHandle.set(speedHandle.x, y);
			setSpeed(y);
		}
	}

	/**
	 * Updates the position of the direction controller handle and sets the direction
	 * @param x
	 */
	private void updateDirectionHandle(int x) {
		if (x > directionHandleMin && x < directionHandleMax) {
			directionHandle.set(x, directionHandle.y);
			setDirection(x);
		}
	}
	
	/**
	 * 
	 * Sets the LegoJoystick object used to check if the robot is transforming
	 * @param parent - The LegoJoystick object
	 */
	public void setParent(ControllerActivity parent) {
		this.parent = parent;
	}
	
	/**
	 * Sets the direction
	 * @param x - The x-coordinate of the direction controller handle
	 */
	private void setDirection(int x) {
		int max;
		if (x < directionHandleMax && x > directionControllerCenter.x) {
			x -= directionControllerCenter.x;
			max = directionHandleMax - directionControllerCenter.x;
			direction = (int)(x * (100.0f/max));
		} else if (x < directionControllerCenter.x && x > directionHandleMin) {
			x -= directionControllerCenter.x;
			max = directionHandleMin - directionControllerCenter.x;
			direction = -(int)(x * (100.0f/max));
		} else direction = 0;
		
		if (direction > 100) direction = 100;
		if (direction < -100) direction = -100;
	}

	/**
	 * Sets the speed
	 * @param y - The y-coordinate of the speed controller handle
	 */
	private void setSpeed(int y) {
		int max;
		if (y > speedHandleMax && y < speedControllerCenter.y) {
			y -= speedControllerCenter.y;
			max = speedHandleMax - speedControllerCenter.y;
			speed = (int)(y * (100.0f/max));
		} else if (y > speedControllerCenter.y && y < speedHandleMin) {
			y -= speedControllerCenter.y;
			max = speedHandleMin - speedControllerCenter.y;
			speed = -(int)(y * (100.0f/max));
		} else speed = 0;
		
		if (speed > 100) speed = 100;
		if (speed < -100) speed = -100;
	}
	
	/**
	 * Returns all touched points on the screen
	 * @param event - The motion event
	 * @return Array of point on screen being touched
	 */
	private Point[] getPoints(MotionEvent event) {
		Point[] points = new Point[event.getPointerCount()];
		for (int i = 0; i < event.getPointerCount(); i++) {
			points[i] = new Point((int)event.getX(i), (int) event.getY(i));
		}
		return points;
	}
	
	 /**
     * Method that should be run when the view is being stopped. Will return both handles to their default position and 
     * tell all listeners that speed and angle has been changed.
     * Will also destroy any remaining source {@link Bitmap}s.
     */
	public void stop() {
		centerHandles();
		createRemoteEvent(speed, direction);
		destroySources();
	}
	
	/**
	 * Check if a point is in the speed handlers bounds
	 * @param point - The point to check
	 * @return Whether the point is in bounds
	 */
	private boolean checkSpeedHandlerBounds(Point point) {
		return checkSpeedHandlerBoundsX(point.x) && checkSpeedHandlerBoundsY(point.y);
	}
	
	/**
	 * Check if the x-coordinate is in the speed handlers x-bounds 
	 * @param x - The x-coordinate
	 * @return Whether the coordinate is in bounds
	 */
	private boolean checkSpeedHandlerBoundsX(int x) {
		return (x >= speedHandlerUpperLeftBound.x && x <= speedHandlerLowerRightBound.x);
	}
	
	/**
	 * Check if the y-coordinate is in the speed handlers y-bounds 
	 * @param y - The y-coordinate
	 * @return Whether the coordinate is in bounds
	 */
	private boolean checkSpeedHandlerBoundsY(int y) {
		return (y >= speedHandlerUpperLeftBound.y && y <= speedHandlerLowerRightBound.y);
	}
	
	/**
	 * Check if a point is in the direction handlers bounds
	 * @param point - The point to check
	 * @return Whether the point is in bounds
	 */
	private boolean checkDirectionHandlerBounds(Point point) {
		return checkDirectionHandlerBoundsX(point.x) && checkDirectionHandlerBoundsY(point.y);
	}
	
	/**
	 * Check if the x-coordinate is in the direction handlers x-bounds 
	 * @param x - The x-coordinate
	 * @return Whether the coordinate is in bounds
	 */
	private boolean checkDirectionHandlerBoundsX(int x) {
		return (x >= directionHandlerUpperLeftBound.x && x <= directionHandlerLowerRightBound.x);
	}
	
	/**
	 * Check if the y-coordinate is in the direction handlers y-bounds 
	 * @param y - The y-coordinate
	 * @return Whether the coordinate is in bounds
	 */
	private boolean checkDirectionHandlerBoundsY(int y) {
		return (y >= directionHandlerUpperLeftBound.y && y <= directionHandlerLowerRightBound.y);
	}
}
