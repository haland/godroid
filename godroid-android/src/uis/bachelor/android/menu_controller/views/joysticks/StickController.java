package uis.bachelor.android.menu_controller.views.joysticks;

import uis.bachelor.android.R;
import uis.bachelor.android.menu_controller.ControllerActivity;
import uis.bachelor.android.menu_controller.views.GestureView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

/**
 * Class used for the sticks.
 * @author Eirik Heskja
 *
 */
public abstract class StickController extends GestureView{
	
	/**
	 * Static representation of the handle stick.
	 */
	protected static Bitmap stick;
	
	/**
	 * The background of the handle stick-
	 */
	protected Paint bgPaint;
	
	/**
	 * The source image for the stick. Will be null if the stick is created.
	 */
	private Bitmap stickSrc;
	
	/**
	 * The margin of the stick.
	 */
	protected static final int MARGIN = 200;
	
	/**
	 * The width and height of the stick.
	 */
	protected static int RADIUS = 70;
	
	/**
	 * The padding of the stick.
	 */
	protected static final int PADDING = 30;
	
	/**
	 * The screen width and height.
	 */
	protected int width, height;

	/**
	 * Default constructor. Will instantiate the stickSrc if the stick is null.
	 * @param context The context holding the view.
	 * @param attrs The attributes defined for the view.
	 */
	public StickController(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (stick == null) stickSrc = BitmapFactory.decodeResource(getResources(), R.drawable.controlls_universal_stick);
	}
	
	/**
	 * Sets the stick radius relative to the screen width.
	 * Instantiates the bitmap and paint.
	 */
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		
		width = getWidth();
		height = getHeight();
		
		RADIUS = width/10;
		
		initBitmap();
		initPaint();
		
	}
	
	/**
	 * If the stick is null, it will be instantiated as a scaled version of the source.
	 */
	private void initBitmap() {
		if (stick == null) stick = Bitmap.createScaledBitmap(stickSrc, RADIUS, RADIUS, true);
	}
	
	/**
	 * Creates the background paint.
	 */
	private void initPaint() {
		bgPaint = new Paint();
		bgPaint.setFilterBitmap(true);
	}
	
	/**
	 * Destroys sources and asks the GC to run.
	 */
	protected void destroySources(){
		if (stickSrc != null)
			stickSrc.recycle();
		System.runFinalization();
		System.gc();
	}

	/**
	 * Will draw the stick.
	 * @param canvas The canvas to draw on.
	 * @param x X-coordinate to draw
	 * @param y Y-coordinate to draw
	 */
	protected void drawStrick(Canvas canvas, int x, int y){
		canvas.drawBitmap(stick, x-(RADIUS/2), y-(RADIUS/2), bgPaint);
	}
	
	/**
	 * Sets the LegoJoystick object to the controller, so they can call the isTransforming() method
	 * @param parent
	 */
	public abstract void setParent(ControllerActivity parent);
	
	/**
	 * To be run when the activity stops
	 */
	public abstract void stop();
}
