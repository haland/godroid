package uis.bachelor.android.menu_controller.views.joysticks.accelerometer;

import uis.bachelor.android.menu_controller.ControllerActivity;
import uis.bachelor.android.menu_controller.model.Accelerometer;
import uis.bachelor.android.menu_controller.model.interfaces.AccelerometerInterface;
import uis.bachelor.android.menu_controller.views.joysticks.StickController;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * @author Anders Hole
 */

public class AccelerometerJoystick extends StickController implements AccelerometerInterface {

	private float y, x;
	private float centerX, centerY;
	private float maxX, maxY;
	
	/**
	 * Max speed and angle.
	 */
	private final int maxSpeed = 100, maxAngle = 100;
	private int speed, angle;
	private Accelerometer accel;
	
	/**
	 * The parent class of the two controllers (joystick and accelerometer)
	 */
	private ControllerActivity parent;

	/**
	 * Creates the Accelerometer joystick. Registers as an {@link Accelerometer} listener.
	 * @param context
	 * @param attrs
	 */
	public AccelerometerJoystick(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(true);
		setFocusableInTouchMode(true);

		if (!isInEditMode()){

			accel = new Accelerometer(context);
			accel.addObserver(this);

			maxX = getMeasuredWidth();
			maxY = getMeasuredHeight();
		}
	}

	/**
	 * Sets the parent of the controllers
	 * @param parent - The LegoJoystick object
	 */
	public void setParent(ControllerActivity parent) {
		this.parent = parent;
	}
	
	/**
	 * Checks if the speed and angle values are in the allowed range and sends them to the robot through {@link GestureView}{@link #createRemoteEvent(int, int)}.
	 */
	private void createRemoteEvent(){
		if(angle > maxAngle)
			angle = 100;
		else if(angle < -maxAngle)
			angle = -100;
		if(speed>maxSpeed)
			speed = 100;
		else if(speed<-maxSpeed)
			speed = -100;

		createRemoteEvent(-speed, angle);
	}

	/**
	 * @see android.view.View#onWindowFocusChanged(boolean)
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		updateSize();
	}

	/**
	 * Updates the size of the screen(pixels).
	 */
	private void updateSize(){
		maxY = getMeasuredHeight();
		maxX = getMeasuredWidth();

		centerX = maxX/2;
		centerY = maxY/2;
	}
	
	/**
	 * Draws the accelerometer graphics.
	 */
	@Override
	public void onDraw(Canvas canvas) {
		drawStrick(canvas, (int) (centerX + x*maxX/2), (int) (centerY + y*maxY/2));
	}

	/**
	 * Updates the speed and angle value and sends a command to the robot. Redraws the view with the updated values.
	 */
	@Override
	public void tiltChanged(float x, float y) {
		try {
			if (!parent.isTransforming()) {
				int angle = (int) (y*maxAngle);
				int speed = (int) (x*maxSpeed);
				if ((this.angle > angle || this.angle < angle) && (this.speed > speed || this.speed < speed )){
					// X, Y is accelerometer axes, since the device is rotated X = y and Y = x
					this.y = x;
					this.x = y;

					this.angle = angle;
					this.speed = speed;
				}
			} else {
				angle = 0;
				speed = 0;
			}
			
			invalidate();
			createRemoteEvent();
		} catch (NullPointerException e) {
			System.out.println("The objects parent needs to be set to the used LegoJoystick!");
			e.printStackTrace();
		}
	}

	/**
	 * Stops the accelerometer joystick and unregisters sensor listener.
	 */
	public void stop() {
		accel.unregisterListener();
		destroySources();
	}
	
}