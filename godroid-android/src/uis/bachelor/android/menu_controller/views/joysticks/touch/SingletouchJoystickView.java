package uis.bachelor.android.menu_controller.views.joysticks.touch;

import uis.bachelor.android.menu_controller.ControllerActivity;
import uis.bachelor.android.menu_controller.views.joysticks.StickController;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;

/**
 * Class that creates the single touch joystick view 
 * @author Chris H�land
 */

public class SingletouchJoystickView extends StickController implements OnLongClickListener, OnTouchListener {

	/**
	 * The speed the robot is to travel with (in the interval -100 to 100)
	 */
	private int speed;
	
	/**
	 * The direction the robot is to travel in (in the interval -100 to 100)
	 */
	private int direction;
	
	/**
	 * Center of the screen
	 */
	private Point center;
	
	/**
	 * The location of the handle
	 */
	private Point location;
	
	/**
	 * The parent class of the two controllers (joystick and accelerometer)
	 */
	private ControllerActivity parent;
	
	/**
	 * Creates the joystick controller
	 * @param context - The Context
	 * @param attrs - Set of attributes
	 */
	public SingletouchJoystickView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (!isInEditMode()){
			setOnLongClickListener(this);
			setOnTouchListener(this);
		}
	}
	
	/**
	 * Sets the LegoJoystick object used to check if the robot is transforming
	 * @param parent - The LegoJoystick object
	 */
	public void setParent(ControllerActivity parent) {
		this.parent = parent;
	}

	/**
	 * Sets the screen center and sets the handles location to center at start
	 */
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		
		center = new Point(width/2, height/2);
		location = new Point(center);
	}

	/**
	 * Repaints whenever a the screen registers touch changes
	 */
	protected void onDraw(Canvas canvas) {
		canvas.save();
		
		drawStrick(canvas, location.x, location.y);
		
		canvas.restore();
		super.onDraw(canvas);
	}
	
	/**
	 * Runs when the user touches the screen. Updates speed and direction
	 */
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction();
		Point point = getPoints(event)[0];
		
		try {
			if (!parent.isTransforming()) {
				switch (action) {
					case MotionEvent.ACTION_DOWN:
						updateHandle(point);
						break;
					case MotionEvent.ACTION_UP:
						centerHandle();
						break;
					case MotionEvent.ACTION_MOVE:
						updateHandle(point);
						break;
					default:
						break;
				}
			} else {
				speed = 0;
				direction = 0;
			}
			
			invalidate();
			createRemoteEvent(speed, direction);
		} catch (NullPointerException e) {
			System.out.println("The objects parent needs to be set to the used LegoJoystick!");
		}
		
		return super.onTouchEvent(event);
	}
	
	/**
	 * Updates the speed and direction and location of the handle.
	 * @param point - The current point
	 */
	private void updateHandle(Point point) {
		setSpeed(point.y);
		setDirection(point.x);
		location.set(point.x, point.y);
	}
	
	/**
	 * Centers the handles
	 */
	private void centerHandle() {
		speed = 0;
		direction = 0;
		location.set(center.x, center.y);
	}
	
	/**
	 * Sets the speed
	 * @param y - The y-coordinate
	 */
	private void setSpeed(int y) {
		if (y > 0 && y < center.y) {
			speed = 100 - (int)(y * (100.0f/center.y));
		} else if (y < height && y > center.y) {
			speed = -(int)((y-center.y) * (100.0f/center.y));
		}
		
		if (speed > 100) speed = 100;
		if (speed < -100) speed = -100;
	}
	
	/**
	 * Sets the direction 
	 * @param x - The x-coordinate
	 */
	private void setDirection(int x) {
		if (x > 0 && x < center.x) {
			direction = -(100 - (int)(x * (100.0f/center.x)));
		} if (x < width && x > center.x) {
			direction = (int)((x-center.x) * (100.0f/center.x));
		}
		
		if (direction > 100) direction = 100;
		if (direction < -100) direction = -100;
	}
	
	/**
	 * Returns all touched points on the screen
	 * @param event - The motion event
	 * @return Array of point on screen being touched
	 */
	private Point[] getPoints(MotionEvent event) {
		Point[] points = new Point[event.getPointerCount()];
		for (int i = 0; i < event.getPointerCount(); i++) {
			points[i] = new Point((int)event.getX(i), (int) event.getY(i));
		}
		return points;
	}
	
	/**
     * Method that runs when a user long-clicks on the screen.
     * Will return false, indicating that the touch-event should be processed somewhere else.
     */
	public boolean onLongClick(View arg0) {
		return false;
	}
	
	 /**
     * Method that should be run when the view is being stopped. Will return both handles to their default position and 
     * tell all listeners that speed and angle has been changed.
     * Will also destroy any remaining source {@link Bitmap}s.
     */
	public void stop() {
		centerHandle();
		createRemoteEvent(speed, direction);
		destroySources();
	}
}
