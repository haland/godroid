package uis.bachelor.android.menu_controller.views;

import java.util.ArrayList;

import uis.bachelor.android.menu_controller.model.interfaces.FlingInterface;
import uis.bachelor.android.menu_controller.model.interfaces.RemoteInterface;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
/**
 * Class used to detect gestures on the screen.
 * @author Eirik Heskja
 *
 */
public class GestureView extends View implements OnTouchListener, FlingInterface {
	
	/**
	 * The gesture detector that will listen for detection.
	 */
	private GestureDetector gesture;
	
	/**
	 * The list of {@link RemoteInterface}s that will be notified on fling.
	 */
	private ArrayList<RemoteInterface> listeners;
	
	/**
	 * Default constructor.
	 * @param context The context holding this view.
	 */
	public GestureView(Context context) {
		this(context, null);
	}
	
	/**
	 * Creates the {@link ArrayList}.
	 * @param context The context holding the view
	 * @param attrs The attributes defined for this view.
	 */
	public GestureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		listeners = new ArrayList<RemoteInterface>();
	}
	
	/**
	 * Tells all the listeners that the speed or angle has changed.
	 * @param speed The new speed
	 * @param angle The new angle
	 */
	protected void createRemoteEvent(int speed, int angle){
		for (RemoteInterface listener : listeners)
			listener.onMove(speed, angle);
	}

	/**
	 * Adds a listener to the list.
	 * @param listener The listener to add.
	 */
	public void addRemoteListener(RemoteInterface listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}
	
	/**
	 * Removes the listner from the list.
	 * @param output The listener to remove.
	 */
	public void removeRemoteListener(RemoteInterface output) {
			listeners.remove(output);
	}

	/**
	 * Used to detect gestures on touch.
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (gesture != null && gesture.onTouchEvent(event))
			return true;
		return false;
	}
	
	/**
	 * Sets the current gesture detector.
	 * @param gesture The new detector.
	 */
	public void setGestureDetector(GestureDetector gesture) {
		this.gesture = gesture;
	}

	/**
	 * Cleas the list of listeners if this view is not the one in focus.
	 */
	@Override
	public void fling(GestureView view) {
		if(!view.equals(this))
			listeners.clear();
	}

}
