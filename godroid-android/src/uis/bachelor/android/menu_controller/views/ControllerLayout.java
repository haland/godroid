package uis.bachelor.android.menu_controller.views;

import uis.bachelor.android.R;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.common.model.communication.interfaces.BluetoothDisconnectMessageInterface;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Custom layout that's used in the remote controls.
 * @author Eirik Heskja
 *
 */
public class ControllerLayout extends RelativeLayout implements BluetoothDisconnectMessageInterface {
	/**
	 * Integer used in the handler.
	 */
	protected static final int UPDATE_BG = 0;
	
	/**
	 * Bitmaps for background, dicsonnected and connected images.
	 */
	private static Bitmap background, disconnected, connected;
	
	/**
	 * Source files for the connected and disconnected image. Will be null if the image has been created.
	 */
	private Bitmap connectedSrc, disconnectedSrc;
	
	/**
	 * Width of the screen
	 */
	private int width;
	
	/**
	 * Height of the screen
	 */
	private int height;
	
	/**
	 * Handler object to update the background on disconnect.
	 */
	private Handler handler;
	
	/**
	 * The background paint.
	 */
	private Paint bgPaint;
	
	/**
	 * Default constructor. Creates the source images if they are null. Will create a new handler object and add this object as a listener in {@link MessageHandler}.
	 * @param context The context holding the layout.
	 * @param attrs The attributes defined for this layout.
	 */
	public ControllerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		setWillNotDraw(false);
		
		if (disconnected == null) disconnectedSrc = BitmapFactory.decodeResource(getResources(), R.drawable.controlls_background_disconnected);
		if (connected == null) connectedSrc = BitmapFactory.decodeResource(getResources(), R.drawable.controlls_background_connected);
		
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch(msg.what){
				case UPDATE_BG: setBackgroundImage(); break;
				}
				super.handleMessage(msg);
			}
		};
		MessageHandler.getInstance().addDisconnectListener(this);
	}
	
	/**
	 * Will set the background image to the disconnected image.
	 */
	protected void setBackgroundImage() {
		background = disconnected;
		invalidate();
	}
	
	/**
	 * Measueres width and heigt, and creates the paint and bitmap.
	 * The background will be set to connected.
	 */
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		width = getWidth();
		height = getHeight();
		
		initPaint();
		initBitmap();
		
		background = (MessageHandler.getInstance().isConnected()) ? connected : disconnected;
	}
	
	/**
	 * Will draw the background when the view need to be redrawn.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.save();
		
		drawBackground(canvas);
		
		canvas.restore();
		
		super.onDraw(canvas);
	}
	
	/**
	 * Will draw the background image in the 0,0 coordinate of the screen.
	 * @param canvas The canvas do draw on.
	 */
	protected void drawBackground(Canvas canvas){
		canvas.drawBitmap(background, 0, 0, bgPaint);
	}
	
	/**
	 * Will instantiate the images and destroy the sources.
	 */
	private void initBitmap() {
		if (disconnected == null) disconnected = Bitmap.createScaledBitmap(disconnectedSrc, width, height, true);
		if (connected == null) connected = Bitmap.createScaledBitmap(connectedSrc, width, height, true);
		destroySources();
	}
	
	/**
	 * Will create a new bacground paint object.
	 */
	private void initPaint() {
		bgPaint = new Paint();
		bgPaint.setFilterBitmap(true);
	}
	
	/**
	 * Will destroy the sources and ask the GC to run.
	 */
	public void destroySources(){
		if (disconnectedSrc != null)
			disconnectedSrc.recycle();
		if (connectedSrc != null)
			connectedSrc.recycle();
		System.gc();
	}
	
	/**
	 * Will update the background on disconnect.
	 */
	@Override
	public void disconnect() {
		handler.sendEmptyMessage(UPDATE_BG);
	}

}
