package uis.bachelor.android.common.views;

import uis.bachelor.android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

/**
 * Class representing a popup dialog.
 * @author Eirik Heskja
 *
 */
public class CustomDialog {
	
	/**
	 * The dialog that will be shown.
	 */
	private AlertDialog dialog;

	/**
	 * boolean variable defining if the dialog is visible or not.
	 */
	private boolean visible;

	/**
	 * Will create a default error dialog.
	 * @param context The context holding the dialog.
	 * @param message The message to show.
	 */
	public CustomDialog(Context context, String message){
		this(context, context.getResources().getString(R.string.error) , message);
	}
	
	/**
	 * Will create a custom error dialog.
	 * @param context The context holding the dialog.
	 * @param title The title of the dialog.
	 * @param message The message to show.
	 */
	public CustomDialog(Context context, String title, String message){
		visible = false;
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				CustomDialog.this.dismiss();
			}
		});
		dialog.setIcon(android.R.drawable.ic_dialog_alert);
		this.dialog = dialog.create();
	
	}

	/**
	 * Will create a custom dialog
	 * @param context The context holding the dialog.
	 * @param title The title of the dialog.
	 * @param message The message to show.
	 * @param cancel Can the dialog be canceled?
	 */
	public CustomDialog(Context context, String title, String message, boolean cancel){
		visible = false;
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setCancelable(cancel);
		dialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				CustomDialog.this.dismiss();
			}
		});
		
		dialog.setIcon(android.R.drawable.ic_dialog_info);
		this.dialog = dialog.create();
	}

	/**
	 * Method to set the listener of the positive button.
	 * The text will be "OK".
	 * @param listener the Listener.
	 */
	public void setPositiveButtonListener(DialogInterface.OnClickListener listener){
		dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", listener);
	}

	/**
	 * Method to set the listener of the positive button.
	 * @param text The text on the button.
	 * @param listener The listener
	 */
	public void setPositiveButton(String text, DialogInterface.OnClickListener listener){
		dialog.setButton(AlertDialog.BUTTON_POSITIVE, text, listener);
	}

	/**
	 * Method to set the listener of the negative button.	
	 * @param text The text on the button
	 * @param listener The listener
	 */
	public void setNegativeButton(String text, DialogInterface.OnClickListener listener){
		dialog.setButton(AlertDialog.BUTTON_NEGATIVE, text, listener);
	}

	/**
	 * Used to get the visibility status.
	 * @return Returns true if the button is visible
	 */
	public boolean isVisible(){
		return visible;
	}

	/**
	 * Will show the dialog if it's not visible.
	 */
	public void show(){
		if (!visible){
			dialog.show();
			visible = true;
		}
	}

	/**
	 * Will set visible status to false and dismiss the dialog.
	 */
	public void dismiss(){
		visible = false;
		dialog.dismiss();
	}

	/**
	 * Sets the cancel listener for the entire dialog.
	 * @param onCancel The listener.
	 */
	public void setOnCancelListener(OnCancelListener onCancel) {
		dialog.setOnCancelListener(onCancel);
	}
}
