package uis.bachelor.android.common.views.listeners.onClick;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
/**
 * Class used when the user clicks on a negative button in a dialog.
 * @author Eirik Heskja
 *
 */
public class DefaultNegativeOnClickListener implements OnClickListener {
	/**
	 * Executed when the user clicks the button.
	 * Will dismiss the dialog.
	 */
	@Override
	public void onClick(DialogInterface dialog, int which) {
		dialog.dismiss();
	}

}
