package uis.bachelor.android.common;

import uis.bachelor.android.menu_bluetooth.model.DeviceDisplayer;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
/**
 * 
 * Activity used to respond to Bluetooth Start Request. This request will be sent to the system, asking to enable the {@link BluetoothAdapter}.
 * If the system fails to enable Bluetooth, this activity will finish. Else it will start listing devices.
 * @author Eirik Heskja
 *
 */
public class ResultActivity extends LanguageActivity {
	/**
	 * 
	 * {@link Integer} defining the result to listen for.
	 */
	private static final int BLUETOOTH_START_REQUEST = 1;
	
	/**
	 * Object that will start start the listing of devices if Bluetooth is enabled.
	 */
	protected DeviceDisplayer deviceDisplayer;
	
	/**
	 * 
	 * Will be run when the application starts.
	 * Instantiates the {@link DeviceDisplayer} object.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		deviceDisplayer = new DeviceDisplayer(this);
	}
	
	/**
	 * Method that will be executed when the system finishes it's attempt to enable the {@link BluetoothAdapter}.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BLUETOOTH_START_REQUEST)
			handleBluetoothResponse(resultCode, data);
	}

	/**
	 * Method for handling the response from the result.
	 * @param resultCode The code identifying the response.
	 * @param data The {@link Intent} associated with the response.
	 */
	public void handleBluetoothResponse(int resultCode, Intent data) {
		if (resultCode == RESULT_OK)
			deviceDisplayer.listDevicesIfBluetoothIsEnabled();
		else
			finish();
	}
}
