package uis.bachelor.android.common;

import standard.Parameters;
import standard.Values;
import standard.Version;
import uis.bachelor.android.R;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.common.model.communication.interfaces.BatteryMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.BluetoothDisconnectMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.DistanceMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.SignalMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.VersionMessageInterface;
import uis.bachelor.android.common.views.CustomDialog;
import uis.bachelor.android.menu_controller.model.interfaces.SegwayMessageInterface;
import uis.bachelor.android.menu_settings.model.interfaces.CalibrationCompleteInterface;
import uis.bachelor.android.menu_settings.model.interfaces.SettingAndCalibrationStartedInterface;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import array.SearchArray;

/**
 * 
 * Abstract class that extends {@link LanguageActivity}. Any {@link Activity} that extends this class will be able to display notification popups and listen for messages. 
 * This {@link Activity} contains no user interface and should only be extended! 
 * @author Eirik Heskja
 *
 */
public abstract class HandlerActivity extends LanguageActivity implements  BluetoothDisconnectMessageInterface, CalibrationCompleteInterface, SettingAndCalibrationStartedInterface, SegwayMessageInterface, VersionMessageInterface, DistanceMessageInterface, SignalMessageInterface, BatteryMessageInterface{

	private static final int MSG_UPDATE_MENU = 0;

	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_UPDATE = 1;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_SENDING_SETTINGS = 2;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_DISMISS = 3;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_DISCONNECT = 4;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_TRANSFORM = 5;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_VERSION = 6;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_DISTANCE = 7;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_SIGNAL = 8;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_BATTERY = 9;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_SHOW_TRANSFORM = 10;
	
	/**
	 * 
	 * Final {@link Integer} variable defining a action for the {@link Handler} object.
	 */
	protected final int MSG_WARNING = 11;

	/**
	 * 
	 * The {@link Handler} object that will handle messages from other threads. 
	 */
	protected Handler handler;
	
	/**
	 * 
	 * Boolean variable used for some messages. Some notifications should only be visible if the {@link Activity} has focus.
	 */
	protected boolean hasFocus, showLoading;

	/**
	 * 
	 * {@link CustomDialog} object that displays information when the user is disconnected.
	 */
	protected CustomDialog disconnectDialog;
	
	/**
	 * 
	 * {@link CustomDialog} object that displays information when calibrating the NXT.
	 */
	protected CustomDialog progress;
	
	/**
	 * 
	 * {@link CustomDialog} object that displays information when the transformation was uncompleted.
	 */
	protected CustomDialog unComplete;
	
	/**
	 * 
	 * {@link CustomDialog} object that displays information when there is a difference between NXT and Android version of the program.
	 */
	protected CustomDialog version;
	
	/**
	 * 
	 * {@link CustomDialog} object that displays information when the transformation is in progress.
	 */
	protected CustomDialog transform;

	/**
	 * 
	 * Will be run when the {@link Activity} is created.
	 * Defines what the {@link Handler} will do when it receivers different messages.
	 * Will also create all the {@link CustomDialog} objects.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (!HandlerActivity.this.isFinishing())
					switch(msg.what){
					case MSG_UPDATE:
						updateText();
						break;
					case MSG_SENDING_SETTINGS:
						showLoadingSettingsDialog();
						break;
					case MSG_DISCONNECT: 
						updateStatus();
						displayDisconnectMessage();
					case MSG_DISMISS:
						dismissSettingsDialog();
						break;
					case MSG_TRANSFORM: 
						showTransformError();
						break;
					case MSG_VERSION:
						checkVersion(msg.arg1);
						break;
					case MSG_DISTANCE:
						tooClose(msg.arg1);
						updateWarning();
						break;
					case MSG_SIGNAL:
						lowSignal(msg.arg1);
						updateWarning();
						break;
					case MSG_BATTERY:
						lowBattery(msg.arg1);
						updateWarning();
						break;
					case MSG_SHOW_TRANSFORM:
						showTransformDialog();
						break;
					case MSG_WARNING:
						updateWarning();
						break;
					case MSG_UPDATE_MENU:
						updateMenu((SearchArray) msg.obj);
						break;
					}
			}
		};
		init();
	}

	protected abstract void updateMenu(SearchArray values);

	/**
	 * 
	 * Registers this {@link Activity} as a listener for different messages in the {@link MessageHandler}.
	 */
	private void addListenersToMessageHandler() {
		MessageHandler.getInstance().addDisconnectListener(this);
		MessageHandler.getInstance().addSegwayListener(this);
		MessageHandler.getInstance().addVersionListener(this);
		MessageHandler.getInstance().addBatteryListener(this);
		MessageHandler.getInstance().addSignalListener(this);
	}

	/**
	 * 
	 * Method that will be run when the program looses / gains focus. 
	 * If the focus is gained and the application requests a calibration info message, it will be shown.
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		this.hasFocus = hasFocus;
		if (showLoading && hasFocus){
			showLoading = false;
			progress.show();
		}
		super.onWindowFocusChanged(hasFocus);
	}
	
	/**
	 * 
	 * Will be run when the application is paused. Makes sure to remove listeners from the {@link MessageHandler}.
	 * There is no need to listen for messages when this application is paused.
	 */
	@Override
	protected void onPause() {
		removeListenersFromMessageHandler();
		super.onPause();
	}

	/**
	 * 
	 * Removes the appropriate listeners from the {@link MessageHandler}. 
	 */
	private void removeListenersFromMessageHandler() {
		MessageHandler.getInstance().removeDisconnectListener(this);
		MessageHandler.getInstance().removeSegwayListener(this);
		MessageHandler.getInstance().removeVersionListener(this);
		MessageHandler.getInstance().removeBatteryListener(this);
		MessageHandler.getInstance().removeSignalListener(this);
	}

	/**
	 * 
	 * Adds the appropriate listeners to the {@link MessageHandler} when the application resumes.
	 */
	@Override
	protected void onResume() {
		addListenersToMessageHandler();
		super.onResume();
	}

	/**
	 * 
	 * Method that should be overridden in any extending {@link Activity}.
	 */
	protected abstract void updateStatus();
	
	/**
	 * 
	 * Method that should be overridden in any extending {@link Activity}.
	 */
	protected abstract void updateText();
	
	/**
	 * 
	 * Method that should be overridden in any extending {@link Activity}.
	 */
	protected abstract void tooClose(int size);
	
	/**
	 * 
	 * Method that should be overridden in any extending {@link Activity}.
	 */
	protected abstract void lowBattery(int battery);
	
	/**
	 * 
	 * Method that should be overridden in any extending {@link Activity}.
	 */
	protected abstract void lowSignal(int signal);
	
	/**
	 * 
	 * Method that should be overridden in any extending {@link Activity}.
	 */
	protected abstract void updateWarning();

	/**
	 * Method that will init all the {@link CustomDialog}s used in this application.
	 */
	private void init() {
		disconnectDialog = new CustomDialog(this, getString(R.string.disconnected_title), getString(R.string.disconnected_text));
		disconnectDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onDisconnectDialogFinish();
			}
		});
		progress = new CustomDialog(this, getString(R.string.transfering), getString(R.string.transfering_long), false);
		unComplete = new CustomDialog(this, getString(R.string.error), getString(R.string.transform_error));
		version = new CustomDialog(this, getString(R.string.information), getString(R.string.version));
		transform = new CustomDialog(this, getString(R.string.turning_to_segway), getString(R.string.turning_to_segway_long), false);
	}

	/**
	 * Will check if the current version is the same as the one received. Will display a dialog if the NXT is compiled for a different version.
	 * @param version The version the NXT is compiled for
	 */
	protected void checkVersion(int version) {
		if (!Version.isSameVersion(version))
			this.version.show();
	}
	
	protected abstract void onDisconnectDialogFinish();

	/**
	 * Will display a message if the NXT is disconnected from the Android device
	 */
	protected void displayDisconnectMessage(){
		dismissVisibleDialogs();
		disconnectDialog.show();
	}

	/**
	 * Will dismiss any visible dialog
	 */
	private void dismissVisibleDialogs() {
		if (version.isVisible())
			version.dismiss();
		if (progress.isVisible())
			progress.dismiss();
		if (unComplete.isVisible())
			unComplete.dismiss();
		if (transform.isVisible())
			transform.dismiss();
	}

	/**
	 * Method for setting the boolean variable showLoading to true. Indicates that the system is sending settings, and a loading dialog should become visible.
	 */
	protected void showLoadingSettingsDialog() {
		showLoading = true;
	}

	/**
	 * Will dismiss the progress dialog.
	 */
	protected void dismissSettingsDialog() {
		progress.dismiss();
	}

	/**
	 * Will show a dialog telling the user that something went wrong during transformation from one state to another in the NXT.
	 */
	protected void showTransformError(){
		unComplete.show();
	}
	/**
	 * Will show a dialog telling the user that the NXT is transforming.
	 */

	protected void showTransformDialog() {
		transform.show();
	}
	
	/**
	 * Dismisses the transform dialog
	 */
	protected void dismissTransformDialog() {
		transform.dismiss();
	}

	/**
	 * Listening method from a interface. Tells the system to display the settings dialog
	 */
	@Override
	public void onSetupStarted() {
		handler.sendEmptyMessage(MSG_SENDING_SETTINGS);
	}

	/**
	 * Listening method from a interface. Will tell the system to dismiss the loading dialog.
	 */
	@Override
	public void onCompleteCalibration() {
		showLoading = false;
		handler.sendEmptyMessage(MSG_DISMISS);
	}

	/**
	 * Listening method from a interface. Will tell the system to display the disconnected dialog
	 */
	@Override
	public void disconnect() {
		handler.sendEmptyMessage(MSG_DISCONNECT);
	}

	/**
	 * Listening method from a interface. Will dismiss the transform dialog and update the {@link Menu} status.
	 */
	@Override
	public void transformationComplete(SearchArray values) {
		dismissTransformDialog();
		if (values.get(Parameters.TYPE) == Values.SEGWAY_INTERUPT)
			handler.sendEmptyMessage(MSG_TRANSFORM);
		else {
			Message message = new Message();
			message.what = MSG_UPDATE_MENU;
			message.obj = values;
			handler.sendMessage(message);
		}
	}

	/**
	 * Listening method from a interface. Will tell the system to check if the version
	 */
	@Override
	public void onVersionReceived(SearchArray values) {
		Message message = new Message();
		message.what = MSG_VERSION;
		message.arg1 = values.get(Parameters.VERSION);
		handler.sendMessage(message);
	}

	/**
	 * Listening method from a interface. Will notify the system that the obstacle detector has detected a distance change.
	 */
	@Override
	public void distanceChanged(SearchArray values) {
		Message message = new Message();
		message.what = MSG_DISTANCE;
		message.arg1 = values.get(Parameters.DISTANCE);
		handler.sendMessage(message);
	}

	/**
	 * Listening method from a interface. Will notify the system that the NXT has low battery.
	 */
	@Override
	public void onLowBattery(SearchArray values) {
		Message message = new Message();
		message.what = MSG_BATTERY;
		message.arg1 = values.get(Parameters.BATTERY);
		handler.sendMessage(message);
	}

	/**
	 * Listening method from a interface. Will notify the system that the NXT has low signal.
	 */
	@Override
	public void onSignalChanged(SearchArray values) {
		Message message = new Message();
		message.what = MSG_SIGNAL;
		message.arg1 = values.get(Parameters.SIGNAL);
		handler.sendMessage(message);
	}

	/**
	 * Will display a dialog telling the user that the NXT is transforming.
	 */
	public void showTransformMessage(){
		handler.sendEmptyMessage(MSG_SHOW_TRANSFORM);
	}
	
	/**
	 * Will tell the system to update all warning messages.
	 */
	public void updateWarningMessages(){
		
		handler.sendEmptyMessage(MSG_WARNING);
	}

}
