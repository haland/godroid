package uis.bachelor.android.common;

import java.util.Locale;

import uis.bachelor.android.menu_language.LanguageMenuActivity;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
/**
 * 
 * Activity for handling language changes. Every application that want to use custom language must extend this class.
 * @author Eirik Heskja
 *
 */
public abstract class LanguageActivity extends Activity {
	
	/**
	 * 
	 * Used to access the saved preferences and find the language selected by the user. 
	 */
	protected SharedPreferences preferences;
	
	/**
	 * 
	 * Key that defines where you find the stored language. 
	 */
	public static final String lang_key = "lang";
	
	/**
	 * 
	 * Will run when the activity is created. Will instantiate the {@link SharedPreferences} object and update language and UI. 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		setLanguage();
		updateUI();
	}
	
	/**
	 * 
	 * Will find the saved language and force the {@link Activity}s resource to use a different language, defined in a new {@link Locale} object. 
	 */
	private void setLanguage() {
		String language = preferences.getString(lang_key, LanguageMenuActivity.norwegian);
		if(language.equalsIgnoreCase(LanguageMenuActivity.norwegian))
			language = "no_NO";
		if(language == LanguageMenuActivity.english)
			language = "en_US";

		Locale locale = new Locale(language);
		Locale.setDefault(locale);

		getResources().getConfiguration().locale = locale;
		getResources().updateConfiguration(getResources().getConfiguration(), getResources().getDisplayMetrics());
	}
	
	/**
	 * 
	 * Method that will make sure that the correct language is set when the {@link Activity} gains and looses focus.
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		setLanguage();
		updateUI();
		super.onWindowFocusChanged(hasFocus);
	}
	
	/**
	 * 
	 * Method that should be implemented in any sub-classes that want the user interface to be updated and use the correct language. 
	 */
	protected void updateUI(){}


}
