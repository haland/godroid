package uis.bachelor.android.common.model.communication.bluetooth;

import java.io.DataInputStream;
import java.io.IOException;

import array.SearchArrayDecoder;

import standard.Flags;
import uis.bachelor.android.common.model.communication.MessageHandler;
/**
 * Class used to handle incoming messages from the NXT.
 * @author Eirik Heskja
 *
 */
public class BluetoothInputThread implements Runnable {
	/**
	 * Boolean variable telling if the {@link Thread} is running.
	 */
	private boolean isRunning;
	
	/**
	 * The {@link Thread} that does the reading.
	 */
	private Thread thread;

	/**
	 * The connection used between the Android and the NXT.
	 */
	private BluetoothConnection connection;
	
	/**
	 * The inputStream used to read messages.
	 */
	private DataInputStream input;

	/**
	 * Default constructor. Will create a new input object and a new thread.
	 * @param connection The {@link BluetoothConnection} used for communication.
	 * @throws IOException Will be thrown if the connection is closed.
	 */
	public BluetoothInputThread(BluetoothConnection connection) throws IOException {
		this.connection = connection;
		isRunning = false;
		thread = new Thread(this);
		input = new DataInputStream(connection.getSocket().getInputStream());
	}

	/**
	 * Starts the thread if it is not alive.
	 */
	public void start() {
		isRunning = true;
		if (!thread.isAlive())
			thread.start();
	}
	
	/**
	 * Will result in terminating the thread. 
	 */
	public void disconnect() {
		isRunning = false;
	}

	/**
	 * Waits for input from the {@link DataInputStream}, reads the string, then gives the input to the {@link MessageHandler}.
	 * Sleeps for 100ms between each loop.
	 */
	public void run() {
		while (isRunning) {
			if (connection.isConnected()) {
				try {
					MessageHandler.getInstance().setIncommingMessage(input.readUTF());
				} catch (IOException e1) {
					isRunning = false;
					MessageHandler.getInstance().setIncommingMessage(SearchArrayDecoder.encode(Flags.MSG_DISCONNECT, null));
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
		}
	}



}
