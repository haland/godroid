package uis.bachelor.android.common.model.communication.buffers;

/**
 * A Buffer that does not implement any kind of Semaphore or similar.
 * Overrides any object if the space is full.
 * @author Eirik Heskja
 *
 */
public class Buffer {

	private Object[] elements;
	private int in, out;
	private int capacity, size;

	/**
	 * Default constructor. 
	 * @param capacity Sets how many items there should be in this que.
	 */
	public Buffer(int capacity){
		this.capacity = capacity;
		elements = new Object[capacity];
		in = out = size = 0;
	}
	
	/**
	 * Puts the object into the buffer.
	 * @param o The object to put
	 */
	public void put(Object o){
		elements[in % capacity] = o;
		in++;
		size++;
	}

	/**
	 * Gets the last object from the buffer.
	 * @return
	 */
	public Object get(){ 
		Object o = null;
		int idx = out % capacity;
		o = elements[idx];
		out++;
		size--;
		
		elements[idx] = null;

		return o;
	}

	/**
	 * Check if the buffer is empty.
	 * @return true if the size is 0, else false.
	 */
	public boolean isEmpty(){
		return size == 0;
	}

	/**
	 * Returns the size of the buffer
	 * @return current buffersize.
	 */
	public int size() {
		return size;
	}

	/**
	 * Sets all the elements to null.
	 */
	public void clear() {
		for (int i = 0; i < elements.length; i++)
			elements[i] = null;
		size = 0;
	}



}
