package uis.bachelor.android.common.model.communication.bluetooth;

import java.io.DataOutputStream;
import java.io.IOException;

import uis.bachelor.android.common.model.communication.MessageHandler;
/**
 * Class used to handle outgoing messages to the NXT.
 * @author Eirik Heskja
 *
 */
public class BluetoothOutputThread implements Runnable {
	
	/**
	 * Boolean variable telling if the {@link Thread} is running.
	 */
	private boolean isRunning;
	
	/**
	 * The {@link Thread} that does the reading.
	 */
	private Thread thread;

	/**
	 * The connection used between the Android and the NXT.
	 */
	private BluetoothConnection connection;
	
	/**
	 * The output used to send messages.
	 */
	private DataOutputStream out;

	/**
	 * Default constructor. Will create a new output and thread object.
	 * @param connection
	 * @throws IOException
	 */
	public BluetoothOutputThread(BluetoothConnection connection) throws IOException {
		this.connection = connection;
		isRunning = false;
		thread = new Thread(this);
		out = new DataOutputStream(connection.getSocket().getOutputStream());
	}
	
	/**
	 * Starts the thread if it isn't already started.
	 */
	public void start() {
		isRunning = true;
		if (!thread.isAlive())
			thread.start();
	}
	
	/**
	 * Will get the next message in the que from the {@link MessageHandler}, and send it to the NXT. 
	 * This method used blocked wait. 
	 */
	public void run() {
		while (isRunning) {
			if (connection.isConnected()) {
				try {
					String output = MessageHandler.getInstance().getNextMessage();
					if (output != null){
						out.writeUTF(output);
						out.flush();
					}
				} catch (IOException e1) {
					isRunning = false;
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	/**
	 * Used to stop this thread from running.
	 */
	public void disconnect() {
		isRunning = false;
	}

}
