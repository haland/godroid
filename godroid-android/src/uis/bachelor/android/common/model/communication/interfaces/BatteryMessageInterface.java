package uis.bachelor.android.common.model.communication.interfaces;

import array.SearchArray;

/**
 * Interface used for warning messages
 * @author Eirik Heskja
 *
 */
public interface BatteryMessageInterface {
	/**
	 * Interface method. Will be called when the battery is low on the NXT.
	 * @param values The new battery value.
	 */
	public void onLowBattery(SearchArray values);
}
