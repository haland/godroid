package uis.bachelor.android.common.model.communication.interfaces;

import array.SearchArray;
/**
 * Interface used in Bluetooth communication.
 * Implementing classes can listen for version messages.
 * @author Eirik Heskja
 *
 */
public interface VersionMessageInterface {
	/**
	 * Method executed when the NXT connects to the Android. Will be used to check the application version.
	 * @param values Parameters holding the NXT version
	 */
	public void onVersionReceived(SearchArray values);
}
