package uis.bachelor.android.common.model.communication.interfaces;

import array.SearchArray;
/**
 * Interface used in Bluetooth communication.
 * @author Eirik Heskja
 *
 */
public interface BluetoothErrorMessageInterface {
	/**
	 * Used when the Android device receives a error message from the NXT.
	 * @param array The parameters holding the error message.
	 */
	public void messageReceived(SearchArray array);
}
