package uis.bachelor.android.common.model.communication;

import java.util.ArrayList;

import array.SearchArray;

import uis.bachelor.android.common.model.communication.interfaces.BatteryMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.BluetoothDisconnectMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.DistanceMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.SignalMessageInterface;
import uis.bachelor.android.common.model.communication.interfaces.VersionMessageInterface;
import uis.bachelor.android.menu_controller.model.interfaces.SegwayMessageInterface;
import uis.bachelor.android.menu_settings.model.interfaces.CalibrationCompleteInterface;
import uis.bachelor.android.menu_settings.model.interfaces.SettingAndCalibrationStartedInterface;
/**
 * Class used to handle any messages received from the NXT.
 * @author Eirik Heskja
 *
 */
public class NotificationHandler {
	/**
	 * List of {@link BluetoothDisconnectMessageInterface} listeners.
	 */
	private ArrayList<BluetoothDisconnectMessageInterface> disconnectListeners;
	
	/**
	 * List of {@link SettingAndCalibrationStartedInterface} listeners.
	 */
	private ArrayList<SettingAndCalibrationStartedInterface> settingsListeners;
	
	/**
	 * List of {@link CalibrationCompleteInterface} listeners.
	 */
	private ArrayList<CalibrationCompleteInterface> calibraionListeners;
	
	/**
	 * List of {@link SegwayMessageInterface} listeners.
	 */
	private ArrayList<SegwayMessageInterface> segwayListeners;
	
	/**
	 * List of {@link DistanceMessageInterface} listeners.
	 */
	private ArrayList<DistanceMessageInterface> distanceListeners;
	
	/**
	 * List of {@link VersionMessageInterface} listeners.
	 */
	private ArrayList<VersionMessageInterface> versionListeners;
	
	/**
	 * List of {@link SignalMessageInterface} listeners.
	 */
	private ArrayList<SignalMessageInterface> signalListeners;
	
	/**
	 * List of {@link BatteryMessageInterface} listeners.
	 */
	private ArrayList<BatteryMessageInterface> batteryListeners;
	
	/**
	 * Default constructor. Will create all the lists.
	 */
	protected NotificationHandler(){
		disconnectListeners = new ArrayList<BluetoothDisconnectMessageInterface>();
		settingsListeners = new ArrayList<SettingAndCalibrationStartedInterface>();
		calibraionListeners = new ArrayList<CalibrationCompleteInterface>();
		segwayListeners = new ArrayList<SegwayMessageInterface>();
		distanceListeners = new ArrayList<DistanceMessageInterface>();
		versionListeners = new ArrayList<VersionMessageInterface>();
		signalListeners = new ArrayList<SignalMessageInterface>();
		batteryListeners = new ArrayList<BatteryMessageInterface>();
	}
	
	/**
	 * Used to notify the {@link BatteryMessageInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifyBatteryListeners(SearchArray values) {
		for (BatteryMessageInterface listener : batteryListeners)
			listener.onLowBattery(values);
	}
	
	/**
	 * Used to add {@link BatteryMessageInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addBatteryListener(BatteryMessageInterface listener){
		if (!batteryListeners.contains(listener))
			batteryListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link BatteryMessageInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeBatteryListener(BatteryMessageInterface listener){
		if (batteryListeners.contains(listener))
			batteryListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link SignalMessageInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifySignalListeners(SearchArray values) {
		for (SignalMessageInterface listener : signalListeners)
			listener.onSignalChanged(values);
	}
	
	/**
	 * Used to add {@link SignalMessageInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addSignalListener(SignalMessageInterface listener){
		if (!signalListeners.contains(listener))
			signalListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link SignalMessageInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeSignalListener(SignalMessageInterface listener){
		if (signalListeners.contains(listener))
			signalListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link VersionMessageInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifyVersionListeners(SearchArray values) {
		for (VersionMessageInterface listener : versionListeners)
			listener.onVersionReceived(values);
	}
	
	/**
	 * Used to add {@link VersionMessageInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addVersionListener(VersionMessageInterface listener){
		if (!versionListeners.contains(listener))
			versionListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link VersionMessageInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeVersionListener(VersionMessageInterface listener){
		if (versionListeners.contains(listener))
			versionListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link DistanceMessageInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifyDistanceListeners(SearchArray values) {
		for (DistanceMessageInterface listener : distanceListeners)
			listener.distanceChanged(values);
	}
	
	/**
	 * Used to add {@link DistanceMessageInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addDistanceListener(DistanceMessageInterface listener){
		if (!distanceListeners.contains(listener))
			distanceListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link DistanceMessageInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeDistanceListener(DistanceMessageInterface listener){
		if (distanceListeners.contains(listener))
			distanceListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link BluetoothDisconnectMessageInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifyDisconnectListeners(){
		for (BluetoothDisconnectMessageInterface listener : disconnectListeners)
			listener.disconnect();
	}
	
	/**
	 * Used to add {@link BluetoothDisconnectMessageInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addDisconnectListener(BluetoothDisconnectMessageInterface listener){
		if (!disconnectListeners.contains(listener))
			disconnectListeners.add(listener);
	}

	/**
	 * Used to remove a {@link BluetoothDisconnectMessageInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeDisconnectListener(BluetoothDisconnectMessageInterface listener) {
		if (disconnectListeners.contains(listener))
			disconnectListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link SettingAndCalibrationStartedInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifySettingsListeners(){
		for (SettingAndCalibrationStartedInterface listener : settingsListeners) {
			listener.onSetupStarted();
		}
	}
	
	/**
	 * Used to add {@link SettingAndCalibrationStartedInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addSettingsListener(SettingAndCalibrationStartedInterface listener){
		if (!settingsListeners.contains(listener))
			settingsListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link SettingAndCalibrationStartedInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeSettingsListener(SettingAndCalibrationStartedInterface listener){
		if (settingsListeners.contains(listener))
			settingsListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link CalibrationCompleteInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifyCalibrationListeners() {
		for (CalibrationCompleteInterface listener : calibraionListeners)
			listener.onCompleteCalibration();
	}
	
	/**
	 * Used to add {@link CalibrationCompleteInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addCalibrationListener(CalibrationCompleteInterface listener){
		if (!calibraionListeners.contains(listener))
			calibraionListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link CalibrationCompleteInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeCalibrationListener(CalibrationCompleteInterface listener){
		if (calibraionListeners.contains(listener))
			calibraionListeners.remove(listener);
	}
	
	/**
	 * Used to notify the {@link SegwayMessageInterface} listeners.
	 * @param values {@link SearchArray} holding the parameters.
	 */
	protected void notifySegwayListeners(SearchArray values) {
		for (SegwayMessageInterface listener : segwayListeners)
			listener.transformationComplete(values);
	}
	
	/**
	 * Used to add {@link SegwayMessageInterface} listeners. If the listener is in the list, it will not be added.
	 * @param listener The listener to be added.
	 */
	public void addSegwayListener(SegwayMessageInterface listener){
		if (!segwayListeners.contains(listener))
			segwayListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link SegwayMessageInterface} listener. 
	 * @param listener The listener to remove
	 */
	public void removeSegwayListener(SegwayMessageInterface listener){
		if (segwayListeners.contains(listener))
			segwayListeners.remove(listener);
	}
	
	/**
	 * Used to remove all {@link SegwayMessageInterface} listeners.
	 */
	public void removeSegwayListeners(){
		segwayListeners.clear();
	}
}
