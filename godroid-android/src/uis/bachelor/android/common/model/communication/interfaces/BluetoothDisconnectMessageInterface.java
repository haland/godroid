package uis.bachelor.android.common.model.communication.interfaces;
/**
 * Interface used in Bluetooth communication. Used when the NXT disconnects from the android.
 * @author Eirik Heskja
 *
 */
public interface BluetoothDisconnectMessageInterface {
	/**
	 * Will tell all implementing classes that the connection has been lost. 
	 */
	public void disconnect();
}
