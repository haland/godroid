package uis.bachelor.android.common.model.communication.interfaces;

import array.SearchArray;

/**
 * Interface used for warning messages
 * @author Eirik Heskja
 *
 */
public interface DistanceMessageInterface {
	/**
	 * Interface method. Will be called when the distance has changed from the ultrasonic senosor on the NXT.
	 * @param values The new sensor value.
	 */
	public void distanceChanged(SearchArray values);

}
