package uis.bachelor.android.common.model.communication.interfaces;

import array.SearchArray;

/**
 * Interface used for warning messages
 * @author Eirik Heskja
 *
 */
public interface SignalMessageInterface {
	/**
	 * Interface method. Called when the signal has changed on the NXT.
	 * @param values The new signal value
	 */
	public void onSignalChanged(SearchArray values);
}
