package uis.bachelor.android.common.model.communication;

import java.util.ArrayList;

import concurrent.Semaphore;

import standard.Flags;
import standard.exception.NoFlagException;
import uis.bachelor.android.common.model.communication.bluetooth.BluetoothConnection;
import uis.bachelor.android.common.model.communication.buffers.Buffer;
import uis.bachelor.android.menu_bluetooth.model.listeners.onClick.DisconnectFromNXTOnClickListener;
import uis.bachelor.android.menu_main.MainMenuActivity;
import android.bluetooth.BluetoothDevice;
import array.SearchArray;
import array.SearchArrayDecoder;
/**
 * Class used to handle all incoming and outgoing messages from/to the NXT.
 * @author Eirik Heskja
 *
 */
public class MessageHandler extends NotificationHandler {
	/**
	 * The {@link BluetoothConnection} used for communication.
	 */
	private BluetoothConnection bluetooth;
	
	/**
	 * {@link Semaphore}s used for blocking any {@link Runnable} object if the system has to wait for the NXT.
	 */
	private Semaphore sendSemaphore, messageSemaphore;
	
	/**
	 * The {@link Buffer} holding all non-priority messages. Bounded, with size = 1.
	 */
	private Buffer messageBuffer;
	
	/**
	 * A {@link ArrayList} holding all the priority messages.
	 */
	private ArrayList<String> priorityBuffer;
	
	/**
	 * Private {@link MessageHandler} instance.
	 */
	private static MessageHandler instance;
	
	/**
	 * Max number of messages in the bounded messageBuffer.
	 */
	private final int msgQueSize = 1;
	
	/**
	 * Boolean variable telling if there is any waiting threads. 
	 */
	private boolean waiting;
	
	/**
	 * Public method to get the {@link MessageHandler} instance.
	 * If it is null, it will be instantiated. 
	 * @return A useable {@link MessageHandler}-object.
	 */
	public static MessageHandler getInstance(){
		if (instance == null)
			instance = new MessageHandler();
		return instance;
	}
	
	/**
	 * Default constructor. Creates all the buffers and semaphores, and starts the timeout thread.
	 */
	private MessageHandler(){
		super();
		sendSemaphore = new Semaphore(1);
		messageSemaphore = new Semaphore();
		messageBuffer = new Buffer(msgQueSize);
		priorityBuffer = new ArrayList<String>();
		waiting = true;
	}

	/**
	 * Method used to put a message in que. If flag has priority, the message will be added in the priority que. Releases the messageSemaphore if any thread is waiting.
	 * @param flag {@link Flags} defining the message type
	 * @param params The {@link SearchArray} holding the message parameters.
	 */
	public void putMessage(int flag, SearchArray params){
		try {
			Flags.isFlag(flag);
			putInQue(flag, params);
			if (waiting || Flags.hasPriority(flag))
				messageSemaphore.release();
		} catch (NoFlagException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Will put the message in the correct que. Encodes the parameters to a string first. 
	 * @param flag {@link Flags} defining the message type
	 * @param params The {@link SearchArray} holding the message parameters.
	 */
	private void putInQue(int flag, SearchArray params) {
		String obj = SearchArrayDecoder.encode(flag, params);
		if (MainMenuActivity.DEBUG)
			System.out.println("in: "+obj);
		if (Flags.hasPriority(flag))
			priorityBuffer.add(obj);
		else 
			messageBuffer.put(obj);
	}
	
	/**
	 * Public method to get the next message from the que. If there is a message in the priority que, it will be removed first.
	 * @return The String representation of the next message. 
	 */
	public String getNextMessage(){
		String o = null;
		sendSemaphore.obtain();
		waiting = true;
		messageSemaphore.obtain();
		if (!priorityBuffer.isEmpty())
			o = priorityBuffer.remove(0);
		else
			o = (String) messageBuffer.get();
		if (MainMenuActivity.DEBUG)
			System.out.println("out: "+o);
		waiting = false;
		return o;
	}
	
	/**
	 * Method used to receive a message from the NXT. Will separate the flag from the parameters and decode the message.
	 * @param message The received message.
	 */
	public void setIncommingMessage(String message){
		int cmd = SearchArrayDecoder.decode_command(message);
		SearchArray values = SearchArrayDecoder.decode_parameters(message);
		handleMessage(cmd, values);
	}

	/**
	 * Will notify the correct listeners.
	 * @param cmd The flag of the received message
	 * @param values The {@link SearchArray} parameters of the message
	 */
	private void handleMessage(int cmd, SearchArray values) {
		switch(cmd){
		case Flags.MSG_OK: sendSemaphore.release(); break;
		case Flags.MSG_ERROR:
		case Flags.MSG_DISCONNECT: disconnectWitNotification(); break;
		case Flags.MSG_SEGWAY: notifySegwayListeners(values); break;
		case Flags.MSG_CALIBRATED: notifyCalibrationListeners(); break;
		case Flags.MSG_SENDING_SETTINGS: notifySettingsListeners(); break;
		case Flags.MSG_DISTANCE: notifyDistanceListeners(values); break;
		case Flags.MSG_VERSION: notifyVersionListeners(values); break;
		case Flags.MSG_BATTERY: notifyBatteryListeners(values);break;
		case Flags.MSG_SIGNAL: notifySignalListeners(values); break;
		}
	}
	
	/**
	 * Disconnects from the {@link BluetoothConnection} and notifies any {@link DisconnectFromNXTOnClickListener} listeners.
	 */
	private void disconnectWitNotification() {
		disconnect();
		notifyDisconnectListeners();
	}
	
	/**
	 * Cleas the buffers.
	 */
	private void clearBuffers() {
		if (messageBuffer != null)
			messageBuffer.clear();
		if (priorityBuffer != null)
			priorityBuffer.clear();
	}
	
	/**
	 * Disconnects from the {@link BluetoothConnection}. Creates new {@link Semaphore} objects, so the system will be ready for a new connection. Will also clear any buffer.
	 */
	private void disconnect(){
		if (bluetooth != null && bluetooth.isConnected())
			bluetooth.disconnect();
		sendSemaphore = new Semaphore(1);
		messageSemaphore = new Semaphore();
		clearBuffers();
	}

	/**
	 * Used to open a new connection to a {@link BluetoothDevice}.
	 * @param device The {@link BluetoothDevice} to connect to.
	 * @return integer representation of the {@link Status}.
	 */
	public int openConnection(BluetoothDevice device) {
		bluetooth = new BluetoothConnection(device);
		return bluetooth.connect();
	}

	/**
	 * Checks if there is a connection
	 * @return If connected, return true. Else, return false.
	 */
	public boolean isConnected() {
		if (bluetooth == null)
			return false;
		return bluetooth.isConnected();
	}

	/**
	 * Public method to clear any message que.
	 */
	public void clearQue() {
		clearBuffers();
	}
	
}