package uis.bachelor.android.common.model.communication.bluetooth;

import java.io.IOException;
import java.util.UUID;

import standard.Values;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
/**
 * Class used to create and maintain a Bluetooth connection.
 * @author Eirik Heskja
 *
 */
public class BluetoothConnection {
	/**
	 * The SERIAL_PORT_UUID used by the NXT Bluetooth Adapter
	 */
	private final String SERIAL_PORT_UUID = "00001101-0000-1000-8000-00805F9B34FB";
	
	/**
	 * Boolean variable telling if there is a connection or not.
	 */
	private boolean connected;
	
	/**
	 * The Socket used for communication.
	 */
	private BluetoothSocket socket;
	
	/**
	 * The device to communicate with
	 */
	private BluetoothDevice device;
	
	/**
	 * The input thread, used to receive messages in a separate thread.
	 */
	private BluetoothOutputThread output;
	
	/**
	 * The output thread, used to send messages in a separate thread.
	 */
	private BluetoothInputThread input;
	
	/**
	 * Default constructor. Initializes the device.
	 * @param device The device used in the communication.
	 */
	public BluetoothConnection(BluetoothDevice device) {
		this.device = device;
	}
	
	/**
	 * Public method ro retreive the connection status.
	 * @return True if connected, else false.
	 */
	public boolean isConnected(){
		return connected;
	}
	
	/**
	 * Will create the socket, cancel device discovery (if any), and connect the socket. After the socekt is created, it will start the input and output threads.
	 * @return {@link Status}.STATUS_CONNECTED if connection is successfull, else it will return {@link Status}.STATS_CONNECTION_ERRROR
	 */
	public int connect(){
		
		try {
			socket =  device.createRfcommSocketToServiceRecord(UUID.fromString(SERIAL_PORT_UUID));
			BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
			socket.connect();
			connected = true;
			startInputAndOutput();
			return Values.STATUS_CONNECTED;
		} catch (IOException e) {
			connected = false;
			return Values.STATUS_CONNECTION_ERROR;
		}
	}
	
	/**
	 * Will close the socket and stop the running threads.
	 */
	public void disconnect(){
		try {
			socket.close();
			output.disconnect();
			input.disconnect();
			connected = false;
		} catch (IOException e) {
		}
	}
	
	/**
	 * Public method to get the output thread used.
	 * @return The {@link BluetoothOutputThread} used.
	 */
	public BluetoothOutputThread getOutput(){
		return output;
	}
	
	/**
	 * Public method to get the socket used
	 * @return The {@link Socket} used.
	 */
	public BluetoothSocket getSocket(){
		return socket;
	}

	/**
	 * Private method that will start both the input and output thread.
	 * @throws IOException Thrown by either {@link BluetoothOutputThread} or {@link BluetoothInputThread} if the socket is not connected.
	 */
	private void startInputAndOutput() throws IOException {
			output = new BluetoothOutputThread(this);
			input = new BluetoothInputThread(this);

			output.start();
			input.start();
	}

}
