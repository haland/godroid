package uis.bachelor.android.common.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import standard.Flags;
import uis.bachelor.android.common.model.communication.MessageHandler;
/**
 * Class used to logg exceptions.
 * @author Eirik Heskja and http://stackoverflow.com/questions/601503/how-do-i-obtain-crash-data-from-my-android-application
 *
 */
public class Logger implements UncaughtExceptionHandler{
	/**
	 * Static Logger instance.
	 */
	private static Logger instance;

	/**
	 * Default {@link UncaughtExceptionHandler}. Used to make sure the system handles UncaughtExceptions.
	 */
	private UncaughtExceptionHandler defaultHander;

	/**
	 * String variables defining the default save path and upload path for the created logs.
	 */
	private String path, url;
	
	/**
	 * Boolean variable used to define wether the Android device should disconnect from the NXT when the system experiences a UncaughtException.
	 */
	private boolean disconnect;
	/**
	 * Creates a Logger object if its null.
	 * Kilde: http://stackoverflow.com/questions/601503/how-do-i-obtain-crash-data-from-my-android-application
	 * @return Logger object.
	 */
	public static Logger getInstance(boolean b){
		if (instance == null)
			instance = new Logger(b);
		return instance;
	}
	/**
	 * Public method for manually logging a message
	 * @param tag The tag to use in the log file.
	 * @param e The {@link Throwable} event to log.
	 */
	public static void Log(String tag, Throwable e){
		tag = tag.replace(":", "-");
		getInstance(false).LogToServer(tag, e);
		getInstance(false).LogToFile(tag,e);
	}
	
	/**
	 * Default constructor
	 * @param b true if the system should disconnect from any NXT if a UncaughtException occurs.
	 */
	private Logger(boolean b){
		disconnect = b;
		defaultHander = Thread.getDefaultUncaughtExceptionHandler();
		path = "/sdcard/uis/error";
		url = "http://www.ux.uis.no/~heskja/uis/upload.php";
	}
	
	/**
	 * Public method for manually logging a message
	 * @param tag The tag to use in the log file
	 * @param message The string to log.
	 */
	public static void Log(String tag, String message){
		getInstance(false).writeToFile(message, tag+".stacktrace");
		getInstance(false).sendToServer(message, tag+".stacktrace");
	}
	
	/**
	 * Method responsible for creating the filenames and calling the write method.
	 * @param tag The tag to use in the log file
	 * @param e The throwable event to log
	 */
	private void LogToFile(String tag, Throwable e){
		String[] values = handleException(e);
		String stacktrace = values[0];
		String filename = tag+" "+values[1];
		writeToFile(stacktrace, filename);
	}
	
	/**
	 * Method responsible for creating the filenames and calling the send method
	 * @param tag The tag to use in the log file
	 * @param e The throwable event to log
	 */
	private void LogToServer(String tag, Throwable e){
		String[] values = handleException(e);
		String stacktrace = values[0];
		String filename = tag+" "+values[1];
		sendToServer(stacktrace, filename);
	}

	/**
	 * Method responsible for saving a log file to disk
	 * @param stacktrace The string representation of the stacktrace
	 * @param filename The filename to save the file as
	 */
	private void writeToFile(String stacktrace, String filename) {
		try {
			File paths = new File(path);
			paths.mkdirs();
			File file = new File(path,filename);
			FileOutputStream out = new FileOutputStream(file);
			OutputStreamWriter bos = new OutputStreamWriter(out);
			bos.write(stacktrace);
			bos.flush();
			bos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Method responsibe for posting the log file to server
	 * @param stacktrace The string representation of the stacktrace
	 * @param filename THe filename to save the file as
	 */
	private void sendToServer(String stacktrace, String filename) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("filename", filename));
		nvps.add(new BasicNameValuePair("stacktrace", stacktrace));
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			httpClient.execute(httpPost);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inherited method. Used to log unhandled expetions.
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		Log("Unhandled", ex);
		
		if (disconnect)
			MessageHandler.getInstance().putMessage(Flags.MSG_DISCONNECT, null);
		
		defaultHander.uncaughtException(thread, ex);
	}

	/**
	 * Will generate a string representation of the {@link Throwable} stacktrace and a timestamped filename
	 * @param ex The throwable exception to log
	 * @return String array with size two. Index 0 is the stacktrace, and index 1 is the timestamped filename.
	 */
	private String[] handleException(Throwable ex) {
		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy HH.mm.ss");
		String timestamp = s.format(new Date());
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		ex.printStackTrace(printWriter);
		String stacktrace = result.toString();
		printWriter.close();
		String filename = timestamp + ".stacktrace";
		String[] values = {stacktrace, filename};
		return values;
		
	}

}
