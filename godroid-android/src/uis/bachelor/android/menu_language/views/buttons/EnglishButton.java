package uis.bachelor.android.menu_language.views.buttons;

import uis.bachelor.android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * Class representing the English button on the language menu.
 * @author Eirik Heskja
 *
 */
public class EnglishButton extends LanguageButton {
	
	/**
	 * Default constructor
	 * @param context The context holding this button.
	 * @param attrs The attributes defined in XML.
	 */
	public EnglishButton(Context context, AttributeSet attrs) {
		super(context, attrs, LanguageButton.RIGHT);
	}

	/**
	 * Will draw the background according to pressed state.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		setBackgroundDrawable(getResources().getDrawable( isPressed() ? R.drawable.language_english_flag_pressed : R.drawable.language_english_flag_unpressed ));
		super.onDraw(canvas);
	}
}
