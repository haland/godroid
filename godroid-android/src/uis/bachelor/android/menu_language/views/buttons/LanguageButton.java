package uis.bachelor.android.menu_language.views.buttons;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**
 * Class used for buttons on the language menu.
 * @author Chris H�land
 *
 */
public class LanguageButton extends Button {
	
	/**
	 * Integer used for defining that the button is on the left side.
	 */
	public static final int LEFT = 0;
	
	/**
	 * Integer used for defining that the button is on the right side.
	 */
	public static final int RIGHT = 1;
	
	/**
	 * Integer representation of the layout position.
	 */
	private int layout;
	
	/**
	 * Boolean variable telling of the button has resized or not.
	 */
	private boolean hasResized;
	
	/**
	 * Default constructor.
	 * @param context The context holding the button
	 * @param attrs The Attributes defined in XML.
	 * @param layout Either LEFT or RIGHT
	 */
	public LanguageButton(Context context, AttributeSet attrs, int layout) {
		super(context, attrs);
		this.layout = layout;
		hasResized = false;
	}
	
	/**
	 * Will resize the button and set the appropriate margins according to defined layout.
	 */
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if (!hasResized) {
			LinearLayout.LayoutParams layoutParameters = (LayoutParams) getLayoutParams();
			layoutParameters.width = (int) (getWidth() * 0.46);
			layoutParameters.height = (int) (layoutParameters.width * 0.73);
			
			switch(layout) {
				case RIGHT:
					layoutParameters.leftMargin = (int) (getWidth() * 0.52);
					break;
				case LEFT:
					layoutParameters.leftMargin = (int) (getWidth() * 0.02);
					break;
			}
			
			setLayoutParams(layoutParameters);
			hasResized = true;
		}
	}
}
