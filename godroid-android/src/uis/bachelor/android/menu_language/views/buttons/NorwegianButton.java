package uis.bachelor.android.menu_language.views.buttons;

import uis.bachelor.android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * Button used in the language menu.
 * @author Eirik Heskja
 *
 */
public class NorwegianButton extends LanguageButton {
	
	/**
	 * Default constructor.
	 * @param context The context holding the button.
	 * @param attrs The attributes defined in XML.
	 */
	public NorwegianButton(Context context, AttributeSet attrs) {
		super(context, attrs, LanguageButton.LEFT);
	}

	/**
	 * Will draw the background according to it's presses state.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		setBackgroundDrawable(getResources().getDrawable( isPressed() ? R.drawable.language_norwegian_flag_pressed : R.drawable.language_norwegian_flag_unpressed ));
		super.onDraw(canvas);
	}
}
