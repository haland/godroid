package uis.bachelor.android.menu_language.model.listeners.onClick;

import uis.bachelor.android.R;
import uis.bachelor.android.common.LanguageActivity;
import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
/**
 * Class used to set the language preferences when a flag is clicked in the language menu.
 * @author Eirik Heskja
 *
 */
public class LanguageButtonOnClickListener implements OnClickListener {
	/**
	 * Text representation of the button clicked
	 */
	private String text;
	
	/**
	 * The activity holding the {@link TextView}.
	 */
	private Activity activity;
	
	/**
	 * The {@link SharedPreferences} holding the language preference
	 */
	private SharedPreferences preferences;
	
	/**
	 * Default constructor. Will instantiate the text and the activitiy.
	 * @param text
	 * @param activity
	 */
	public LanguageButtonOnClickListener(String text, Activity activity){
		this.text = text;
		this.activity = activity;
	}
	
	/**
	 * Inherited method. Will set the preference "language" according to the text in this class.
	 */
	public void onClick(View v) {
		TextView selected_text = (TextView) activity.findViewById(R.id.selected_lang_text);
		activity.findViewById(R.id.saveLanguageButton).setEnabled(true);
		selected_text.setText(text);
		preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		preferences.edit().putString(LanguageActivity.lang_key, text).commit();
	}

}
