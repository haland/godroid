package uis.bachelor.android.menu_language.model.listeners.onClick;

import uis.bachelor.android.R;
import uis.bachelor.android.menu_main.MainMenuActivity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
/**
 * Class used when in the language menu. Will set the preferences and start a new activity when clicked.
 * @author Eirik Heskja
 *
 */
public class LanguageContinueOnClickListener implements OnClickListener {
	/**
	 * The activity holding the {@link CheckBox} view.
	 */
	private Activity activity;
	
	/**
	 * The {@link SharedPreferences} holding the default language preference
	 */
	private SharedPreferences preferences;
	
	/**
	 * The intent used to start the new {@link Activity}
	 */
	private Intent intent;
	
	/**
	 * Default constructor. Will instantiate the preferences and the intent.
	 * @param activity
	 */
	public LanguageContinueOnClickListener(Activity activity){
		this.activity = activity;
		this.preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		intent = new Intent(activity, MainMenuActivity.class);
	}
	
	/**
	 * Inherited method from {@link OnClickListener}. Will set the preferences and start a new activity when clicked.
	 */
	public void onClick(View v) {		
		boolean default_language = ((CheckBox) activity.findViewById(R.id.defaultLanguageChecked)).isChecked();
		preferences.edit().putBoolean("default_language", default_language).commit();
		activity.startActivity(intent);
		activity.finish();
	}

}
