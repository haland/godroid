package uis.bachelor.android.menu_language;

import java.lang.Thread.UncaughtExceptionHandler;

import uis.bachelor.android.R;
import uis.bachelor.android.common.model.Logger;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.menu_language.model.listeners.onClick.LanguageButtonOnClickListener;
import uis.bachelor.android.menu_language.model.listeners.onClick.LanguageContinueOnClickListener;
import uis.bachelor.android.menu_main.MainMenuActivity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Button;

/**
 * 
 * The default welcome activity in this application. Will display a language select menu if no default language is set.
 * @author Eirik Heskja
 *
 */
public class LanguageMenuActivity extends Activity {
   
	/**
	 * 
	 * String representing the Norwegian language option.
	 */
	public static final String norwegian = "Norsk";
	
	/**
	 * 
	 * String representing the English language option.
	 */
	public static final String english = "English";
	
	 /** 
	  * 
	  * Called when the activity is first created. 
	  * Will display the language select menu if no default language is set. 
	  * If default language is set, this activity will start the {@link MainMenuActivity} {@link Activity} and finish this {@link Activity}.
	  * Will also clear any messages from the {@link MessageHandler} and add the default {@link UncaughtExceptionHandler} to the application {@link Thread}.
	  */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(Logger.getInstance(true));
        MessageHandler.getInstance().clearQue();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean default_language = preferences.getBoolean("default_language", false);
        
        if (default_language){
        	Intent intent = new Intent(this, MainMenuActivity.class);
        	startActivity(intent);
        	finish();
        } else {
        	setContentView(R.layout.language);
        	Button norButton = (Button) findViewById(R.id.norButton);
        	Button engButton = (Button) findViewById(R.id.engButton);
        	Button continueButton = (Button) findViewById(R.id.saveLanguageButton);

        	norButton.setOnClickListener(new LanguageButtonOnClickListener(norwegian, this));
        	engButton.setOnClickListener(new LanguageButtonOnClickListener(english, this));
        	continueButton.setOnClickListener(new LanguageContinueOnClickListener(this));
        }
       
    }
}