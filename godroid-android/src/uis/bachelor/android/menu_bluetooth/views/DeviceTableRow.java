package uis.bachelor.android.menu_bluetooth.views;

import uis.bachelor.android.R;
import uis.bachelor.android.common.model.Logger;
import uis.bachelor.android.menu_bluetooth.BluetoothMenuActivity;
import uis.bachelor.android.menu_bluetooth.model.listeners.onClick.NewConnectionOnClickListener;
import uis.bachelor.android.menu_main.MainMenuActivity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Custom tableRow for a TableLayout.
 * @author Chris H�land
 *
 */
public class DeviceTableRow extends TableRow {
	/**
	 * {@link TextView} for the device ID
	 */
	private TextView deviceID;

	/**
	 * {@link TextView} for the device name
	 */
	private TextView deviceName;
	
	/**
	 * {@link TableRow} for the device ID
	 */
	private TableRow deviceIDRow;

	/**
	 * Device row for the device name
	 */
	private TableRow deviceNameRow;
	
	/**
	 * The {@link TableLayout} holing everything in place-
	 */
	private TableLayout tableLayout;
	
	/**
	 * Default constructor. Should not be used.
	 * @param context
	 */
	public DeviceTableRow(Context context) {
		super(context);
	}
	
	/**
	 * Default constructor. Will create the correct layout, with device ID and device name, as well as a proper background image. 
	 * @param device
	 * @param deviceList
	 */
	public DeviceTableRow(BluetoothDevice device, BluetoothMenuActivity deviceList){
		super(deviceList);
		setGravity(Gravity.CENTER);		
		
		deviceName = getDeviceName(device, deviceList);
			
		deviceNameRow = new TableRow(deviceList);
		deviceNameRow.setGravity(Gravity.CENTER);
		deviceNameRow.addView(deviceName);
		
		deviceID = getDeviceID(device, deviceList);
		
		deviceIDRow = new TableRow(deviceList);
		deviceIDRow.setGravity(Gravity.CENTER);
		deviceIDRow.addView(deviceID);

		tableLayout = new TableLayout(deviceList);
		tableLayout.addView(deviceNameRow);
		tableLayout.addView(deviceIDRow);

		addView(tableLayout);
		setOnClickListener(new NewConnectionOnClickListener(device, deviceList));
		setBackgroundDrawable(getResources().getDrawable(R.drawable.bluetooth_label_unpressed));
	}
	
	/**
	 * Inherited method. Will draw a background image according to pressed state.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		setBackgroundDrawable(getResources().getDrawable( isPressed() ? R.drawable.bluetooth_label_pressed : R.drawable.bluetooth_label_unpressed ));
		super.onDraw(canvas);
	}
	
	/**
	 * Private method to retrieve the device ID
	 * @param device The {@link BluetoothDevice} holding the device ID
	 * @param deviceList the {@link Context} that should hold the {@link TextView}
	 * @return a {@link TextView} with the ID.
	 */
	private TextView getDeviceID(BluetoothDevice device, BluetoothMenuActivity deviceList) {
		TextView textView = new TextView(deviceList);
		textView.setText(device.getAddress());
		textView.setGravity(Gravity.CENTER);
		textView.setTextColor(Color.BLACK);
		
		return textView;
	}
	/**
	 * Private method to retrieve the device name.
	 * @param device The {@link BluetoothDevice} holding the device name
	 * @param deviceList the {@link Context} that should hold the {@link TextView}
	 * @return a {@link TextView} with the name.
	 */
	private TextView getDeviceName(BluetoothDevice device, BluetoothMenuActivity deviceList) {
		TextView textView = new TextView(deviceList);
		textView.setGravity(Gravity.CENTER);
		textView.setTextColor(Color.BLACK);
		textView.setTextSize(32);
		
		int count = 0;
		textView.setText("");
		while (textView.getText().equals("") && count < 20) {
			count++;
			textView.setText(device.getName());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				if (MainMenuActivity.DEBUG) Logger.Log("Interrupted thread", e);
			}
		}
		
		return textView;
	}
}
