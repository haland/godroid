package uis.bachelor.android.menu_bluetooth.model.interfaces;

import android.bluetooth.BluetoothDevice;
/**
 * Interface used for listing {@link BluetoothDevice}s.
 * @author Eirik Heskja
 *
 */
public interface DeviceListener {
	/**
	 * Method used when a known device is listed.
	 * @param device The known device
	 */
	public void knownDeviceFound(BluetoothDevice device);
	
	/**
	 * Method used when a unknown device is listed.
	 * @param device The unknown device.
	 */
	public void unknownDeviceFound(BluetoothDevice device);
}
