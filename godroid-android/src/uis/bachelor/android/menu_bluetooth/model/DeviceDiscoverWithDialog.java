package uis.bachelor.android.menu_bluetooth.model;

import uis.bachelor.android.R;
import uis.bachelor.android.common.views.CustomDialog;
import uis.bachelor.android.menu_bluetooth.model.listeners.onCancel.ScanningDialogOnCancelListener;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Handler;
import android.os.Message;
/**
 * Class used to discover unknown devices
 * @author Eirik Heskja
 *
 */
public class DeviceDiscoverWithDialog implements Runnable {
	/**
	 * The dialog to show during the process.
	 */
	private CustomDialog dialog;
	
	/**
	 * The activity holding the dialog.
	 */
	private Activity activity;
	
	/**
	 * The adapter that does the discovery.
	 */
	private BluetoothAdapter adapter;
	
	/**
	 * Handler object to update GUI elements.
	 */
	private Handler handler;

	/**
	 * The default time before the dialog disappears.
	 */
	private long SEARCH_TIME = 12000;

	/**
	 * Default constructor. Will create the handler object.
	 * @param activty The activity holding the dialog.
	 * @param adapter The adapter doing the discovery.
	 */
	public DeviceDiscoverWithDialog(Activity activty, BluetoothAdapter adapter){
		this.activity = activty;
		this.adapter = adapter;
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				dialog.dismiss();
			}
		};
	}

	/**
	 * Creates the dialog and shows it. Then starts the discovery thread.
	 */
	public void startDiscovery(){
		dialog = new CustomDialog(activity, activity.getString(R.string.looking_for_devices_title),  activity.getString(R.string.looking_for_devices_text), true);
		dialog.setOnCancelListener(new ScanningDialogOnCancelListener(adapter));
		dialog.show();
		Thread thread = new Thread(this);
		thread.start();	
	}

	/**
	 * Starts the discovery process, then waits for 12 sec before it cancels discovery and dismisses the dialog.
	 */
	@Override
	public void run() {
		adapter.startDiscovery();
		try {
			Thread.sleep(SEARCH_TIME);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		handler.sendEmptyMessage(0);
		adapter.cancelDiscovery();
	}

}
