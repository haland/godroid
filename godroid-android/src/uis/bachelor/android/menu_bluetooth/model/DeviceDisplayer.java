package uis.bachelor.android.menu_bluetooth.model;

import java.util.ArrayList;
import java.util.Set;

import uis.bachelor.android.menu_bluetooth.model.interfaces.DeviceListener;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
/**
 * Class used to display known and unknown {@link BluetoothDevice}s.
 * @author Eirik Heskja
 *
 */
public class DeviceDisplayer {
	/**
	 * Integer representation of the requested action started.
	 */
	private static final int BLUETOOTH_START_REQUEST = 1;
	
	/**
	 * The {@link Activity} holding the {@link BroadcastReceiver}.
	 */
	private Activity activity;
	
	/**
	 * List of {@link DeviceListener} listeners.
	 */
	private ArrayList<DeviceListener> listeners;
	
	/**
	 * The {@link BroadcastReceiver} used to receive system broadcasts. 
	 */
	private BroadcastReceiver receiver;
	
	/**
	 * The intent filter used to filter out BluetoothDevice.Action_found events
	 */
	private IntentFilter filter;
	
	/**
	 * The intent filter used to filter out BluetoothDevice.Action_acl_disconnected events
	 */
	private IntentFilter filterDisconnect;
	
	/**
	 * The adapter used for Bluetooth communication
	 */
	private BluetoothAdapter adapter;

	/**
	 * The runnable thread used to discover adapters.
	 */
	private DeviceDiscoverWithDialog discover;
	
	/**
	 * Default constructor. Creates all the objects used.
	 * @param activity The activity that's holding the receiver.
	 */
	public DeviceDisplayer(Activity activity){
		this.activity = activity;
		listeners = new ArrayList<DeviceListener>();

		receiver = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (action.equals(BluetoothDevice.ACTION_FOUND))
					createUnknownDeviceEvent((BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE));
			}
		};
		adapter = BluetoothAdapter.getDefaultAdapter();
		filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		filterDisconnect = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
		discover = new DeviceDiscoverWithDialog(activity, adapter);
		activity.registerReceiver(receiver, filterDisconnect);
	}
	
	/**
	 * Method to add a {@link DeviceListener} to the list of listeners.
	 * @param listener
	 */
	public void addDeviceListener(DeviceListener listener){
		listeners.add(listener);
	}
	
	/**
	 * Will start the Bluetooth enable process if the adapter is disabled. 
	 * If the adapter is enabled, it will list all known devices.
	 */
	public void listDevicesIfBluetoothIsEnabled(){
		if (!adapter.isEnabled())
			enableBluetooth();
		else
			listKnownDevices();
	}
	
	/**
	 * Method used to enable the {@link BluetoothAdapter}.
	 */
	private void enableBluetooth() {
		Intent bluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		activity.startActivityForResult(bluetoothIntent, BLUETOOTH_START_REQUEST);
	}
	
	/**
	 * Loops through all bonded (known) devices, and creates a event for all listening threads to handle.
	 */
	private void listKnownDevices() {
		Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
		for (BluetoothDevice device : pairedDevices)
			createKnownDeviceEvent(device);
	}
	
	/**
	 * Starts the device discovery process in {@link DeviceDiscoverWithDialog}.
	 */
	public void listUnknownDevices(){
		activity.registerReceiver(receiver, filter);
		discover.startDiscovery();
	}
	
	/**
	 * Tells all the listening threads that a unknown device is found
	 * @param device The found device
	 */
	protected void createUnknownDeviceEvent(BluetoothDevice device) {
		for (DeviceListener listener: listeners)
			listener.unknownDeviceFound(device);
	}
	
	/**
	 * Tells all the listening threads that a known device is found. 
	 * @param device
	 */
	private void createKnownDeviceEvent(BluetoothDevice device) {
		for (DeviceListener listener : listeners)
			listener.knownDeviceFound(device);
	}
	
	/**
	 * Public method to retrieve the {@link BroadcastReceiver} used.
	 * @return the used {@link BroadcastReceiver}.
	 */
	public BroadcastReceiver getReceiver() {
		return receiver;
	}

}
