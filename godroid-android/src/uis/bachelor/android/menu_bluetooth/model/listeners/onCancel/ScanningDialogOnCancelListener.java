package uis.bachelor.android.menu_bluetooth.model.listeners.onCancel;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
/**
 * Class used as when a user cancels a scanning dialog.
 * @author Eirik Heskja
 *
 */
public class ScanningDialogOnCancelListener implements OnCancelListener {
	/**
	 * The {@link BluetoothAdapter} that does the scanning.
	 */
	private BluetoothAdapter adapter;
	
	/**
	 * Default constructor. Instantiates the adapter.
	 * @param adapter The used {@link BluetoothAdapter}.
	 */
	public ScanningDialogOnCancelListener(BluetoothAdapter adapter){
		this.adapter = adapter;
	}
	
	/**
	 * Method executed when the user cancels a scanning dialog.
	 * Will cancel the device discovery process.
	 */
	@Override
	public void onCancel(DialogInterface dialog) {
		adapter.cancelDiscovery();
	}

}
