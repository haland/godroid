package uis.bachelor.android.menu_bluetooth.model.listeners.onClick;

import uis.bachelor.android.menu_bluetooth.BluetoothMenuActivity;
import uis.bachelor.android.menu_bluetooth.model.ConnectionCreatorWithDialog;
import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.view.View.OnClickListener;
/**
 * Class used when the user want a new connection
 * @author Eirik Heskja
 *
 */
public class NewConnectionOnClickListener implements OnClickListener {
	/**
	 * The {@link ConnectionCreatorWithDialog} object used to start the dialog.
	 */
	private ConnectionCreatorWithDialog dialog;
	
	/**
	 * Default constuctor. Will create a new {@link ConnectionCreatorWithDialog} object.
	 * @param device The device to connect to.
	 * @param list The list holding the device.
	 */
	public NewConnectionOnClickListener(BluetoothDevice device, BluetoothMenuActivity list){
		dialog = new ConnectionCreatorWithDialog(device, list);
	}

	/**
	 * Method executed when a user clicks on a new connection button.
	 * Will start the connection process.
	 */
	public void onClick(View v) {
		dialog.startConnection();
	}

	
}
