package uis.bachelor.android.menu_bluetooth.model.listeners.onClick;

import standard.Flags;
import uis.bachelor.android.R;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.common.model.communication.interfaces.BluetoothDisconnectMessageInterface;
import uis.bachelor.android.common.views.CustomDialog;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
/**
 * Class used when a user wants to disconnect.
 * @author Eirik Heskja
 *
 */
public class DisconnectFromNXTOnClickListener implements OnClickListener, BluetoothDisconnectMessageInterface {
	/**
	 * The dialog to show when the button is pressed.
	 */
	private CustomDialog dialog;
	
	/**
	 * Default constructor. Will create a new dialog.
	 * @param activity
	 */
	public DisconnectFromNXTOnClickListener(Activity activity){
		dialog = new CustomDialog(activity, activity.getString(R.string.disconnecting_title),  activity.getString(R.string.disconnecting_text), false);
	}
	
	/**
	 * Will tell the handler to start disconnecting and show a dialog.
	 */
	@Override
	public void onClick(View v) {
		MessageHandler.getInstance().addDisconnectListener(this);
		MessageHandler.getInstance().putMessage(Flags.MSG_DISCONNECT, null);
		dialog.show();
	}

	/**
	 * Will dismiss the dialog on disconnect.
	 */
	@Override
	public void disconnect() {
		dialog.dismiss();
	}

}
