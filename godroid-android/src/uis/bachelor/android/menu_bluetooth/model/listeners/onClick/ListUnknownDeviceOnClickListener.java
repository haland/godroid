package uis.bachelor.android.menu_bluetooth.model.listeners.onClick;

import uis.bachelor.android.menu_bluetooth.model.DeviceDisplayer;
import android.view.View;
import android.view.View.OnClickListener;
/**
 * Class used to when the user want to list unknown devices. 
 * @author Eirik Heskja
 *
 */
public class ListUnknownDeviceOnClickListener implements OnClickListener {
	/**
	 * The {@link DeviceDisplayer} object that will do the listing.
	 */
	private DeviceDisplayer deviceDisplayer;
	
	/**
	 * Default constructor. Will instantiate the {@link DeviceDisplayer}.
	 * @param devicedDisplayer
	 */
	public ListUnknownDeviceOnClickListener(DeviceDisplayer devicedDisplayer){
		this.deviceDisplayer = devicedDisplayer;
	}
	
	/**
	 * Used when the user clicks on the button. Will start the process of listing unknown devices.
	 */
	public void onClick(View v) {
		deviceDisplayer.listUnknownDevices();
	}
}