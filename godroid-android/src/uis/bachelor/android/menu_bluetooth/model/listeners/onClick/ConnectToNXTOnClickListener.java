package uis.bachelor.android.menu_bluetooth.model.listeners.onClick;

import uis.bachelor.android.R;
import uis.bachelor.android.common.views.CustomDialog;
import uis.bachelor.android.menu_bluetooth.BluetoothMenuActivity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
/**
 * Class used when a user want start the connection process.
 * @author Eirik Heskja
 *
 */
public class ConnectToNXTOnClickListener implements OnClickListener {
	/**
	 * The context holding the dialog.
	 */
	private Context activity;
	
	/**
	 * The {@link CustomDialog} to show if there is no installed {@link BluetoothAdapter}.
	 */
	private CustomDialog dialog;
	
	/**
	 * The intent to be started if there is a {@link BluetoothAdapter}.
	 */
	private Intent intent;

	/**
	 * Default constructor. Will create new intent and dialog objects.
	 * @param activity
	 */
	public ConnectToNXTOnClickListener(Context activity){
		this.activity = activity;
		intent = new Intent(activity, BluetoothMenuActivity.class);
		dialog = new CustomDialog(activity, activity.getString(R.string.no_devices_detected_error));
	}

	/**
	 * Method executed when the user clicks on the connect button on the home screen.
	 * Will check if there is a default {@link BluetoothAdapter}, and start the intent if there is. Else, it will display a dialog.
	 */
	public void onClick(View v) {
		if (BluetoothAdapter.getDefaultAdapter() != null)
			activity.startActivity(intent);
		else
			dialog.show();
	}

}
