package uis.bachelor.android.menu_bluetooth.model;

import standard.Values;
import uis.bachelor.android.R;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.common.views.CustomDialog;
import uis.bachelor.android.menu_bluetooth.BluetoothMenuActivity;
import uis.bachelor.android.menu_settings.model.SettingsSender;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
/**
 * Class used to start a new connection.
 * @author Eirik Heskja
 *
 */
public class ConnectionCreatorWithDialog implements Runnable {
	/**
	 * The dialog to show if the connection failed.
	 */
	private CustomDialog dialog;
	
	/**
	 * The device to connect to.
	 */
	private BluetoothDevice device;
	
	/**
	 * The handler used to handle messages.
	 */
	private Handler handler;
	
	/**
	 * The {@link BluetoothMenuActivity} that started the dialog.
	 */
	private BluetoothMenuActivity list;

	/**
	 * Integer value used in the handler.
	 */
	private final int DISMISS = -1;


	/**
	 * Default constructor. Creates the handler and the {@link CustomDialog} for connection.
	 * @param device The device to connect to.
	 * @param list The list holding the dialog.
	 */
	public ConnectionCreatorWithDialog(BluetoothDevice device, final BluetoothMenuActivity list){
		this.list = list;
		this.device = device;
		dialog = new CustomDialog(list, list.getString(R.string.connecting_title), list.getString(R.string.connecting_text), false);
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what){
				case Values.STATUS_CONNECTION_ERROR:
					CustomDialog custom = new CustomDialog(list, list.getString(R.string.connection_error));
					custom.setPositiveButtonListener(new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							dismiss();
						}
					});
					custom.show();
					break;
				case DISMISS: dismiss(); break;	
				}
			}
		};
	}

	/**
	 * Will dismiss the dialog and finish the activity.
	 */
	private void dismiss() {
		dialog.dismiss(); list.finish();
	}

	/**
	 * Will start a new thread and show the dialog.
	 */
	public void startConnection(){
		dialog.show();
		Thread thread = new Thread(this);
		thread.start();	
	}
	
	/**
	 * Will try to open a new connection. If the connection fails, it will display a new dialog, else it will start sending settings to the NXT.
	 */
	@Override
	public void run() {
		int status = MessageHandler.getInstance().openConnection(device);
		if (status == Values.STATUS_CONNECTION_ERROR)
			handler.sendEmptyMessage(status);
		else{
			handler.sendEmptyMessage(DISMISS);
			SettingsSender.getInstance(list).send();
		}
	}


}
