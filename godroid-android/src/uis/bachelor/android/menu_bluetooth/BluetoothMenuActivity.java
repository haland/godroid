package uis.bachelor.android.menu_bluetooth;

import java.util.ArrayList;

import uis.bachelor.android.R;
import uis.bachelor.android.common.ResultActivity;
import uis.bachelor.android.menu_bluetooth.model.DeviceDisplayer;
import uis.bachelor.android.menu_bluetooth.model.interfaces.DeviceListener;
import uis.bachelor.android.menu_bluetooth.model.listeners.onClick.ListUnknownDeviceOnClickListener;
import uis.bachelor.android.menu_bluetooth.views.DeviceTableRow;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.widget.TableLayout;
/**
 * 
 * Activity for displaying and locating both known and unknown {@link BluetoothDevice}s.
 * @author Eirik Heskja
 *
 */
public class BluetoothMenuActivity extends ResultActivity implements DeviceListener {
	/**
	 * 
	 * {@link ArrayList} that contains all new, found devices. This will help preventing the same device being displayed multiple times.
	 */
	private ArrayList<BluetoothDevice> found;
	
	/**
	 * 
	 * Default method that runs when the {@link Activity} start.
	 * Will instantiate the found {@link ArrayList} and list known devices if {@link BluetoothAdapter} is enabled.
	 * Will also add it self to the list of listeners in {@link DeviceDisplayer}.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.devicelayout);

		findViewById(R.id.searchButton).setOnClickListener(new ListUnknownDeviceOnClickListener(deviceDisplayer));
		deviceDisplayer.addDeviceListener(this);
		found = new ArrayList<BluetoothDevice>();
		deviceDisplayer.listDevicesIfBluetoothIsEnabled();
		
	}
	/**
	 * 
	 * Default method that will run when the {@link Activity} is destroyed.
	 * Will clear the found {@link ArrayList} and unregister any receivers associated with the {@link DeviceDisplayer}.
	 */
	@Override
	protected void onDestroy() {
		if (deviceDisplayer.getReceiver() != null)
			unregisterReceiver(deviceDisplayer.getReceiver());
		if (found != null)
			found.clear();
		super.onDestroy();
	}

	/**
	 * 
	 * Will add the known {@link BluetoothDevice} to the list of devices.
	 * @param device The known {@link BluetoothDevice} found
	 */
	public void knownDeviceFound(BluetoothDevice device) {
		addDeviceToView(device, R.id.table);
	}
	/**
	 * 
	 * Will add the unknwon {@link BluetoothDevice} to the list of devices.
	 * @param device The new, unknwon {@link BluetoothDevice} found.
	 */
	public void unknownDeviceFound(BluetoothDevice device) {
		if (!found.contains(device)) {
			addDeviceToView(device, R.id.table);
			found.add(device);
		}
	}
	
	/**
	 * 
	 * Will create an new {@link DeviceTableRow} and add it to the appropriate TableLayout.
	 * @param device The {@link BluetoothDevice} to be added.
	 * @param ID The ID defining what TableLayout the row should be added to.
	 */
	private void addDeviceToView(BluetoothDevice device, int ID) {
		((TableLayout) findViewById(ID)).addView(new DeviceTableRow(device, this));
	}


}
