package uis.bachelor.android.menu_main.views.buttons;

import uis.bachelor.android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * Class used for the connect button on the mainmenu.
 * @author Chris H�land
 *
 */
public class ConnectButton extends MainMenuButton {
	/**
	 * Default constructor.
	 * @param context The context that holds the button.
	 * @param attrs The attributes defined in XML.
	 */
	public ConnectButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Will draw the background on the button according to it's pressed state.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		setBackgroundDrawable(getResources().getDrawable( isPressed() ? R.drawable.main_label_connect_nxt_pressed : R.drawable.main_label_connect_nxt_unpressed ));
		super.onDraw(canvas);
	}
}
