package uis.bachelor.android.menu_main.views.buttons;

import uis.bachelor.android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

/**
 * Class used for the Remote button on the main menu.
 * @author Chris H�land
 *
 */
public class RemoteButton extends MainMenuButton {
	/**
	 * Default constructor
	 * @param context The context that holds this button.
	 * @param attrs The attributes used in the XML file to create the button.
	 */
	public RemoteButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Will draw a background image depending on the buttons pressed state.
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		setBackgroundDrawable(getResources().getDrawable( isPressed() ? R.drawable.main_label_remote_nxt_pressed : R.drawable.main_label_remote_nxt_unpressed ));
		super.onDraw(canvas);
	}
}
