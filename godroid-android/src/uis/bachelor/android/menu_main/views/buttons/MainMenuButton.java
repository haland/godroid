package uis.bachelor.android.menu_main.views.buttons;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Class used for each of the buttons on the mainmenu.
 * @author Chris H�land
 *
 */
public class MainMenuButton extends Button {
	
	/**
	 * Boolean variable telling if the button has been resized.
	 */
	private boolean hasResized;
	
	/**
	 * Default constructor. 
	 * @param context The context that holds the button.
	 * @param attrs Atrributes defined in the XML file.
	 */
	public MainMenuButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		hasResized = false;
	}

	/**
	 * Will resize the button on layout. 
	 */
	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if (!hasResized) {
			ViewGroup.LayoutParams layoutParameters = getLayoutParams();
			layoutParameters.width = (int) (getWidth() * 0.76);
			layoutParameters.height = (int) (layoutParameters.width * 0.39);

			setLayoutParams(layoutParameters);
			hasResized = true;
		}
	}
}
