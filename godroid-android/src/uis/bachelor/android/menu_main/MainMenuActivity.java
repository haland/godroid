package uis.bachelor.android.menu_main;

import standard.Flags;
import uis.bachelor.android.R;
import uis.bachelor.android.common.HandlerActivity;
import uis.bachelor.android.common.model.communication.MessageHandler;
import uis.bachelor.android.menu_bluetooth.model.listeners.onClick.ConnectToNXTOnClickListener;
import uis.bachelor.android.menu_bluetooth.model.listeners.onClick.DisconnectFromNXTOnClickListener;
import uis.bachelor.android.menu_controller.model.listeners.onClick.RemoteControllOnClickListener;
import uis.bachelor.android.menu_settings.PreferencesMenuActicity;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import array.SearchArray;
/**
 * 
 * The main menu for this {@link Application}. Will contain a connect/disconnect button and a button for starting the remote control.
 * @author Eirik Heskja
 *
 */
public class MainMenuActivity extends HandlerActivity {
	
	/**
	 * 
	 * Static variable used for debug purposes only.
	 */
	public static boolean DEBUG = true;
	
	/**
	 * 
	 * A OnClickListener that will be added to the connect / disconnect button.
	 */
	private ConnectToNXTOnClickListener connect;
	
	/**
	 * 
	 * A OnClickListener that will be added to the connect / disconnect button.
	 */
	private DisconnectFromNXTOnClickListener disconnect;

	/**
	 * 
	 * The Intent related to displaying {@link PreferencesMenuActicity}
	 */
	private Intent intent;
	
	/**
	 *
	 * Will be run when the {@link Activity} starts. 
	 * Will instantiate any objects in this class, add the appropiate listeners to the buttons and put the buttons where they should be. 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		connect = new ConnectToNXTOnClickListener(this);
		disconnect = new DisconnectFromNXTOnClickListener(this);
		intent = new Intent(this, PreferencesMenuActicity.class);
		
		findViewById(R.id.remoteButton).setOnClickListener(new RemoteControllOnClickListener(this));
		updateStatus();
		updateSettings();
		
		Display display = getWindowManager().getDefaultDisplay();
		
		RelativeLayout connectBtn = (RelativeLayout)findViewById(R.id.RelativeLayout1);
		connectBtn.setPadding(0, (int) (display.getHeight()/3.8), 0, 0);
		MessageHandler.getInstance().addCalibrationListener(this);
		MessageHandler.getInstance().addSettingsListener(this);
	}
	
	/**
	 * 
	 * Method for setting the "DEBUG" variable according to Preferences.
	 */
	private void updateSettings() {
		DEBUG = preferences.getBoolean("debug", false);
	}
	
	/**
	 * 
	 * Method that will update settings and update any status messages when the {@link Activity} gains focus.
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if (hasFocus){
			updateStatus();
			updateSettings();
		}
		super.onWindowFocusChanged(hasFocus);
	}
	
	/**
	 * 
	 * Method that will update settings and update any status messages when the {@link Activity} resumes.
	 */
	@Override
	protected void onResume() {
		super.onResume();
		updateStatus();
		updateSettings();
	}

	/**
	 * 
	 * Will update the background image and set the correct {@link OnClickListener} to the connectButton.
	 */
	@Override
	protected void updateStatus() {
		boolean isConnected = MessageHandler.getInstance().isConnected();
		updateBackground(isConnected);
		updateButton(isConnected);
	}

	/**
	 * 
	 * Will set the correct background image according to parameter. If connected, the background will contain a green Android-logo, else the background will be white.
	 * @param isConnected If true, the background will be green, else it will be white.
	 */
	public void updateBackground(boolean isConnected) {
		findViewById(R.id.RelativeLayout1).setBackgroundDrawable(getResources().getDrawable( isConnected ? R.drawable.main_background_connected : R.drawable.main_background_disconnected ));
	}

	/**
	 * Sets the appropriate {@link OnClickListener} to the "connectButton"
	 * @param isConnected If true, the button will act as a Disconnect button, else it will act as a connect button.
	 */
	private void updateButton(boolean isConnected) {
		if (!isConnected){
			((Button) findViewById(R.id.connectButton)).setOnClickListener(connect);
		}
		else{
			((Button) findViewById(R.id.connectButton)).setOnClickListener(disconnect);
		}
	}

	/**
	 * 
	 * Method that will be executed when this {@link Activity} closes. Will make sure to disable Bluetooth.
	 */
	@Override
	protected void onDestroy() {
		disableBluetooth();
		super.onDestroy();
	}

	/**
	 * 
	 * Will disconnect from the NXT device if it is connected.
	 * Will disable Bluetooth if that option is set to "true" in the Preferences.
	 */
	private void disableBluetooth() {
		if (MessageHandler.getInstance().isConnected())
			MessageHandler.getInstance().putMessage(Flags.MSG_DISCONNECT, null);
		boolean disableBluetooth = preferences.getBoolean("bluetoothDisable", true);
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		if (adapter != null && disableBluetooth)
			adapter.disable();
	}

	/**
	 * 
	 * Makes sure to inflate the {@link Menu} and set the correct icon for any {@link MenuItem}s.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.settingsmenu, menu);
		menu.getItem(0).setIcon(android.R.drawable.ic_menu_preferences);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * 
	 * Will start the {@link PreferencesMenuActicity} {@link Activity}.
	 */
	private void displaySettings(){
		startActivity(intent);
	}

	/**
	 * 
	 * Method for choosing what action to be executed when the user clicks on a {@link MenuItem}.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.preferences: 
			displaySettings();
			return true; 
		}
		return false;
	}

	/**
	 * Dummy inherited method. Does nothing.
	 */
	@Override
	protected void updateText() {
	}

	/**
	 * Dummy inherited method. Does nothing.
	 */
	@Override
	protected void tooClose(int size) {
	}

	/**
	 * Dummy inherited method. Does nothing.
	 */
	@Override
	protected void lowBattery(int battery) {
	}

	/**
	 * Dummy inherited method. Does nothing.
	 */
	@Override
	protected void lowSignal(int signal) {
	}

	/**
	 * Dummy inherited method. Does nothing.
	 */
	@Override
	protected void updateWarning() {
	}
	
	@Override
	protected void onDisconnectDialogFinish() {}

	@Override
	protected void updateMenu(SearchArray values) {}

}

