package concurrent;

/**
 * Used guide for Semaphore use: {@link http://www.javaworld.com/javaworld/javaqa/1999-11/02-qa-semaphore.html}
 * @author Chris H�land
 */

public class Semaphore {
	/**
	 * Number of allowed processes to run at start
	 */
	private int permits;
	
	/**
	 * Creates a standard semaphore with 0 allowed processes at start
	 */
	public Semaphore () {
		this.permits = 0;
	}
	
	/**
	 * Creates a semaphore 
	 * @param permits - Number off allowed processes to run at start
	 */
	public Semaphore(int permits) {
		if (permits < 0)
			throw new IllegalArgumentException("permits must be a positiv number");
		this.permits = permits;
	}
	
	/**
	 * Holds the process till release() is run by another process
	 */
	public synchronized void obtain() {
		while (permits == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} permits--;
	}
	
	/**
	 * Releases a process from the semaphore
	 */
	public synchronized void release() {
		if (permits == 0)
			notify();
		permits++;
	}
}
