package standard;

import standard.exception.NoFlagException;

/**
 * @author Eirik Heskja
 */

public class Flags {	
	/**
	 * Flag for sending a drive message
	 */
	public static final int MSG_DRIVE = 1001;
	
	/**
	 * Flag for sending transformation message
	 */
	public static final int MSG_SEGWAY = 1002;
	
	/**
	 * Flag for sending disconnect message
	 */
	public static final int MSG_DISCONNECT = 1004;
	
	/**
	 * Flag for sending settings message
	 */
	public static final int MSG_SETTINGS = 1005;
	
	/**
	 * Flag for sending all settings has been sent message
	 */
	public static final int MSG_SETTINGS_COMPLETE = 1006;
	
	/**
	 * Flag for sending error message
	 */
	public static final int MSG_ERROR = 1007;
	
	/**
	 * Flag for sending ok message
	 */
	public static final int MSG_OK = 1008;
	
	/**
	 * Flag for sending calibrated message
	 */
	public static final int MSG_CALIBRATED = 1009;
	
	/**
	 * Flag for sending is sending settings message
	 */
	public static final int MSG_SENDING_SETTINGS = 1010;
	
	/**
	 * Flag for sending distance message
	 */
	public static final int MSG_DISTANCE = 1011;
	
	/**
	 * Flag for sending version message
	 */
	public static final int MSG_VERSION = 1012;
	
	/**
	 * Flag for sending stop message
	 */
	public static final int MSG_STOP =1013;
	
	/**
	 * Flag for sending battery message
	 */
	public static final int MSG_BATTERY = 1014;
	
	/**
	 * Flag for sending signal message
	 */
	public static final int MSG_SIGNAL = 1015;

	/**
	 * Checks if the parameter given is a flag
	 * @param cmd - To check if it is a flag
	 * @return Whether the parameter is a flag
	 * @throws NoFlagException - Thrown if the parameter is not a flag
	 */
	public static boolean isFlag(int cmd) throws NoFlagException{
		switch(cmd){
			case MSG_DRIVE:
			case MSG_SEGWAY:
			case MSG_DISCONNECT:
			case MSG_SETTINGS:
			case MSG_SETTINGS_COMPLETE:
			case MSG_ERROR: 
			case MSG_OK: 
			case MSG_CALIBRATED:
			case MSG_SENDING_SETTINGS:
			case MSG_DISTANCE:
			case MSG_VERSION:
			case MSG_STOP:
			case MSG_BATTERY:
			case MSG_SIGNAL:
				return true;
			default: throw new NoFlagException(cmd);
		}
	}

	/**
	 * Checks if the flag has a sending priority
	 * @param flag - Flag to check
	 * @return Whether the flag has a sending priority
	 */
	public static boolean hasPriority(int flag) {
		switch(flag){
		case MSG_SETTINGS:
		case MSG_SETTINGS_COMPLETE:
		case MSG_STOP:
		case MSG_DISCONNECT:
			return true;
		default:
			return false;
		}
	}
}
