package standard;

/**
 * Standard values used for certain parameters when sending command
 * @author Chris H�land & Eirik Heskja
 */

public class Values {
	/* SETTING TYPES */
	/**
	 * Value for sending gyroscope settings 
	 */
	public static final int SETTING_GYROSCOPE = 1;
	
	/**
	 * Value for sending left motor settings
	 */
	public static final int SETTING_MOTOR_LEFT = 2;
	
	/**
	 * Value for sending right motor settings
	 */
	public static final int SETTING_MOTOR_RIGHT = 3;
	
	/**
	 * Value for sending middle motor settings
	 */
	public static final int SETTING_MIDDLE_MOTOR = 4;
	
	/**
	 * Value for sending accelerometer settings
	 */
	public static final int SETTING_ACCELEROMETER = 5;

	/**
	 * Value for sending ultrasonic sensor settings  
	 */
	public static final int SETTING_ULTRA_SONIC_SENSOR = 7;
	
	/**
	 * Value for sending segway settings
	 */
	public static final int SETTING_SEGWAY = 8;
	
	
	/* TRANSFORMATION */
	
	/**
	 * Value for sending transformation interrupted  
	 */
	public static final int SEGWAY_INTERUPT = -1;
	
	/**
	 * Value for sending transform to regular mode (drive on three wheels)
	 */
	public static final int SEGWAY_DOWN = 0;
	
	/**
	 * Value for sending transform to segway mode (balancing)
	 */
	public static final int SEGWAY_UP = 1;
	
	
	/* WARNINGS */
	/**
	 * The the max distance of which the NXT is supposed to notify the Android it is driving to close
	 */
	public static final int WARNING_DISTANCE = 35;
	
	/**
	 * The battery warning level
	 */
	public static final int WARNING_BATTERY = 75;
	
	/**
	 * The signal warning level
	 */
	public static final int WARNING_SIGNAL = 210;
	
	
	/* Android STATUS variables */
	
	/**
	 * Status to use when established connection to a NXT
	 */
	public static final int STATUS_CONNECTED = 6;
	
	/**
	 * Status to use when there is an error establishing a connection to the NXT
	 */
	public static final int STATUS_CONNECTION_ERROR = 7;
	
	
	/* NXT STATUS variables */
	
	/**
	 * Status to use when there has occured an error at the NXT
	 */
	public static final int STATUS_SEND_ERROR = 8;
	
	
	/*-- MOTOR PORT VALUES --*/
	
	/**
	 * Integer value representing MotorPort.A 
	 */
	public static final int MOTOR_PORT_A = 1;
	
	/**
	 * Integer value representing MotorPort.B
	 */
	public static final int MOTOR_PORT_B = 2;
	
	/**
	 * Integer value representing MotorPort.C
	 */
	public static final int MOTOR_PORT_C = 3;
	
	
	/*-- SENSOR PORT VALUES --*/
	
	/**
	 * Integer value representing SensorPort.1
	 */
	public static final int SENSOR_PORT_1 = 1;
	
	/**
	 * Integer value representing SensorPort.2
	 */
	public static final int SENSOR_PORT_2 = 2;
	
	/**
	 * Integer value representing SensorPort.3
	 */
	public static final int SENSOR_PORT_3 = 3;
	
	/**
	 * Integer value representing SensorPort.4
	 */
	public static final int SENSOR_PORT_4 = 4;
	
	
	/*-- BOOLEAN INTEGER VALUES --*/
	
	/**
	 * Integer value representing "true"
	 */
	public static final int BOOLEAN_TRUE = 1;
	
	/**
	 * Integer value representing "false"
	 */
	public static final int BOOLEAN_FALSE = 0;
}