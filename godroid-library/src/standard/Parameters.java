package standard;

/**
 * This class contains String parameters used for sending values back and forth between the NXT and
 * the Android unit. It also contains Integer values representing the motor and sensor ports and
 * the boolean values. All String variables are parameters used when adding values to {@link} 
 * @author Chris H�land & Eirik Heskja
 */

public class Parameters {
	/**
	 * Parameter used for what kind of command/message is being sent
	 */
	public static final String TYPE = "type";
	
	/**
	 * Parameter used for the version of the program
	 */
	public static final String VERSION = "ver";

	/**
	 * Parameter used for sending the status of the NXT
	 */
	
	public static final String STATUS = "status";
	
	/**
	 * Parameter used for sensor ports
	 */
	public static final String SENSOR = "sesnor";
	
	/**
	 * Parameter used for motor ports
	 */
	public static final String MOTOR_PORT = "motor";
	
	/**
	 * Parameter used for direction of the motor 
	 */
	public static final String MOTOR_DIRECTION = "dir";
	
	/**
	 * Parameter used for sending the travel speed to the NXT
	 */
	public static final String DRIVE_VELOCITY = "speed";
	
	/**
	 * Parameter used for sending the direction the NXT is to travel in
	 */
	public static final String DRIVE_DIRECTION = "angle";
	
	/**
	 * Parameter used for sending the gyroscope sensor port
	 */
	public static final String GYRO_PORT = "gyro";
	
	/**
	 * Parameter used for sending the accelerometer sensor port
	 */
	public static final String ACCELEROMETER_PORT = "accel";
	
	/**
	 * Parameter used for sending the ultrasonic sensor port
	 */
	public static final String DETECTION_PORT = "detection_port"; 

	/**
	 * Parameter used for sending the number of degrees the middle motor is to rotate from the calibration
	 * zero point to its zero point for use.
	 */
	public static final String TRANSFORMATION_ZERO_DEGREES = "cal_to_zero";
	
	/**
	 * Parameter used for sending the gyroscope read off value where the NXT is about to fall when trying to start
	 * balancing 
	 */
	public static final String SEGWAY_GYROSCOPE_FALL_VALUE = "seg_fall";
	
	/**
	 * Parameter used for sending the accelerometer value where the NXT is supposed to stop the lift motor, so it can 
	 * begin rotating a few degrees at a time, until balancing.
	 */
	public static final String SEGWAY_ACCELEROMETER_REACH_VALUE = "seg_lift_val";
	
	/**
	 * Parameter used for telling the Android unit that the segway transformation is completed
	 */
	public static final String TRANSFORMATION_SEGWAY_COMPLETE = "segway";
	
	/**
	 * Parameter used for telling the Android unit that the transformation to normal driving mode is completed
	 */
	public static final String TRANSFORMATION_TRICYCLE_COMPLETE = "tricycle";
	
	/**
	 * Parameter used for telling the Android unit that the transformation has been interrupted
	 */
	public static final String TRANSFORMATION_INTERRUPTED = "interrupted";
	
	/**
	 * Parameter used for sending to close to an object warning to the Android
	 */
	public static final String DISTANCE = "distance";
	
	/**
	 * Parameter used for sending the battery level to the Android
	 */
	public static final String BATTERY = "battery";
	
	/**
	 * Parameter used for sending low signal warning to the Android
	 */
	public static final String SIGNAL = "signal";

}
