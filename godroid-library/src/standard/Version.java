package standard;

/**
 * Version control of Android and NXT 
 * @author Eirik Heskja
 */

public class Version {
	/**
	 * The version number
	 */
	public static final int VERSION = 2;
	
	/**
	 * Checks if two units are the same version
	 * @param v1 - Version number of unit one
	 * @param v2 - Version number of unit two
	 * @return Whether the two units versions corresponds
	 */
	private static boolean isSameVersion(int v1, int v2){
		return v1 == v2;
	}
	
	/**
	 * Checks if the input version is the same as the class' version
	 * @param v1 - The version to check
	 * @return Whether the versions correspond
	 */
	public static boolean isSameVersion(int v1){
		return isSameVersion(v1, VERSION);
	}
}
