package standard.exception;

/**
 * @author Eirik Heskja
 */

public class NoFlagException extends Exception {

	private static final long serialVersionUID = 4100317599811662988L;
	
	/**
	 * Throws an exception if the flag does not exist
	 * @param flag - The value that is not a flag
	 */
	public NoFlagException(int flag){
		super("CMD: "+flag+" is not a valid flag!");
	}

}
