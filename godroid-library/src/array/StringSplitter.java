package array;

/**
 * 
 * The LeJOS library does not contain a String.split()-method. This class splits Strings using the LeJOS library String-methods.
 * 
 * @author Chris H�land
 * 
 */

public class StringSplitter {
	/**
	 * Value returned from indexOf(str) when str is not found in the String.  
	 */
	private static final int NOT_FOUND = -1;
	
	/**
	 * The number of elements in a String s, delimited by an delimiter.
	 */
	private static int count;
	
	/**
	 * Splits a String by a delimiter
	 * @param text - The text to be split
	 * @param delimiter - The character(s) the text is to be split by
	 * @return elements - remaining text bits after splitting the text by the delimiter
	 */
	public static String[] split(String text, String delimiter) {
		int elements = countElements(text, delimiter);
		String[] splitString = new String[elements];

		for (int i = 0; i < elements; i++) {
			int nextDelimiter = text.indexOf(delimiter);

			if (nextDelimiter  != NOT_FOUND) {
				splitString[i] = text.substring(0, nextDelimiter);
				text = text.substring(nextDelimiter + delimiter.length(), text.length());
			} else {
				splitString[i] = text;
			}
		}
		
		return splitString;
	}
	
	/**
	 * Counts the number of elements obtainable from splitting the text by the delimiter
	 * @param text - The text to be split
	 * @param delimiter - The character(s) the text is to be split by
	 * @return Number of elements in the text, which is split by the delimiter
	 */
	private static int countElements(String text, String delimiter) {
		count = 0;
		String countText = text;
		int nextDelimiter = text.indexOf(delimiter);
		
		if (nextDelimiter != NOT_FOUND) {
			countText = countText.substring(nextDelimiter + delimiter.length(), countText.length());
			countElements(countText, delimiter);
		} return ++count;
	}
}