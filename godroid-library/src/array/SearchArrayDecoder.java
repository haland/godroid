package array;

/**
 * 
 * SearchArrayDecoder encodes a SearchArray to a String containing a flag (command) and every element of the array as
 * well as it decodes the flag and SearchArray from a given String.
 * 
 * @author Chris H�land
 *
 */

public class SearchArrayDecoder {
	/**
	 * Position of the Integer variable in the Object-array within the Object-array retrieved from the SearchArray using the method getList().
	 */
	private static final int VARIABLE = 1;
	
	/**
	 * Position of the String parameter in the Object-array within the Object-array retrieved from the SearchArray using the method getList().
	 */
	private static final int PARAMETER = 0;
	
	/**
	 * Delimiter that separates the already delimited parameter and variable (by SUBDELIM) from the next one.
	 */
	private static final String DELIM = ";";
	
	/**
	 * Delimiter that separates the parameter and the variable (e.g. parameter = i, SUBDELIM = ! and variable = 1 will give the String output: "i!1")
	 */
	private static final String SUBDELIM = "::";
	
	/**
	 * Encodes a flag and a SearchArray to String
	 * @param flag - The command to be sent
	 * @param array - The values to be sent
	 * @return A String containing the flag and values of the array
	 */
	public static String encode(int flag, SearchArray array) {
		String message = String.valueOf(flag);
		
		if (array != null) {
			String[] args = new String[array.size()];
			
			for (int i = 0; i < array.size(); i++) {
				Object[] o = (Object[]) array.getList()[i];
				if ((String) o[PARAMETER] != null)
					args[i] = (String)o[PARAMETER] + SUBDELIM + String.valueOf((Integer)o[VARIABLE]);
			}
			
			for (String arg : args)
				message += DELIM + arg;
		}
		
		return message;
	}
	
	/**
	 * Decodes the flag (command) from a String
	 * @param input - The String containing a flag (and a SearchArray)
	 * @return The flag (command)
	 */
	public static int decode_command(String input) {
		return Integer.valueOf(StringSplitter.split(input, DELIM)[0]);
	}
	
	/**
	 * Decodes the SearchArray from a String
	 * @param input - The String containing a flag (and a SearchArray)
	 * @return The SearchArray
	 */
	public static SearchArray decode_parameters(String input) {
		SearchArray arr = new SearchArray();		
		String[] split = StringSplitter.split(input, DELIM);
		
		if (split.length > 1) {
			for (int i = 1; i < split.length; i++) {
				String[] subsplit = StringSplitter.split(split[i], SUBDELIM);
				if (subsplit.length == 2)
					arr.set(subsplit[0], Integer.parseInt(subsplit[1]));
			}
			
			return arr;
		} else return null;
	}
}
