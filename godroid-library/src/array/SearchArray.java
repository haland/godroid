package array;

import array.exceptions.ParameterNotInArrayException;

/**
 * A PHP inspired ArrayList, letting the user get a Integer value by a String parameter.
 * 
 * @author Chris H�land
 */

public class SearchArray {
	/**
	 * The default capacity of the arrays
	 */
	private static final int DEFAULT_CAPACITY = 10;
	
	/**
	 * Number to return if the parameter is not found
	 */
	private static final int NOT_FOUND = -1;
	
	/**
	 * The parameters to search the array by 
	 */
	private String[] theParams;
	
	/**
	 * The Integer values associated with the parameter
	 */
	private int[] theItems;
	
	/**
	 * The size of the array list
	 */
	private int theSize;

	/**
	 * Creates a SearchArray
	 */
	public SearchArray() {
		theSize = 0;
		theParams = new String[DEFAULT_CAPACITY];
		theItems = new int[DEFAULT_CAPACITY];
	}

	/**
	 * Returns the size of the array list
	 * @return The size of the array list
	 */
	public int size() {
		return theSize;
	}

	/**
	 * Gets the Integer value by requesting it by a String parameter
	 * @param param - The String parameter 
	 * @return The value associated with the parameter
	 */
	public int get(String param) {
		int idx = isParam(param);
		
		if (idx == NOT_FOUND)
			return NOT_FOUND;
		
		return theItems[idx];
	}
	
	/**
	 * Returns a array of array of objects. The inner array is of length 2, containing [0] = parameter and [1] = value. The outer array contains all of the array lists parameters and values
	 * @return Array of array of array lists content
	 */
	public Object[] getList() {
		Object[] list = new Object[theSize];
		for (int i = 0; i < theSize; i++) {
			Object[] o = new Object[2];
				o[0] = theParams[i];
				o[1] = theItems[i];
			list[i] = o;
		} return list;
	}

	/**
	 * Add a value to the array list
	 * @param param - The parameter to associate the value to
	 * @param value - The value to set
	 */
	public void set(String param, int value) {
		int idx = isParam(param);

		ensureCapasity();

		if (idx == NOT_FOUND) {
			theItems[theSize] = value;
			theParams[theSize] = param;
			theSize++;
		} else {
			theItems[idx] = value;
			theParams[idx] = param;
		}		
	}

	/**
	 * Removes a value from the array list
	 * @param param - The parameter associated with a value
	 * @return The value removed
	 */
	public int remove(String param) {
		int idx = isParam(param);
		if (idx == NOT_FOUND) {
			try {
				throw new ParameterNotInArrayException();
			} catch (ParameterNotInArrayException e) {
				return NOT_FOUND;
			}
		}
		
		int itmRemoved = theItems[idx];

		for (int a = idx; a < size() - 1; a++) {
			theItems[a] = theItems[a + 1];
			theParams[a] = theParams[a + 1];
		}

		theSize--;
		return itmRemoved;
	}

	/**
	 * Clears the list
	 */
	public void clear() {
		theSize = 0;
		theParams = new String[DEFAULT_CAPACITY];
		theItems = new int[DEFAULT_CAPACITY];
	}

	/**
	 * Extend the arrays length
	 */
	private void ensureCapasity() {
		if (theItems.length == size()) {
			int[] tempI = theItems;
			String[] tempS = theParams;
			theItems = new int[(theItems.length * 2) + 1];
			theParams = new String[(theParams.length * 2) + 1];
			for (int a = 0; a < size(); a++) {
				theItems[a] = tempI[a];
				theParams[a] = tempS[a];
			}
		}
	}
	
	/**
	 * Determines if a parameter already exists
	 * @param param - The parameter to check
	 * @return If a parameter already exists
	 */
	private int isParam(String param) {
		for (int i = 0; i < theSize; i++)
			if (theParams[i] != null && theParams[i].equals(param))
				return i;
		return NOT_FOUND;
	}
}
