package array.exceptions;

/**
 * Exception to cast when a parameter is not found in the SearchArray.
 * 
 * @author Chris H�land
 *
 */

public class ParameterNotInArrayException extends Exception {
	private static final long serialVersionUID = -6960315919308996439L;

	public ParameterNotInArrayException() {
		super();
	}
}
