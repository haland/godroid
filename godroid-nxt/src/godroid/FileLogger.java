package godroid;

import godroid.bluetooth.MessageHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;

import lejos.nxt.Sound;
import standard.Flags;

/**
 * Class that writes logs to file
 * @author Eirik �. Heskja
 */

public class FileLogger implements Thread.UncaughtExceptionHandler{

	/**
	 * The file logger
	 */
	private static FileLogger instance;
	
	/**
	 * The uncaught exception handler
	 */
	private UncaughtExceptionHandler defaultHandler;
	
	/**
	 * Creates the object
	 */
	private FileLogger(){
		defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
	}
	
	/**
	 * Writes log to file
	 * @param t
	 */
	public static void Log(Throwable t){
		getInstance().log(t);
	}
	
	/**
	 * Writes log to file
	 * @param t
	 */
	private void log(Throwable t){
			String timestamp = System.currentTimeMillis()+"";
		    File log = new File(timestamp+".log");
		    try {
		    	log.createNewFile();
		    	writeStackTrace(log, t);
			} catch (IOException e) {} 
		    Sound.beep();
	}

	/**
	 * Writes the stack strace
	 * @param f
	 * @param t
	 * @return
	 * @throws IOException
	 */
	private File writeStackTrace(File f, Throwable t) throws IOException{
		FileOutputStream os = new FileOutputStream(f);
		PrintStream s = new PrintStream(os);
		t.printStackTrace(s);
		s.close();
		os.close();
		return f;
	}

	/**
	 * What to do if a uncaught exception appears
	 */
	public void uncaughtException(Thread t, Throwable e) {
		Log(e);
		MessageHandler.getInstance().putMessage(Flags.MSG_ERROR, null);
		defaultHandler.uncaughtException(t, e);
	}

	/**
	 * Gets a instance of the class
	 * @return
	 */
	public static FileLogger getInstance() {
		if (instance == null)
			instance = new FileLogger();
		return instance;
	}


}
