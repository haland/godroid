package godroid;

import godroid.bluetooth.MessageHandler;
import godroid.detection.interfaces.DetectorInterface;
import godroid.drive.interfaces.TransformedInterface;
import standard.Flags;
import standard.Parameters;
import standard.Values;
import standard.Version;
import array.SearchArray;

/**
 * Class that notifies the Android device when changes on the NXT is made
 * @author Chris H�land & Eirik �. Heskja
 */

public class NotifyAndroid implements TransformedInterface, DetectorInterface {
	/**
	 * The array sent
	 */
	private SearchArray array;
	
	/**
	 * Whether to send a warning message to the Android unit about driving to close to a object,
	 * or the signal strenght is to low
	 */
	private boolean warnDist, warnSign ;

	/**
	 * Creates the notifier
	 */
	public NotifyAndroid() {
		warnDist = warnSign = false;
		array = new SearchArray();
	}

	/**
	 * Sends a transformation complete message to the Android, when transformation has complete 
	 */
	public void hasTransformed(String type) {
		array = new SearchArray();
		array.set(Parameters.TYPE, getSourceAsInt(type));
		MessageHandler.getInstance().putMessage(Flags.MSG_SEGWAY, array);
	}

	/**
	 * Converts the NXTs transformation flags into the universal transformation flags.
	 * @param source - Transformation type
	 * @return The converted transformation type
	 */
	private int getSourceAsInt(String source){
		if (source.equals(Parameters.TRANSFORMATION_SEGWAY_COMPLETE))
			return Values.SEGWAY_UP;
		if (source.equals(Parameters.TRANSFORMATION_TRICYCLE_COMPLETE))
			return Values.SEGWAY_DOWN;
		return Values.SEGWAY_INTERUPT;
	}

	/**
	 * Tells the class if it is supposed to send a distance to object warning in the next message sent
	 */
	public void obstacleDetected(int length) {
		if (length <= Values.WARNING_DISTANCE && !warnDist)
			sendDistance(true);
		else if (length >= Values.WARNING_DISTANCE && warnDist)
			sendDistance(false);
	}

	/**
	 * Sends the distance
	 * @param tooClose
	 */
	private void sendDistance(boolean tooClose) {
		array = new SearchArray();
		int flag = (tooClose) ? Values.BOOLEAN_TRUE : Values.BOOLEAN_FALSE;
		array.set(Parameters.DISTANCE, flag);
		MessageHandler.getInstance().putMessage(Flags.MSG_DISTANCE, array);
		warnDist = tooClose;
	}

	/**
	 * Sends the version of the NXTs program
	 */
	public void sendVersion() {
		array = new SearchArray();
		array.set(Parameters.VERSION, Version.VERSION);
		MessageHandler.getInstance().putMessage(Flags.MSG_VERSION, array);
	}

	/**
	 * Sends the battery level
	 * @param battery Level of the battery
	 */
	public void sendBattery(int battery) {
		array = new SearchArray();
		array.set(Parameters.BATTERY, battery);
		if (battery <= Values.WARNING_BATTERY )
			MessageHandler.getInstance().putMessage(Flags.MSG_BATTERY, array);
		else if  (battery > Values.WARNING_BATTERY)
			MessageHandler.getInstance().putMessage(Flags.MSG_BATTERY, array);

	}

	/**
	 * Sends the signal strength
	 * @param signal The signal strength
	 */
	public void sendBluetooth(int signal) {
		array = new SearchArray();
		array.set(Parameters.SIGNAL, signal);
		if (signal <= Values.WARNING_SIGNAL && !warnSign){
			MessageHandler.getInstance().putMessage(Flags.MSG_SIGNAL, array);
			warnSign = true;
		}
		else if (signal >= Values.WARNING_SIGNAL && warnSign){
			MessageHandler.getInstance().putMessage(Flags.MSG_SIGNAL, array);
			warnSign = false;
		}
	}
}
