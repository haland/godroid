package godroid.drive;

import godroid.bluetooth.interfaces.DisconnectMessageInterface;
import godroid.bluetooth.interfaces.DriveMessageInterface;
import godroid.bluetooth.interfaces.TransformationMessageInterface;
import godroid.drive.interfaces.DriveInterface;
import godroid.drive.interfaces.TransformedInterface;
import godroid.drive.motors.controllers.DriveController;
import godroid.drive.motors.controllers.SegwayController;
import godroid.settings.Settings;
import standard.Parameters;
import array.SearchArray;

public class Maneuvering implements DriveMessageInterface, DisconnectMessageInterface, TransformationMessageInterface, TransformedInterface {
	/**
	 * Whether the robot is driveable
	 */
	private boolean driveable;
	
	/**
	 * The current drive percent
	 */
	private int currentDriverPercent;
	
	/**
	 * The regular drive controller
	 */
	private DriveController driveController;
	
	/**
	 * The current controller
	 */
	private DriveInterface currentController;
	
	/**
	 * The segway drive controller
	 */
	private SegwayController segwayController;

	/**
	 * Creats the maneuvering object
	 * @param settings - The settings class containing all the settings of the NXT
	 */
	public Maneuvering(Settings settings) {
		driveable = true;
		currentDriverPercent = 0;
		segwayController = SegwayController.getInstance();
		driveController = new DriveController(settings, this);

		currentController = driveController;
	}

	/**
	 * Whether the robot is driveable
	 * @return - Whether the robot is driveable 
	 */
	public boolean isDriveable() {
		return driveable;
	}
	
	/**
	 * Gets the current driving percent
	 * @return The current driving percent
	 */
	public int getCurrentDriverPercent() {
		return currentDriverPercent;
	}

	/**
	 * Gets the direction observer
	 * @return The regular drive controller
	 */
	public DriveController getDistanceObserver() {
		return driveController;
	}

	/**
	 * Makes the current controller drive if robot is driveable
	 * @param percent - Percentage to drive with
	 */
	public void drive(int percent) {
		if (driveable) {
			try {
				checkParameters(percent, -100, 100);
			} catch (IllegalArgumentException e) {
				percent = 0;
			} currentDriverPercent = percent;
			currentController.drive(percent);
		}
	}

	/**
	 * Makes the current controller turn the robot if driveable
	 * @param percent - Percentage to turn
	 */
	public void turn(int percent) {
		if (driveable) {
			try {
				checkParameters(percent, -100, 100);
			} catch (IllegalArgumentException e) {
				percent = 0;
			} currentController.turn(percent);
		}
	}

	/**
	 * Checks if a parameter is between two limits
	 * @param param - The parameter to check
	 * @param lim1 - The higher limit
	 * @param lim2 - The lower limit
	 * @throws IllegalArgumentException - Exception thrown if parameter is not between the limits
	 */
	private void checkParameters(int param, int lim1, int lim2) throws IllegalArgumentException {
		if (param < lim1 || param > lim2)
			throw new IllegalArgumentException();
	}

	/**
	 * Makes the robot drive and turn when a drive message is received
	 */
	public void driveMessageReceived(SearchArray cmds) {
		turn(cmds.get(Parameters.DRIVE_DIRECTION));
		drive(cmds.get(Parameters.DRIVE_VELOCITY));
	}

	/**
	 * Stops the robot when the NXT has been disconnected from the Android
	 */
	public void disconnectMessageReceived() {
		turn(0);
		drive(0);
	}

	/**
	 * Changes the current controller when the robot has transformed
	 */
	public void hasTransformed(String type) {
		if (type.equals(Parameters.TRANSFORMATION_SEGWAY_COMPLETE))
			currentController = segwayController;
		else if (type.equals(Parameters.TRANSFORMATION_TRICYCLE_COMPLETE))
			currentController = driveController;
		driveable = true;
	}

	/**
	 * Stops movement when the robot is about to transform
	 */
	public void transformMessageReceived(SearchArray command) {
		turn(0);
		drive(0);
		
		driveable = false;
	}
}
