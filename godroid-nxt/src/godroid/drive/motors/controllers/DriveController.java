package godroid.drive.motors.controllers;

import godroid.PrimitiveDecoder;
import godroid.detection.interfaces.DetectorInterface;
import godroid.drive.Maneuvering;
import godroid.drive.interfaces.DriveInterface;
import godroid.drive.motors.regular.DriveMotor;
import godroid.settings.Settings;
import standard.Parameters;
import standard.Values;
import array.SearchArray;

/**
 * The class which makes the robot drive and turn while in regular (three wheel) mode
 * @author Chris H�land
 */

public class DriveController implements DriveInterface, DetectorInterface {
	/**
	 * Distance to the object in front of the robot
	 */
	private int obstacleDistance;
	
	/**
	 * The left motor
	 */
	private DriveMotor left;
	
	/**
	 * The right motor
	 */
	private DriveMotor right;
	
	/**
	 * The class that controls which drive mode the robot is in and reports to the representative driving controller.
	 */
	private Maneuvering maneuvering;
	
	/**
	 * Creates the drive controller for regular (three wheel) mode
	 * @param settings - The settings class containing all settings sent from the Android to the NXT
	 * @param maneuvering - The class that controls which drive mode the robot is in and reports to the representative driving controller. 
	 */
	public DriveController(Settings settings, Maneuvering maneuvering) {
		obstacleDistance = 255;
		
		this.left = createMotor(settings.getDriveMotorLeftSettings());
		settings.addDriveMotorLeftSettingsChangeListener(left);
		
		this.right = createMotor(settings.getDriveMotorRightSettings());
		settings.addDriveMotorRightSettingsChangeListener(right);
		
		this.maneuvering = maneuvering;
	}

	/**
	 * Creates a drive motor
	 * @param settings - The settings for the motor
	 * @return The drive motor
	 */
	private DriveMotor createMotor(SearchArray settings) {
		return new DriveMotor(PrimitiveDecoder.decodeMotor(settings.get(Parameters.MOTOR_PORT)),
				PrimitiveDecoder.decodeBoolean(settings.get(Parameters.MOTOR_DIRECTION)));
	}
	
	/**
	 * Reduces the speed of the left motor
	 * @param percent - Percent of reduction (1 - percent/100)
	 */
	private void turnLeft(int percent) {
		left.setTurnRegulation(percent);
	}
	
	/**
	 * Reduces the speed of the right motor
	 * @param percent - Percent of reduction (1 - percent/100)
	 */
	private void turnRight(int percent) {
		right.setTurnRegulation(percent);
	}
	
	/**
	 * Makes the robot turn by regulating the speed of the motors
	 */
	public void turn(int percent) throws IllegalArgumentException {
		if (percent < 0) {
			turnLeft(-percent);
			turnRight(1+percent);
		} else if (percent > 0){
			turnRight(percent);
			turnLeft(1-percent);
		} else {
			turnRight(percent);
			turnLeft(percent);
		}
	}
	
	/**
	 * Makes the robot drive backwards or forwards by decreasing or increasing the motors' speed
	 */
	public void drive(int percent) {
		if (percent > 0 )
			percent *= (obstacleDistance > Values.WARNING_DISTANCE) ? 1 : 
				(1.0f * obstacleDistance) / Values.WARNING_DISTANCE;
		
		left.drive(percent);
		right.drive(percent);
	}
	
	/**
	 * Stops the robot
	 */
	public void stop() {
		left.flt();
		right.flt();
	}

	/**
	 * Makes the robot drive slower the closer it gets to a obstacle
	 */
	public void obstacleDetected(int length) {
		obstacleDistance = length;
		drive(maneuvering.getCurrentDriverPercent());
	}
}
