package godroid.drive.motors.controllers;

import godroid.PrimitiveDecoder;
import godroid.bluetooth.interfaces.DriveMessageInterface;
import godroid.drive.interfaces.DriveInterface;
import godroid.drive.motors.segway.SegwayManeuvering;
import godroid.drive.motors.segway.balancing.interfaces.BalancingInterface;
import godroid.settings.Settings;

import java.util.ArrayList;
import java.util.Iterator;

import lejos.nxt.SensorPort;
import standard.Parameters;
import array.SearchArray;

/**
 * Responsible for starting/stopping balancing, and lets the robot be controlled in two wheel mode by the same drive messages as in three wheel mode.
 * @author Anders Hole
 */
public class SegwayController implements DriveInterface, DriveMessageInterface {

	private static SegwayController uniqueInstance;
	public static final double WHEEL_SIZE = 5.6;
	public static final double TRACK_WIDTH = 14.5;
	final double TURN_RATE = 50;
	private ArrayList<BalancingInterface> observers = new ArrayList<BalancingInterface>();
	private Settings settings;
	private SegwayManeuvering maneuvering;

	public static SegwayController getInstance(){
		if(uniqueInstance == null)
			uniqueInstance = new SegwayController();
		return uniqueInstance;
	}

	public SegwayController(){
		settings = Settings.getInstance();
	}

	/**
	 * Drive the robot with a certain percent(-100, 100), same as drive(int percent).
	 * @param percent
	 */
	public void drive(int percent) {
		setSpeed(percent);
	}

	/**
	 * Drives the robot until percent is set to 0. Percent from -100 to 100. Negative values makes the robot go backwards(Depends on which way the motors is .
	 * @param percent
	 */
	public void setSpeed(int percent) {
		double currentPercent = (percent < 0) ? -percent : percent;
		maneuvering.setTravelSpeed(currentPercent);

		if (currentPercent != 0) {
			if (percent < 0) {	
				maneuvering.backward();
			} else {
				maneuvering.forward();
			}
		} else maneuvering.stop();		
	}

	/**
	 * Turns robot
	 */
	public void turn(int percent) throws IllegalArgumentException {
		try {
			checkParameters(percent, -100, 100);
		} catch (IllegalArgumentException e) {
			percent = 0;
		}
		maneuvering.steer(TURN_RATE, percent*2, true); // Adjustment needed percent [- 200, 200]
	}

	/**
	 * Checks if a parameter is between two limits
	 * @param param - The parameter to check
	 * @param lim1 - The higher limit
	 * @param lim2 - The lower limit
	 * @throws IllegalArgumentException - Exception thrown if parameter is not between the limits
	 */
	private void checkParameters(int param, int lim1, int lim2) throws IllegalArgumentException {
		if (param < lim1 || param > lim2)
			throw new IllegalArgumentException();
	}

	/**
	 * Stops balancing(Segwaymode), the robot stops balancing. Do not call this if robot isn't balancing/has fallen over.
	 */
	public void stopSegwayMode(){
		maneuvering.stopBalancing();
		maneuvering = null;
	}

	/**
	 * Starts balancing(Segwaymode). This will lift the robot and start balacing.
	 */
	public void start() {
		int gyroscope_fall_value = settings.getSegwaySettings().get(
				Parameters.SEGWAY_GYROSCOPE_FALL_VALUE);
		SensorPort gyroPort = PrimitiveDecoder.decodePort(
				settings.getGyroscopeSettings().get(Parameters.GYRO_PORT));
		
		maneuvering = new SegwayManeuvering(gyroPort, TRACK_WIDTH, 
				gyroscope_fall_value);
	}
	/**
	 * @see godroid.bluetooth.interfaces.DriveMessageInterface#driveMessageReceived(array.SearchArray)
	 */
	@Override
	public void driveMessageReceived(SearchArray cmds) {
		turn(cmds.get(Parameters.DRIVE_DIRECTION));
		drive(cmds.get(Parameters.DRIVE_VELOCITY));
	}

	/**
	 * @see godroid.drive.interfaces.DriveInterface#stop()
	 */
	@Override
	public void stop() {
		stopSegwayMode();
	}

	/**
	 * Notifies observers when robot has fallen.
	 */
	public void notifyBalancingFallObservers(){
		Iterator<BalancingInterface> i = observers.iterator();
		while(i.hasNext())
			i.next().robotHasFallen();
	}

	/**
	 * Add observers that gets notified when robot has fallen.
	 * @param observer
	 */
	public void addObserver(BalancingInterface observer) {
		if(!observers.contains(observer))
			observers.add(observer);		
	}

}