package godroid.drive.motors;

import lejos.nxt.NXTRegulatedMotor;

/**
 * Class for the Mindstorm motors (uses the NXTRegulatedMotor)
 * @author Chris H�land
 */

public class RegulatedMotor implements Runnable {
	/**
	 * The voltage of the battery
	 */
	private static final int BATTERY_VOLTAGE = 10;
	
	/**
	 * The radius of the tiers used
	 */
	private static final float DECK_RADIUS = 0.028f;
	
	/**
	 * The maximum speed of the motors
	 */
	protected static final float MAX_SPEED = 100 * BATTERY_VOLTAGE;
	
	
	/**
	 * Used to check if driving forward when using immediate return for rotating
	 */
	private static final int FORWARD = 1;
	
	/**
	 * Used to check if driving backward when using immediate return for rotating
	 */
	private static final int BACKWARD = -1;
	
	
	/**
	 * The thread used when using immediate return
	 */
	private Thread thread;
	
	/**
	 * The degrees to rotate, set for using immediate return
	 */
	private int threadDegree;
	
	/**
	 * The direction to rotate, set for using immediate return
	 */
	private int threadDirection;
	
	/**
	 * The percent used for driving/setting speed of the motor
	 */
	protected float speedPercent;
	
	/**
	 * The motor to use
	 */
	protected NXTRegulatedMotor motor;
	
	/**
	 * Whether the motor is attached the right way
	 */
	private boolean motorAttachedRightWay;
	
	/**
	 * Whether to use immediate return
	 */
	private boolean threadRotation = false;
	
	/**
	 * Creates a regulated motor
	 * @param motor - The NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way 
	 */
	public RegulatedMotor(NXTRegulatedMotor motor, boolean motorAttachedRightWay) {
		this.motor = motor;
		this.speedPercent = 1.0f;
		this.thread = new Thread(this);
		this.motorAttachedRightWay = motorAttachedRightWay;
		
		this.thread.start();
	}
	
	/**
	 * Sets what motor to use
	 * @param motor - The motor
	 */
	public void setMotor(NXTRegulatedMotor motor) {
		this.motor = motor;
	}
	
	/**
	 * Set if the motor is attached the right way 
	 * @param motorAttachedRightWay - Whether the motor is attached the right way 
	 */
	public void setMotorAttachedRightWay(boolean motorAttachedRightWay) {
		this.motorAttachedRightWay = motorAttachedRightWay;
	}
	
	/**
	 * Stops the motor, not keeping it locked
	 */
	public void flt() {
		flt(false);
	}
	
	/**
	 * Stops the motor, not keeping it locked
	 * @param immediateReturn - Whether to return immediately
	 */
	public void flt(boolean immediateReturn) {
		motor.flt(immediateReturn);
	}
	
	/**
	 * Stops the motor
	 */
	public void stop() {
		stop(false);
	}
	
	/**
	 * Stops the motor
	 * @param immediateReturn - Whether to return immediately
	 */
	public void stop(boolean immediateReturn) {
		motor.stop(immediateReturn);
	}
	
	/**
	 * Makes the motor rotate forward, until flt() or stop() is called
	 */
	public void forward() {
		if (motorAttachedRightWay)
			motor.forward();
		else motor.backward();
	}
	
	/**
	 * Makes the motor rotate backward, until flt() or stop() is called
	 */
	public void backward() {
		if (motorAttachedRightWay)
			motor.backward();
		else motor.forward();
	}
	
	/**
	 * Gets the tacho count
	 * @return The tacho count
	 */
	public int getTachoCount() {
		return motor.getTachoCount();
	}
	
	/**
	 * Resets the tacho count
	 */
	public void resetTachoCount() {
		motor.resetTachoCount();
	}
	
	/**
	 * Sets the speed of the motor
	 * @param percent - Speed set to a percent of the max speed
	 */
	public void setSpeed(float percent) {
		motor.setSpeed((MAX_SPEED * percent));
	}
	
	/**
	 * Gets the velocity of the motor (uses the radius of the tiers)
	 * @return The velocity of the motor
	 */
	public float getVelocity() {
		int revolvingSpeed = getRotationSpeed();
		revolvingSpeed = (revolvingSpeed < 0 ) ? -revolvingSpeed : revolvingSpeed;
		
		float metersPerSecond = (float) ((revolvingSpeed * (2*Math.PI*DECK_RADIUS)/360.0f));
		
		int removeDecimals = (int) (metersPerSecond * 100);
		metersPerSecond = removeDecimals / 100.0f;
		
		return metersPerSecond;
	}
	
	/**
	 * Whether the motor is moving
	 * @return Whether the motor is moving
	 */
	public boolean isMoving() {
		return motor.isMoving();
	}
	
	/**
	 * Whether the motor is stalled
	 * @return Whether the motor is stalled
	 */
	public boolean isStalled() {
		return motor.isStalled();
	}
	
	/**
	 * Sets the stall threshold
	 * @param error - The error threshold
	 * @param time - The time that the error threshold needs to be exceeded for.
	 */
	public void setStallThreshold(int error, int time) {
		motor.setStallThreshold(error, time);
	}
	
	/**
	 * Gets the position of the motor
	 * @return The motors position
	 */
	public int getPosition() {
		return checkRotationDirection(motor.getPosition());
	}
	
	/**
	 * Gets the rotation speed of the motor
	 * @return - The motors rotation speed
	 */
	public int getRotationSpeed() {
		return checkRotationDirection(motor.getRotationSpeed());
	}
	
	/**
	 * Cleans up an integer depending on the direction of the motor
	 * @param degree - Degrees to rotate
	 * @return The degrees either the same or its negative counterpart depending on whether the motor is attached
	 * the right way 
	 */
	private int checkRotationDirection(int degree) {
		return (motorAttachedRightWay) ? degree : -degree;	
	}

	/**
	 * Rotates the motor
	 * @param degree - Degrees to rotate
	 */
	public void rotate(int degree) {
		rotate(degree, false);
	}
	
	/**
	 * Rotates the motor
	 * @param degree - Degrees to rotate
	 * @param immediateReturn - Whether to return immediately
	 */
	public void rotate(int degree, boolean immediateReturn) {
		if (immediateReturn) {
			if (degree >= 0)
				threadDirection = FORWARD;
			else threadDirection = BACKWARD;
			threadDegree = degree;
			threadRotation = true;
		} else {
			if (degree >= 0)
				rotateForward(degree);
			else rotateBackward(degree);
		}
	}
	
	/**
	 * Rotates the motor forward
	 * @param degree - Degrees to rotate
	 */
	public void rotateForward(int degree) {
		resetTachoCount();
		while (getPosition() < degree) {
			forward();
		} flt();
	}
	
	/**
	 * Rotates the motor backward
	 * @param degree - Degrees to rotate
	 */
	public void rotateBackward(int degree) {
		resetTachoCount();
		while(getPosition() > degree) {
			backward();
		} flt();
	}
	
	/**
	 * Rotates the motor when immediateReturn is true
	 */
	public void run() {
		while(true) {
			while (threadRotation) {
				switch(threadDirection) {
				case FORWARD:
					rotateForward(threadDegree);
					break;
				case BACKWARD:
					rotateBackward(threadDegree);
					break;
				} threadRotation = false;
			}
		}
	}
}