package godroid.drive.motors.segway;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.TachoMotorPort;

/**
 * @author Anders Hole
 */

public class SegwayMotors  {

	private static SegwayMotors uniqueInstance;
	public static final boolean motorAttachedRightWay = true;
	private NXTMotor left_NXT;
	private NXTMotor right_NXT;

	/**
	 * Returns an instance of the segway motors controller.
	 */
	public static SegwayMotors getInstance(){
		if(uniqueInstance == null)
			uniqueInstance = new SegwayMotors();
		return uniqueInstance;
	}

	/**
	 * Return an instance of the left motor.
	 * @return {@link NXTMotor}
	 */
	public NXTMotor getLeftNXTInstance(){
		if(left_NXT==null){
			if(uniqueInstance==null)
				uniqueInstance = new SegwayMotors();
			if(left_NXT == null)
				left_NXT = new SegwayMotor(MotorPort.A);
		}
		return left_NXT;
	}

	/**
	 * Return an instance of the right motor.
	 * @return {@link NXTMotor}
	 */
	public NXTMotor getRightNXTInstance(){
		if(right_NXT==null){
			if(uniqueInstance==null)
				uniqueInstance = new SegwayMotors();
			if(right_NXT == null)
				right_NXT = new SegwayMotor(MotorPort.C);
		}
		return right_NXT;
	}

	/**
	 * Class that allow the drive motors to be attached both ways.
	 */
	private class SegwayMotor extends NXTMotor {

		/**
		 * Creates the motor.
		 * @param port - TH
		 */
		public SegwayMotor(TachoMotorPort port) {
			super(port);
		}

		/**
		 * Run motor forward or backward depending on which way the motor is attached.
		 */
		public void forward(){
			if(motorAttachedRightWay)
				super.forward();
			else
				super.backward();
		}

		/**
		 * Run motor forward or backward depending on which way the motor is attached.
		 */
		public void backward(){
			if(motorAttachedRightWay)
				super.backward();
			else
				super.forward();
		}

		/**
		 * Returns tacho count from the motor depeding on which way the motor is attached.
		 */
		public int getTachoCount()
		{
			if(motorAttachedRightWay)
				return encoderPort.getTachoCount();
			else
				return -encoderPort.getTachoCount();
		}
	}
}
