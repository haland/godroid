package godroid.drive.motors.segway.balancing.scrapheap.exceptions;

/**
 * Exception thrown when the degrees are out of bounds [-100,100]
 * @author Chris H�land
 */

public class DegreesOutOfBoundsException extends Exception {
	
	/**
	 * Creates the exception
	 */
	public DegreesOutOfBoundsException() {
		super();
	}
}
