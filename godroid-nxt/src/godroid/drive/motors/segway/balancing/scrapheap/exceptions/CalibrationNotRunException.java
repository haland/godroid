package godroid.drive.motors.segway.balancing.scrapheap.exceptions;

/**
 * Exception thrown if the degree motor is not calibrated
 * @author Chris H�land
 */

public class CalibrationNotRunException extends Exception {
	
	/**
	 * Creates the exception
	 */
	public CalibrationNotRunException() {
		super();
	}
}
