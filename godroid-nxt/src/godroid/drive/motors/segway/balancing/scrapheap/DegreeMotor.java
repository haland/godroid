package godroid.drive.motors.segway.balancing.scrapheap;

import godroid.PrimitiveDecoder;
import godroid.drive.motors.segway.balancing.scrapheap.interfaces.CalibratedInterface;
import godroid.settings.Settings;
import godroid.settings.interfaces.DegreeMotorSettingsChangeInterface;

import java.util.ArrayList;

import lejos.nxt.NXTRegulatedMotor;
import standard.Parameters;
import array.SearchArray;

/**
 * The motor that is used for lifting and lowering the robot to and from Segway-mode
 * @author Chris H�land
 */

public class DegreeMotor extends DARMotor implements DegreeMotorSettingsChangeInterface {
	/**
	 * The instance of the degree motor
	 */
	private static DegreeMotor instance;
	
	/**
	 * Creates the motor used for lifting and lowering the robot from Segway-mode
	 * @param motor - The used NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 * @param degreesToMoveFromCalibrationToPointZero - Number of degrees to move from the calibration zero point to the zero point wanted by the user
	 */
	private DegreeMotor(NXTRegulatedMotor motor, boolean motorAttachedRightWay,	
			int degreesToMoveFromCalibrationToPointZero) {

		super(motor, motorAttachedRightWay, 
				degreesToMoveFromCalibrationToPointZero);
		setSpeed(SPEED);
	}
	
	/**
	 * Creates the motor used for lifting and lowering the robot from Segway-mode
	 * @param motor - The used NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 * @param degreesToMoveFromCalibrationToPointZero - Number of degrees to move from the calibration zero point to the zero point wanted by the user
	 * @param calibratedListeners - Listeners for when the motor has calibrated
	 * @param currentAngle - The current angle of the motor
	 * @param maximumAngle - The maximum reachable angle of the motor
	 * @param calibrated - Whether the motor has been calibrated
	 */
	private DegreeMotor(NXTRegulatedMotor motor, boolean motorAttachedRightWay, int degreesToMoveFromCalibrationToPointZero, ArrayList<CalibratedInterface> calibratedListeners, 
			int currentAngle, int maximumAngle, boolean calibrated) {

		super(motor, motorAttachedRightWay, degreesToMoveFromCalibrationToPointZero, 
				calibratedListeners, currentAngle, maximumAngle, calibrated);
		setSpeed(SPEED);
	}
	
	/**
	 * Creates a new instance of the motor when the settings for the motor is changed
	 */
	public void settingsChanged(SearchArray settings) {
		Settings.getInstance().removeDegreeMotorSettingsChangeListener(instance);
		
		int maximumAngle = this.maximumAngle;
		int currentAngle = this.currentAngle;
		boolean calibrated = this.calibrated;
		ArrayList<CalibratedInterface> calibratedListeners = this.calibratedListeners;

		NXTRegulatedMotor motor = PrimitiveDecoder.decodeMotor(settings.get(Parameters.MOTOR_PORT));
		boolean motorAttachedRightWay = PrimitiveDecoder.decodeBoolean(settings.get(Parameters.MOTOR_DIRECTION));
		int degreesToMoveFromCalibrationToPointZero = settings.get(Parameters.TRANSFORMATION_ZERO_DEGREES);

		instance = null;
		getInstance(Settings.getInstance(), motor, motorAttachedRightWay, degreesToMoveFromCalibrationToPointZero, 
				calibratedListeners, currentAngle, maximumAngle, calibrated);
		
		if (calibrated) 
			hasCalibrated();
	}
		
	/**
	 * Get a instance of the motor
	 * @return The motors instance
	 */
	public static DegreeMotor getInstance() {
		if (instance == null) {
			getInstance(Settings.getInstance().getDegreeMotorSettings());
		} return instance;
	}
	
	/**
	 * Get a instance of the motor
	 * @param settings - The settings of the motor
	 * @return The instance of the motor
	 */
	private static DegreeMotor getInstance(SearchArray settings) {
		NXTRegulatedMotor motor = PrimitiveDecoder.decodeMotor(settings.get(Parameters.MOTOR_PORT));
		boolean motorAttachedRightWay = PrimitiveDecoder.decodeBoolean(settings.get(Parameters.MOTOR_DIRECTION));
		int degreesToMoveFromCalibrationToPointZero = settings.get(Parameters.TRANSFORMATION_ZERO_DEGREES);

		return getInstance(Settings.getInstance(), motor, motorAttachedRightWay,
				degreesToMoveFromCalibrationToPointZero);
	}
	
	/**
	 * Get a instance of the motor
	 * @param settings - The settings of the motor
	 * @param motor - The used NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 * @param degreesToMoveFromCalibrationToPointZero - Number of degrees to move from the calibration zero point to the zero point wanted by the user
	 * @return The instance of the motor
	 */
	private static DegreeMotor getInstance(Settings settings, NXTRegulatedMotor motor, boolean motorAttachedRightWay, 
			int degreesToMoveFromCalibrationToPointZero) {

		instance = new DegreeMotor(motor, motorAttachedRightWay, degreesToMoveFromCalibrationToPointZero);

		settings.addDegreeMotorSettingsChangeListener(instance);
		
		return instance;
	}
	
	/**
	 * Get a instance of the motor
	 * @param settings - The settings of the motor
	 * @param motor - The used NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 * @param degreesToMoveFromCalibrationToPointZero - Number of degrees to move from the calibration zero point to the zero point wanted by the user
	 * @param calibratedListeners - Listeners for when the motor has calibrated
	 * @param currentAngle - The current angle of the motor
	 * @param maximumAngle - The maximum reachable angle of the motor
	 * @param calibrated - Whether the motor has been calibrated
	 */
	private static DegreeMotor getInstance(Settings settings, NXTRegulatedMotor motor, boolean motorAttachedRightWay, 
			int degreesToMoveFromCalibrationToPointZero, ArrayList<CalibratedInterface> calibratedListeners, 
			int currentAngle, int maximumAngle, boolean calibrated) {
		
		instance = new DegreeMotor(motor, motorAttachedRightWay, degreesToMoveFromCalibrationToPointZero,
				calibratedListeners, currentAngle, maximumAngle, calibrated);
		
		settings.addDegreeMotorSettingsChangeListener(instance);
		
		return instance;
	}
}