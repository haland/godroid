package godroid.drive.motors.segway.balancing.scrapheap.interfaces;

/**
 * Interface implemented by calibration listeners 
 * @author Chris H�land
 */

public interface CalibratedInterface {
	/**
	 * When the DegreeMotor has calibrated it runs this for all its listeners
	 */
	public void hasCalibrated();
}
