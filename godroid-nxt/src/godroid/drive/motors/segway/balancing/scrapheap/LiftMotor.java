package godroid.drive.motors.segway.balancing.scrapheap;

import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;

/**
 * Responsible for calibrating lift arm, lifting the robot and returning arm to zero position while the robot is balancing. 
 * When balancing is stopped the arm returns to 0.8 of the max lift value to catch the fall.
 * @author Anders Hole
 */
public class LiftMotor {

	private NXTRegulatedMotor motor;
	private final int calibrateSpeed = 50;
	private final int liftSpeedBalancing = 125;
	private int currentDegree, minDegree, balanceDegree, maxDegree;

	/**
	 * Creates the lift motor and calibrates the liftarm.
	 * @param port - Motor port of which the lift motor is connected.
	 */
	public LiftMotor(MotorPort port) {
		if (port.equals(MotorPort.A))
			motor = Motor.A;
		else if (port.equals(MotorPort.B))
			motor = Motor.B;
		else if (port.equals(MotorPort.C))
			motor = Motor.C;
		
		balanceDegree = 500;
		
		initialize();
	}

	/**
	 * Recalibrate the arm/motor, stall threshold is lowered to minimize wear on gears during calibration.
	 */
	private void recalibrateMotor(){
		motor.setStallThreshold(1, 100); // Standard values = 50, 1000
		motor.setSpeed(calibrateSpeed);
		motor.resetTachoCount();
		motor.backward();

		while(motor.isMoving()){
			if(motor.isStalled()){
				motor.flt();
				minDegree = motor.getTachoCount();
				break;
			}
		}
		motor.setStallThreshold(400, 1000);
	}

	/**
	 * Moves motor a predefined degrees.
	 */
	public void moveToBalance(){
		motor.setSpeed(liftSpeedBalancing);
		rotate(balanceDegree, false);
	}

	/**
	 * Moves arm back to zero position
	 */
	public void moveToZero(){
		motor.setStallThreshold(1, 100);
		motor.setSpeed(calibrateSpeed);
		rotate(minDegree-currentDegree,true);
	}
	
	/**
	 * Moves arm to catch the robots fall when balancing is stopped.
	 */
	public void moveArmToStopBalance(){
		rotate((int) (maxDegree*0.8), false);
	}
	
	/**
	 * Rotates the motor and updates the maximum degrees the motors have rotated in one(positive) direction.
	 */
	public void rotate(int angle, boolean immedeatereturn){
		motor.rotate(angle, immedeatereturn);
		currentDegree += angle;
		if(currentDegree>maxDegree)
			maxDegree = currentDegree;
		
	}

	/**
	 * Initializes(calibrates) the motor.
	 */
	public void initialize(){
		recalibrateMotor();
	}

	public void flt() {
		motor.flt();
	}
}