package godroid.drive.motors.segway.balancing.scrapheap;

import godroid.drive.motors.RegulatedMotor;
import godroid.drive.motors.segway.balancing.scrapheap.exceptions.CalibrationNotRunException;
import godroid.drive.motors.segway.balancing.scrapheap.exceptions.DegreesOutOfBoundsException;
import godroid.drive.motors.segway.balancing.scrapheap.interfaces.CalibratedInterface;

import java.util.ArrayList;
import java.util.Iterator;

import lejos.nxt.NXTRegulatedMotor;

/**
 * Super-class for the DegreeMotor
 * @author Chris H�land
 */

public class DARMotor extends RegulatedMotor {
	/**
	 * Illegal value of the maximum angle
	 */
	private static final int ILLEGAL = -1;
	
	/**
	 * The speed of the degree motor
	 */
	protected static final float SPEED = 0.15f;
	
	
	/**
	 * The tacho stall of the motor
	 */
	private static final int TACHO_STALL = 1;
	
	/**
	 * The delay associated with tacho stall and checking if motor is stalled
	 */
	private static final int CALIBRATION_DELAY = 100;

	
	/**
	 * The maximum reachable angle of the motor
	 */
	protected int maximumAngle;
	
	/**
	 * The current angle of the motor
	 */
	protected int currentAngle;
	
	/**
	 * Whether the motor has been calibrated
	 */
	protected boolean calibrated;
	
	/**
	 * Number of degrees to move from the calibration zero point to the zero point wanted by the user
	 */
	protected int degreesToMoveFromCalibrationToPointZero;
	
	/**
	 * Listeners for when the motor has calibrated
	 */
	protected ArrayList<CalibratedInterface> calibratedListeners;

	/**
	 * Creates the motor object
	 * @param motor - The used NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 * @param degreesToMoveFromCalibrationToPointZero - Number of degrees to move from the calibration zero point to the zero point wanted by the user 
	 */
	public DARMotor(NXTRegulatedMotor motor, boolean motorAttachedRightWay, int degreesToMoveFromCalibrationToPointZero) {
		super(motor, motorAttachedRightWay);

		this.currentAngle = 0;
		this.maximumAngle = -1;
		this.calibrated = false;
		this.calibratedListeners = new ArrayList<CalibratedInterface>();
		this.degreesToMoveFromCalibrationToPointZero = degreesToMoveFromCalibrationToPointZero;

		setSpeed(SPEED);
		setStallThreshold(TACHO_STALL, CALIBRATION_DELAY);
	}

	/**
	 * Creates the motor object. Supposed to replace the previous object of the same motor
	 * @param motor - The used NXTRegulatedMotor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 * @param degreesToMoveFromCalibrationToPointZero - Number of degrees to move from the calibration zero point to the zero point wanted by the user
	 * @param calibratedListeners - Listeners for when the motor has calibrated
	 * @param currentAngle - The current angle of the motor
	 * @param maximumAngle - The maximum reachable angle of the motor
	 * @param calibrated - Whether the motor has been calibrated
	 */
	public DARMotor(NXTRegulatedMotor motor, boolean motorAttachedRightWay, int degreesToMoveFromCalibrationToPointZero, 
			ArrayList<CalibratedInterface> calibratedListeners, int currentAngle, int maximumAngle, boolean calibrated) {

		super(motor, motorAttachedRightWay);

		this.currentAngle = 0;
		this.calibrated = calibrated;
		this.currentAngle = currentAngle;
		this.maximumAngle = maximumAngle;
		this.calibratedListeners = calibratedListeners;
		this.degreesToMoveFromCalibrationToPointZero = degreesToMoveFromCalibrationToPointZero;

		setSpeed(SPEED);
		setStallThreshold(TACHO_STALL, CALIBRATION_DELAY);
	}

	/**
	 * Calibrates the motor
	 */
	public void calibrate() {
		while(!isStalled()) {  
			backward();
		} flt();
		
		super.rotate(degreesToMoveFromCalibrationToPointZero);
		
		calibrated = true;
		hasCalibrated();
	}
	
	/**
	 * Moves the motor to a given degree
	 * @param degree - The degree to rotate to between 0 and maximumAngle
	 */
	public void move(int degree) {
		move(degree, false);
	}

	/**
	 * Moves the motor to a given degree
	 * @param degree - The degree to rotate to between 0 and maximumAngle
	 * @param immediateReturn - Whether to return from the method immediately
	 */
	public void move(int degree, boolean immediateReturn) {
		try {
			checkCalibrated();
			checkDegreesInBounds(degree);

			if (degree < currentAngle)
				super.rotate(-(currentAngle-degree), immediateReturn);
			else super.rotate(degree-currentAngle, immediateReturn);

			currentAngle = degree;
		} catch (DegreesOutOfBoundsException e) {
			//-- Can not rotate to degree (it's beyond maximumAngle or 0) --\\
		} catch (CalibrationNotRunException e) {
			calibrate();
		}
	}
	
	/**
	 * Moves the motor to support the robot when falling down from Segway-mode
	 */
	public void moveToSupport() {
		if (maximumAngle != ILLEGAL)
			move((int) (maximumAngle * 0.8f));
	}
	
	/**
	 * Rotates the motor a given amount of degrees
	 * @param degree - Degrees to rotate
	 */
	public void rotate(int degree) {
		rotate(degree, false);
	}
	
	/**
	 * Rotates the motor a given amount of degrees and increases the maximumAngle
	 * @param degree - Degrees to rotate
	 * @param immediateReturn - Whether to return from the method immediately 
	 */
	public void rotate(int degree, boolean immediateReturn) {
		maximumAngle = (currentAngle += degree);
		super.rotate(degree, immediateReturn);
	}

	/**
	 * Notifies listeners when the motor has calibrated
	 */
	protected void hasCalibrated() {
		Iterator<CalibratedInterface> i = calibratedListeners.iterator();
		while (i.hasNext())
			i.next().hasCalibrated();
	}

	/**
	 * Adds a calibration listener
	 * @param l - The calibration listener
	 */
	public void addCalibratedListener(CalibratedInterface l) {
		if (!calibratedListeners.contains(l))
			calibratedListeners.add(l);
	}

	/**
	 * Removes a calibration listener
	 * @param l - The calibration listener
	 */
	public void removeCalibratedListener(CalibratedInterface l) {
		if (calibratedListeners.contains(l))
			calibratedListeners.remove(l);
	}

	/**
	 * Check if the motor is calibrated
	 * @throws CalibrationNotRunException - Exception thrown if the motor is not calibrated
	 */
	private void checkCalibrated() throws CalibrationNotRunException {
		if (!calibrated) 
			throw new CalibrationNotRunException();
	}

	/**
	 * Checks if the degrees are between 0 and maximumAngle
	 * @param degree - degrees to rotate
	 * @throws DegreesOutOfBoundsException - Exception thrown if the degrees are out of bounds
	 */
	private void checkDegreesInBounds(int degree) throws DegreesOutOfBoundsException {
		if (maximumAngle != ILLEGAL) {
			if (degree > maximumAngle || degree < 0)
				throw new DegreesOutOfBoundsException();
		} else {
			if (degree < 0)
				throw new DegreesOutOfBoundsException();
		}
	}
}