package godroid.drive.motors.segway.balancing;

import godroid.drive.motors.controllers.SegwayController;
import godroid.drive.motors.segway.SegwayMotors;
import godroid.drive.motors.segway.balancing.scrapheap.LiftMotor;
import lejos.nxt.NXTMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.GyroSensor;
import lejos.util.Delay;


/**
 * @author LeJOS
 * @author Anders Hole
 */
public class Balance extends Thread {

	protected boolean balancing;

	// Motors and gyro:
	protected GyroSensor gyro; 
	protected NXTMotor left_motor;
	protected NXTMotor right_motor;

	/** 
	 * Loop wait time.  WAIT_TIME is the time in ms passed to the Wait command.
	 * NOTE: Balance control loop only takes 1.128 MS in leJOS NXJ. 
	 */
	private static final int WAIT_TIME = 7; // originally 8

	// PI:
	private static final double KGYROANGLE = 7.7;
	private static final double KGYROSPEED = 1.15; // Speed of corrections
	private static final double KPOS = 0.07; // Controls that the robot stands in one point and does not drift forwards/backwards
	private static final double KSPEED = 0.12; // Makes the robot more stable/does not tip over so easy

	/**
	 * This constant aids in drive control. When the robot starts moving because of user control,
	 * this constant helps get the robot leaning in the right direction.  Similarly, it helps 
	 * bring robot to a stop when stopping.
	 */
	private static final double KDRIVE = -0.02;

	long tMotorPosOK;
	long cLoop;
	int power;


	/**
	 * Power differential used for steering based on difference of target steering and actual motor difference.
	 */
	private static double KSTEER = 0.25;

	/**
	 * Gyro offset control
	 * The gyro sensor will drift with time.  This constant is used in a simple long term averaging
	 * to adjust for this drift. Every time through the loop, the current gyro sensor value is
	 * averaged into the gyro offset weighted according to this constant.
	 */
	private static final double EMAOFFSET = 0.0005;

	/** 
	 * If robot power is saturated (over +/- 100) for over this time limit then 
	 * robot must have fallen.  In milliseconds.
	 */
	private static final double TIME_FALL_LIMIT = 1000; // originally 1000


	/**
	 * This constant is in degrees/second for maximum speed.  Note that position 
	 * and speed are measured as the sum of the two motors, in other words, 600 
	 * would actually be 300 degrees/second for each motor.
	 */
	private static final double CONTROL_SPEED  = 600.0;


	/**
	 * motorControlDrive is the target speed for the sum of the two motors
	 * in degrees per second.
	 */
	private double motorControlDrive = 0.0;

	/**
	 * motorControlSteer is the target change in difference for two motors
	 * in degrees per second.
	 */
	private double motorControlSteer = 0.0;

	/**
	 * This global contains the target motor differential, essentially, which 
	 * way the robot should be pointing.  This value is updated every time through 
	 * the balance loop based on motorControlSteer.
	 */
	private double motorDiffTarget = 0.0;

	/**
	 * Time that robot first starts to balance.  Used to calculate tInterval.
	 */
	private long tCalcStart;

	/**
	 * tInterval is the time, in seconds, for each iteration of the balance loop.
	 */
	private double tInterval;

	/**
	 * ratioWheel stores the relative wheel size compared to a standard NXT 1.0 wheel.
	 * RCX 2.0 wheel has ratio of 0.7 while large RCX wheel is 1.4.
	 */
	private double ratioWheel;

	// Gyro globals
	private double gOffset;
	private double gAngleGlobal = 0;
	private int fallLimit;

	// Motor globals
	private double motorPos;
	protected long mrcSum, mrcSumPrev;
	private long motorDiff;
	protected long mrcDeltaP3;
	protected long mrcDeltaP2;
	protected long mrcDeltaP1;

	private double wheelDiameter = SegwayController.WHEEL_SIZE;

	/**
	 * Responsible for lifting and balancing of the Segway, gyroscope is calibrated and the Segway lifted into a near vertical position.
	 * Then the angle is slowly increased to the point that the Segway starts falling before the self-balancing thread starts.
	 * Wheel diameter is used in balancing equations.
	 *  
	 *  <li>NXT 1.0 wheels = 5.6 cm
	 *  <li>NXT 2.0 wheels = 4.32 cm
	 *  <li>RCX "motorcycle" wheels = 8.16 cm
	 * 
	 * @param motorPort The motorport for the {@link LiftMotor}
	 * @param gyroPort The {@link SensorPort} the gyroscope is connected to.
	 * @param gyroscopeFallValue The value of which the gyroscope detects as a fall. This is a negative value.
	 */

//	private LiftMotor motor;
	private SegwayLiftMotor motor;
	public Balance(SensorPort gyroPort, int gyroscopeFallValue) {
		balancing = true;

		right_motor = SegwayMotors.getInstance().getRightNXTInstance();
		left_motor = SegwayMotors.getInstance().getLeftNXTInstance();

		gyro = new GyroSensor(gyroPort);
		motor = new SegwayLiftMotor();

		this.ratioWheel = wheelDiameter/5.6; // Original algorithm was tuned for 5.6 cm NXT 1.0 wheels.
		fallLimit = gyroscopeFallValue;
		// Took out 50 ms delay here.

		// Get the initial gyro offset
		getGyroOffset();

		start();
	}

	/**
	 * Number of offset samples to average when calculating gyro offset.
	 */
	private static final int OFFSET_SAMPLES = 100;

	/**
	 * This function returns a suitable initial gyro offset.  It takes
	 * 100 gyro samples over a time of 1/2 second and averages them to
	 * get the offset.  It also check the max and min during that time
	 * and if the difference is larger than one it rejects the data and
	 * gets another set of samples.
	 */
	protected void getGyroOffset() {
		double gSum;
		int  i, gMin, gMax, g;

		// Ensure that the motor controller is active since this affects the gyro values.
		left_motor.stop(); //These methods don't do it for some reason.
		right_motor.stop(); // .flt();

		do {
			gSum = 0.0;
			gMin = 1000;
			gMax = -1000;
			for (i=0; i<OFFSET_SAMPLES; i++) {
				g = gyro.readValue();

				if (g > gMax)
					gMax = g;
				if (g < gMin)
					gMin = g;

				gSum += g;
				try { Thread.sleep(5);
				} catch (InterruptedException e) {}
			}
		} while ((gMax - gMin) > 1);   // Reject and sample again if range too large

		//Average the sum of the samples.
		gOffset = gSum / OFFSET_SAMPLES + 1.0;

		// Even with motor controller active, the initial offset appears to
		// be off from the actual needed offset to keep robot from wondering.
		// This +1 helps keep robot from wondering when it first starts to
		// balance. NOTE: Maybe running motors @ low power will improve it. -BB
	}

	/**
	 * Get the data from the gyro. 
	 * Fills the pass by reference gyroSpeed and gyroAngle based on updated information from the Gyro Sensor.
	 * Maintains an automatically adjusted gyro offset as well as the integrated gyro angle.
	 * 
	 */
	private void updateGyroData() {
		float gyroRaw;


		gyroRaw = gyro.readValue();
		gOffset = EMAOFFSET * gyroRaw + (1-EMAOFFSET) * gOffset;
		gyroSpeed = gyroRaw - gOffset; // Angular velocity (degrees/sec)

		gAngleGlobal += gyroSpeed*tInterval;
		gyroAngle = gAngleGlobal; // Absolute angle (degrees)
	}

	/**
	 * Keeps track of wheel position with both motors.
	 */
	private void updateMotorData() {
		long mrcLeft, mrcRight, mrcDelta;

		// Keep track of motor position and speed
		mrcLeft = left_motor.getTachoCount();
		mrcRight = right_motor.getTachoCount();

		// Maintain previous mrcSum so that delta can be calculated and get
		// new mrcSum and Diff values
		mrcSumPrev = mrcSum;
		mrcSum = mrcLeft + mrcRight;
		motorDiff = mrcLeft - mrcRight;

		// mrcDetla is the change int sum of the motor encoders, update
		// motorPos based on this detla
		mrcDelta = mrcSum - mrcSumPrev;
		motorPos += mrcDelta;

		// motorSpeed is based on the average of the last four delta's.
		motorSpeed = (mrcDelta+mrcDeltaP1+mrcDeltaP2+mrcDeltaP3)/(4*tInterval);

		// Shift the latest mrcDelta into the previous three saved delta values
		mrcDeltaP3 = mrcDeltaP2;
		mrcDeltaP2 = mrcDeltaP1;
		mrcDeltaP1 = mrcDelta;
	}

	/** 
	 * Global variables used to control the amount of power to apply to each wheel.
	 * Updated by the steerControl() method.
	 */
	private int powerLeft, powerRight; // originally local variables

	/**
	 * This function determines the left and right motor power that should
	 * be used based on the balance power and the steering control.
	 */
	private void steerControl(int power) {
		int powerSteer;

		// Update the target motor difference based on the user steering
		// control value.
		motorDiffTarget += motorControlSteer * tInterval;

		// Determine the proportionate power differential to be used based
		// on the difference between the target motor difference and the
		// actual motor difference.
		powerSteer = (int)(KSTEER * (motorDiffTarget - motorDiff));

		// Apply the power steering value with the main power value to
		// get the left and right power values.
		powerLeft = power + powerSteer;
		powerRight = power - powerSteer;

		// Limit the power to motor power range -100 to 100
		if (powerLeft > 100)   powerLeft = 100;
		if (powerLeft < -100)  powerLeft = -100;

		// Limit the power to motor power range -100 to 100
		if (powerRight > 100)  powerRight = 100;
		if (powerRight < -100) powerRight = -100;
	}

	/**
	 * Calculate the interval time from one iteration of the loop to the next.
	 * Note that first time through, cLoop is 0, and has not gone through
	 * the body of the loop yet.  Use it to save the start time.
	 * After the first iteration, take the average time and convert it to
	 * seconds for use as interval time.
	 */
	private void calcInterval(long cLoop) {
		if (cLoop == 0) {
			// First time through, set an initial tInterval time and
			// record start time
			tInterval = 0.0055;
			tCalcStart = System.currentTimeMillis();
		} else {
			// Take average of number of times through the loop and
			// use for interval time.
			tInterval = (System.currentTimeMillis() - tCalcStart)/(cLoop*1000.0);
		}
	}

	private double gyroSpeed, gyroAngle; // originally local variables
	private double motorSpeed; // originally local variable


	/* 
	 * This is the main balance thread for the robot.
	 *
	 * Robot is assumed to start leaning on a wall.  The first thing it
	 * does is take multiple samples of the gyro sensor to establish and
	 * initial gyro offset.
	 *
	 * After an initial gyro offset is established, the robot backs up
	 * against the wall until it falls forward, when it detects the
	 * forward fall, it start the balance loop.
	 *
	 * The main state variables are:
	 * gyroAngle  This is the angle of the robot, it is the results of
	 *            integrating on the gyro value.
	 *            Units: degrees
	 * gyroSpeed  The value from the Gyro Sensor after offset subtracted
	 *            Units: degrees/second
	 * motorPos   This is the motor position used for balancing.
	 *            Note that this variable has two sources of input:
	 *             Change in motor position based on the sum of
	 *             MotorRotationCount of the two motors,
	 *            and,
	 *             forced movement based on user driving the robot.
	 *            Units: degrees (sum of the two motors)
	 * motorSpeed This is the speed of the wheels of the robot based on the
	 *            motor encoders.
	 *            Units: degrees/second (sum of the two motors)
	 *
	 * From these state variables, the power to the motors is determined
	 * by this linear equation:
	 *     power = KGYROSPEED * gyro +
	 *             KGYROANGLE * gyroAngle +
	 *             KPOS       * motorPos +
	 *             KSPEED     * motorSpeed;
	 *
	 */
	
	/**
	 * Moves the robot to an upright position with a motor/arm, then slowly moves the robot past the balance point and starts balancing.
	 */
	@Override
	public void run() {
		setPriority(MAX_PRIORITY);
		power = 0;

		cLoop = 0;

		tMotorPosOK = System.currentTimeMillis();

		// Reset the motors to make sure we start at a zero position
		left_motor.resetTachoCount();
		right_motor.resetTachoCount();

		int gyroStill = 0;

		int count = 0;
		int gyroAvg = 0, gyroPrev1 = 0,gyroPrev2 = 0 ,gyroPrev3 = 0;
		boolean tryingToBalance = true;


		motor.toSegway();
		while(tryingToBalance){
			count = 0;
			motor.rotate(3);
			while(count<20 && tryingToBalance){
				gyroStill = ((int) (gyro.readValue() - gOffset));
				gyroAvg = (gyroStill + gyroPrev1 + gyroPrev2 + gyroPrev3) / 4;
				if(gyroAvg < fallLimit){ // Segtester: 20
					motor.rotate(-150, true);
					tryingToBalance = false;
					break;
				}
				gyroPrev3 = gyroPrev2;
				gyroPrev2 = gyroPrev1;
				gyroPrev1 = gyroStill;
				Delay.msDelay(10);
				count++;
			}
		}

		// NOTE: This balance control loop only takes 1.128 MS to execute each loop in leJOS NXJ.
		while(true){
			if(balancing) {
				if(cLoop == 300){
//					setPriority(NORM_PRIORITY);
					motor.toRegular(true);
				}
				calcInterval(cLoop++);

				updateGyroData();

				updateMotorData();

				// Apply the drive control value to the motor position to get robot to move.
				motorPos -= motorControlDrive * tInterval;

				// This is the main balancing equation
				power = (int)((KGYROSPEED * gyroSpeed +               // Deg/Sec from Gyro sensor
						KGYROANGLE * gyroAngle) / ratioWheel + // Deg from integral of gyro
						KPOS       * motorPos +                 // From MotorRotaionCount of both motors
						KDRIVE     * motorControlDrive +        // To improve start/stop performance
						KSPEED     * motorSpeed);                // Motor speed in Deg/Sec


				if (Math.abs(power) < 100)
					tMotorPosOK = System.currentTimeMillis();

				// Apply the power values to the motors
				// NOTE: It would be easier/faster to use MotorPort.controlMotorById(), but it needs to be public.

				steerControl(power); // Movement control. Not used for balancing.
				left_motor.setPower(Math.abs(powerLeft));
				right_motor.setPower(Math.abs(powerRight));

				if(powerLeft > 0) left_motor.forward(); 
				else left_motor.backward();

				if(powerRight > 0) right_motor.forward();
				else right_motor.backward();

				// Check if robot has fallen by detecting that motorPos is being limited
				// for an extended amount of time.
				if ((System.currentTimeMillis() - tMotorPosOK) > TIME_FALL_LIMIT){
					SegwayController.getInstance().notifyBalancingFallObservers();
					break;
				}

				try {Thread.sleep(WAIT_TIME);} catch (InterruptedException e) {}
			} else break;
		} // end of while() loop
		
		left_motor.backward();
		right_motor.backward();
		
		Delay.msDelay(300);
		
		left_motor.flt();
		right_motor.flt();

	} // END OF BALANCING THREAD CODE

	/**
	 * This method allows the robot to move forward/backward and make in-spot rotations as
	 * well as arcs by varying the power to each wheel. This method does not actually 
	 * apply direct power to the wheels. Control is filtered through to each wheel, allowing the robot to 
	 * drive forward/backward and make turns. Higher values are faster. Negative values cause the wheel
	 * to rotate backwards. Values between -200 and 200 are good. If values are too high it can make the
	 * robot balance unstable.
	 * 
	 * @param left_wheel The relative control power to the left wheel. -200 to 200 are good numbers.
	 * @param right_wheel The relative control power to the right wheel. -200 to 200 are good numbers.
	 */

	public void wheelDriver(int left_wheel, int right_wheel) {
		// Set control Drive and Steer.  Both these values are in motor degree/second
		motorControlDrive = (left_wheel + right_wheel) * CONTROL_SPEED / 200.0;
		motorControlSteer = (left_wheel - right_wheel) * CONTROL_SPEED / 200.0;
	}

}