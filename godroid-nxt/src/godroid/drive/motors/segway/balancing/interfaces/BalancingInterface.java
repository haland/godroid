package godroid.drive.motors.segway.balancing.interfaces;

/**
 * Interface used to notify observers if the robot falls during balancing
 */
public interface BalancingInterface {

	/**
	 * Notify observers when robot has fallen
	 */
	public void robotHasFallen();
}
