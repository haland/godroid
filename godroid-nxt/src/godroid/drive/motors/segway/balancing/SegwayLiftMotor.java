package godroid.drive.motors.segway.balancing;

import godroid.PrimitiveDecoder;
import godroid.settings.Settings;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.addon.AccelHTSensor;
import standard.Parameters;

public class SegwayLiftMotor implements Runnable {
	/**
	 * The speed the motor is to use when calibrating the lift arm
	 */
	private static final int CALIBRATION_SPEED = 50;
	
	/**
	 * The speed the motor is to use when moving up to get ready to balance
	 */
	private static final int BALANCING_SPEED = 125;
	
	
	/**
	 * The tread used for immediate return when running the method toRegular(); 
	 */
	private Thread thread;
	
	/**
	 * Whether toRegular() is supposed to be run in the calling thread, or this class' thread
	 */
	private boolean immediateReturn;
	
	/**
	 * The motor that is used as the lift motor
	 */
	private NXTRegulatedMotor motor;
	
	/**
	 * Whether the motor is attached the right way
	 */
	private boolean attachedRightWay;
	
	/**
	 * The accelerometer
	 */
	private AccelHTSensor accelerometer;
	
	/**
	 * The value of which the motor stops moving upwards when the accelerometers value is less 
	 * than. The value is read of the y-axis. 
	 */
	private int accelerometerReachValue;
	
	/**
	 * Creates a object of the SegwayLiftMotor.
	 */
	public SegwayLiftMotor() {
		immediateReturn = false;
		Settings settings = Settings.getInstance();
		
		this.motor = PrimitiveDecoder.decodeMotor(
				settings.getDegreeMotorSettings().get(Parameters.MOTOR_PORT));
		this.attachedRightWay = PrimitiveDecoder.decodeBoolean(
				settings.getDegreeMotorSettings().get(Parameters.MOTOR_DIRECTION));
		this.accelerometer = new AccelHTSensor(PrimitiveDecoder.decodePort(
				settings.getAccelerometerSettings().get(Parameters.ACCELEROMETER_PORT)));
		this.accelerometerReachValue = settings.getSegwaySettings().get(
				Parameters.SEGWAY_ACCELEROMETER_REACH_VALUE);
		
		thread = new Thread(this);
		thread.start();
	}
	
	/**
	 * Calibrates the arm, then moves the robot a bit up, stopping it before it reaches
	 * maximum angle. 
	 */
	public synchronized void toSegway() {
		toRegular();
		
		motor.setStallThreshold(400, 1000);
		motor.setSpeed(BALANCING_SPEED);
		
		forward();
		while(motor.isMoving()) {
			if (Math.abs(accelerometer.getYAccel()) < accelerometerReachValue) {
				motor.stop();
				break;
			}
		}
		
		motor.setSpeed(CALIBRATION_SPEED);
	}
	
	/**
	 * Returns the motor to its calibration position
	 */
	public synchronized void toRegular() {
		toRegular(false);
	}
	
	/**
	 * Returns the motor to its calibration position 
	 * @param immediateReturn Whether the method is supposed to return immediately
	 */
	public synchronized void toRegular(boolean immediateReturn) {
		if (!immediateReturn) {
			motor.setStallThreshold(1, 80);
			motor.setSpeed(CALIBRATION_SPEED);
			
			backward();
			while(motor.isMoving()){
				if(motor.isStalled()){
					motor.flt();
					break;
				}
			}
		} 
		
		this.immediateReturn = immediateReturn;
	}
	
	/**
	 * Rotates the motor
	 * @param degrees Degrees to rotate
	 */
	public void rotate(int degrees) {
		rotate(degrees, false);
	}
	
	/**
	 * Rotates the motor
	 * @param degrees Degrees to rotate
	 * @param immediateReturn Whether the method is supposed to return immediately
	 */
	public void rotate(int degrees, boolean immediateReturn) {
		if (!attachedRightWay) 
			degrees = -degrees;
		motor.rotate(degrees, immediateReturn);
	}
	
	/**
	 * Makes the motor drive forward
	 */
	private void forward() {
		if (attachedRightWay)
			motor.forward();
		else motor.backward();
	}
	
	/**
	 * Makes the motor drive backward
	 */
	private void backward() {
		if (attachedRightWay)
			motor.backward();
		else motor.forward();
	}

	/**
	 * The threads method
	 */
	public void run() {
		while(true) {
			if (immediateReturn) {
				immediateReturn = false;
				toRegular(immediateReturn);
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// Do nothing
			}
		}
	}
}
