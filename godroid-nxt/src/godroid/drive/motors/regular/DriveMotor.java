package godroid.drive.motors.regular;

import godroid.PrimitiveDecoder;
import godroid.drive.motors.RegulatedMotor;
import godroid.settings.interfaces.DriveMotorLeftSettingsChangeInterface;
import godroid.settings.interfaces.DriveMotorRightSettingsChangeInterface;
import lejos.nxt.NXTRegulatedMotor;
import standard.Parameters;
import array.SearchArray;

/**
 * Class that creates a drive motor 
 * @author Chris H�land
 */

public class DriveMotor extends RegulatedMotor implements DriveMotorLeftSettingsChangeInterface, 
																DriveMotorRightSettingsChangeInterface {
	/**
	 * The percentage of the robots speed the motor is supposed to drive with. Changes when the robot
	 * is turning.
	 */
	private float turnRegulation;
	
	/**
	 * The current percentage of the robots max speed
	 */
	private float currentPercent;
	
	/**
	 * Creates a drive motor
	 * @param motor - The motor
	 * @param motorAttachedRightWay - Whether the motor is attached the right way
	 */
	public DriveMotor(NXTRegulatedMotor motor, boolean motorAttachedRightWay) {
		super(motor, motorAttachedRightWay);
		this.turnRegulation = 1.0f;
	}

	/**
	 * Sets the turn regulation 
	 * @param percent The percentage
	 */
	public void setTurnRegulation(int percent) {
		this.turnRegulation = 1 - percent/100.0f;
	}
	
	/**
	 * Sets the speed of the motor using the percent and turnRegulation
	 * @param percent The percent
	 */
	public void drive(int percent) {
		currentPercent = (percent < 0) ? -percent/100.0f : percent/100.0f;
		super.setSpeed(currentPercent * turnRegulation);
	
		if (currentPercent != 0) {
			if (percent < 0)
				backward();
			else forward();
		} else flt(true);		
	}
	
	/**
	 * Changes the motors settings if new settings are received
	 */
	public void settingsChanged(SearchArray settings) {
		setMotor(PrimitiveDecoder.decodeMotor(settings.get(Parameters.MOTOR_PORT)));
		setMotorAttachedRightWay(PrimitiveDecoder.decodeBoolean(settings.get(Parameters.MOTOR_DIRECTION)));
	}
}
