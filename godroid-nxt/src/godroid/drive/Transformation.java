package godroid.drive;

import godroid.bluetooth.interfaces.DisconnectMessageInterface;
import godroid.bluetooth.interfaces.TransformationMessageInterface;
import godroid.drive.interfaces.TransformedInterface;
import godroid.drive.motors.controllers.SegwayController;
import godroid.drive.motors.segway.balancing.SegwayLiftMotor;
import godroid.drive.motors.segway.balancing.interfaces.BalancingInterface;

import java.util.ArrayList;
import java.util.Iterator;

import standard.Parameters;
import standard.Values;
import array.SearchArray;

public class Transformation implements TransformationMessageInterface, DisconnectMessageInterface, BalancingInterface {
	/**
	 * The segway controller
	 */
	private SegwayController segway;
	
	/**
	 * The transformation listeners
	 */
	private ArrayList<TransformedInterface> listeners;

	/**
	 * Creates the transformation object
	 */
	public Transformation() {
		this.segway = SegwayController.getInstance();
		this.listeners = new ArrayList<TransformedInterface>();
		
		segway.addObserver(this);
	}

	/**
	 * Transforms the robot to segway mode
	 */
	public void segway() {
		segway.start();
		hasTransformed(Parameters.TRANSFORMATION_SEGWAY_COMPLETE);
	}

	/**
	 * Transforms the robot to regular mode
	 */
	public void tricycle() {
		segway.stop();
		hasTransformed(Parameters.TRANSFORMATION_TRICYCLE_COMPLETE);
	}

	/**
	 * Notifies the transformation listeners when the robot has transformed
	 * @param cmd - The transformation type 
	 */
	private void hasTransformed(String cmd) {
		Iterator<TransformedInterface> i = listeners.iterator();
		while (i.hasNext())
			i.next().hasTransformed(cmd);
	}

	/**
	 * Adds a transformation listener
	 * @param l - The transformation listener
	 */
	public void addTranformedChangeListener(TransformedInterface l) {
		if (!listeners.contains(l))
			listeners.add(l);
	}

	/**
	 * Removes a transformation listener
	 * @param l - The transformation listener
	 */
	public void removeTransformedChangeListener(TransformedInterface l) {
		if (listeners.contains(l))
			listeners.remove(l);
	}

	/**
	 * Transforms the robot when a transformation message has been received
	 */
	public void transformMessageReceived(SearchArray cmds) {
		int mode = cmds.get(Parameters.TYPE);
		switch (mode) {
		case Values.SEGWAY_DOWN:
			segway();
			break;
		case Values.SEGWAY_UP:
			tricycle();
			break;
		}
	}

	/**
	 * Rotates the middle motor to zero when the NXT looses connection to the Android
	 */
	public void disconnectMessageReceived() {
		SegwayLiftMotor motor = new SegwayLiftMotor();
		motor.toRegular(false);
	}

	/**
	 * Informs the Android and maneuvering class that the robot has fallen
	 */
	public void robotHasFallen() {
		segway.stop();
		hasTransformed(Parameters.TRANSFORMATION_TRICYCLE_COMPLETE);
	}
}