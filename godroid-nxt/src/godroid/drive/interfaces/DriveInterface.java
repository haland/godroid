package godroid.drive.interfaces;

/**
 * Interface for driving
 * @author Chris H�land 
 */

public interface DriveInterface {
	/**
	 * When robot is to stop
	 */
	public void stop();
	
	/**
	 * When robot is to turn
	 * @param percent Percentage to reduce the speed of on of the wheels
	 */
	public void turn(int percent);
	
	/**
	 * When robot is to drive
	 * @param percent Percentage of the max speed the robot is to drive with
	 */
	public void drive(int percent);
}
