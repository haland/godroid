package godroid.drive.interfaces;

/**
 * Listener inferface for when the robot has transformed
 * @author Chris H�land
 */

public interface TransformedInterface {
	/**
	 * Run when the robot has transformed
	 * @param e - The transformed event
	 */
	public void hasTransformed(String type);
}
