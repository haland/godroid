package godroid;

import lejos.nxt.Motor;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import standard.Values;

/**
 * Decodes primitives sent over bluetooth in to objects used by the NXT (motors, motor ports, sensor ports and boolean values)
 * @author Chris H�land
 */

public class PrimitiveDecoder {

	/**
	 * Decodes an integer into a NXTRegulatedMotor
	 * @param port - Integer value representing the motors port
	 * @return The NXTRegulatedMotor represented by the integer
	 */
	public static NXTRegulatedMotor decodeMotor(int port) {
		switch(port) {
			case Values.MOTOR_PORT_A:
				return Motor.A;
			case Values.MOTOR_PORT_B:
				return Motor.B;
			case Values.MOTOR_PORT_C:
				return Motor.C;
			default:
				return null;
		}
	}
	
	/**
	 * Decodes an integer into a MotorPort
	 * @param port - Integer value representing the motor port
	 * @return The MotorPort represented by the integer
	 */
	public static MotorPort decodeMotorPort(int port) {
		switch(port) {
			case Values.MOTOR_PORT_A:
				return MotorPort.A;
			case Values.MOTOR_PORT_B:
				return MotorPort.B;
			case Values.MOTOR_PORT_C:
				return MotorPort.C;
			default:
				return null;
		}
	}

	/**
	 * Decodes an integer into a boolean
	 * @param bool - Integer value representing the boolean
	 * @return The boolean represented by the integer
	 */
	public static boolean decodeBoolean(int bool) {
		switch(bool) {
			case Values.BOOLEAN_TRUE:
				return true;
			case Values.BOOLEAN_FALSE:
				return false;
			default:
				return true;
		}
	}

	/**
	 * Decodes an integer into a SensorPort
	 * @param port - Integer value representing the SensorPort
	 * @return The SensorPort represented by the integer
	 */
	public static SensorPort decodePort(int port){
		switch(port){
			case Values.SENSOR_PORT_1: 
				return SensorPort.S1;
			case Values.SENSOR_PORT_2: 
				return SensorPort.S2;
			case Values.SENSOR_PORT_3: 
				return SensorPort.S3;
			case Values.SENSOR_PORT_4: 
				return SensorPort.S4;
			default: return null;
		}
	}
}
