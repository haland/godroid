package godroid.bluetooth;


import godroid.bluetooth.communication.BluetoothConnection;
import godroid.settings.Settings;

import java.util.ArrayList;

import lejos.nxt.comm.BTConnection;
import standard.Flags;
import standard.exception.NoFlagException;
import array.SearchArray;
import array.SearchArrayDecoder;
import concurrent.Semaphore;

/**
 * Class used to handle any Bluetooth message from the Android device.
 * @author Eirik Heskja
 *
 */
public class MessageHandler extends NotificationHandler{
	/**
	 * The {@link BluetoothConnection} used to communicate.
	 */
	private BluetoothConnection bluetooth;
	
	/**
	 * {@link Semaphore} used to block any threads that want to run before settings has been received.
	 */
	private Semaphore startupSemaphore;
	
	/**
	 * {@link Semaphore} used to block any output threads that want to run when there is no settings.
	 */
	private Semaphore messageSemaphore;
	
	/**
	 * The {@link ArrayList} of strings waiting to be sent.
	 */
	private ArrayList<String> messageBuffer;
	
	/**
	 * Private instance of this class.
	 */
	private static MessageHandler instance;
	
	/**
	 * Public method to get the {@link MessageHandler} object.
	 * @return a instance of this class.
	 */
	public static MessageHandler getInstance(){
		if (instance == null)
			instance = new MessageHandler();
		return instance;
	}
	
	/**
	 * Default constructor. Instantiates any object used in this class.
	 */
	private MessageHandler(){
		super();
		startupSemaphore = new Semaphore();
		messageSemaphore = new Semaphore();
		messageBuffer = new ArrayList<String>();
	}
	
	/**
	 * Method to put any message in the que.
	 * @param flag The flag representing the type of the message
	 * @param params The parameters used in the communication.
	 */
	public void putMessage(int flag, SearchArray params){
		try {
			Flags.isFlag(flag);
			putInQue(flag, params);
			messageSemaphore.release();
		} catch (NoFlagException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to encode any message and put it in the que as a string.
	 * @param flag The flag representing the type of the message.
	 * @param params The paramets used in the communication. Will be encoded to a string.
	 */
	private void putInQue(int flag, SearchArray params) {
		String obj = SearchArrayDecoder.encode(flag, params);
		messageBuffer.add(obj);
	}
	
	/**
	 * Method to get the latest message from the buffer/que.
	 * Blocking.
	 * @return The string representation of the latest message.
	 */
	public String getNextMessage(){
		String o;
		messageSemaphore.obtain();
		
		o = messageBuffer.remove(messageBuffer.size()-1);
		return o;
	}
	
	/**
	 * Method to receive any incoming message. Will decode the string and send handle it as a {@link SearchArray}.
	 * @param message String representation of the incoming message.
	 */
	public void setIncommingMessage(String message){
		int cmd = SearchArrayDecoder.decode_command(message);
		SearchArray values = SearchArrayDecoder.decode_parameters(message);
		
		handleMessage(cmd, values);
	}

	/**
	 * Used to handle any incoming message according to it's flag.
	 * @param cmd The flag used in the message.
	 * @param values The parameters used in the message.
	 */
	private void handleMessage(int cmd, SearchArray values) {
		switch(cmd){
		case Flags.MSG_DRIVE:
		case Flags.MSG_STOP:
			notifyDriveListeners(values); break;
		case Flags.MSG_SEGWAY: notifySegwayListeners(values); break;
		case Flags.MSG_DISCONNECT: disconnectFromPhone(); break;
		case Flags.MSG_SETTINGS: setSetting(values); break;
		case Flags.MSG_SETTINGS_COMPLETE: 
			startupSemaphore.release();
			putMessage(Flags.MSG_CALIBRATED, null);
			break;
		default: break;
		}
	}

	/**
	 * Used to tell the {@link Settings} class that the settings has arrived.
	 * @param values The parameters representing the settings.
	 */
	private void setSetting(SearchArray values) {
		Settings.getInstance().settingsReceived(values);
	}

	/**
	 * Used to handle disconnection.
	 */
	private void disconnectFromPhone(){
		try {
			bluetooth.disconnect();
		} catch (Exception e) {}
		notifyDisconnectListeners();
	}
	
	/**
	 * Will obtain the startup semaphore. Released when te settings has been received.
	 */
	public void waitForSettingsReceived() {
		startupSemaphore.obtain();
	}

	/**
	 * Will start a new {@link BluetoothConnection}.
	 */
	public void start() {
		bluetooth = new BluetoothConnection();
		bluetooth.start();
	}

	/**
	 * Gets the current {@link BTConnection}.
	 * @return {@link BTConnection}.
	 */
	public BTConnection getBluetooth() {
		return bluetooth.getConnection();
	}
}