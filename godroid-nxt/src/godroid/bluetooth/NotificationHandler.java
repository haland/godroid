package godroid.bluetooth;

import godroid.bluetooth.interfaces.DisconnectMessageInterface;
import godroid.bluetooth.interfaces.DriveMessageInterface;
import godroid.bluetooth.interfaces.TransformationMessageInterface;

import java.util.ArrayList;

import array.SearchArray;
/**
 * Class used to handle Bluetooth messages from the Android device.
 * @author Eirik Heskja
 *
 */
public class NotificationHandler {
	
	/**
	 * List of {@link DriveMessageInterface} listeners.
	 */
	private ArrayList<DriveMessageInterface> driveListeners;
	
	/**
	 * List of {@link DisplayMessageInterface} listeners.
	 */
	
	/**
	 * List of {@link TransformationMessageInterface} listeners.
	 */
	private ArrayList<TransformationMessageInterface> segwayListeners;
	
	/**
	 * List of {@link DisplayMessageInterface} listeners.
	 */
	private ArrayList<DisconnectMessageInterface> disconnectListeners;

	/**
	 * Default constructor. Will instantiate all the listener arrays.
	 */
	public NotificationHandler() {
		driveListeners = new ArrayList<DriveMessageInterface>();
		segwayListeners = new ArrayList<TransformationMessageInterface>();
		disconnectListeners = new ArrayList<DisconnectMessageInterface>();
	}
	
	/**
	 * Used to notify any {@link DisconnectMessageInterface} listeners.
	 */
	protected void notifyDisconnectListeners() {
		for (DisconnectMessageInterface listener : disconnectListeners)
			listener.disconnectMessageReceived();
	}

	/**
	 * Used to add a {@link DriveMessageInterface} listener to the list of listeners.
	 * @param listener The listener to add
	 */
	public void addDriveListener(DriveMessageInterface listener){
		driveListeners.add(listener);
	}
	
	/**
	 * Used to add a {@link TransformationMessageInterface} listener to the list of listeners.
	 * @param listener The listener to add
	 */
	public void addTransformationMessageListener(TransformationMessageInterface segway){
		segwayListeners.add(segway);
	}

	/**
	 * Used to add a  {@link DisconnectMessageInterface} listener to the list of listeners.
	 * @param listener The listener to add
	 */
	/**
	 * Used to add a {@link DisconnectMessageInterface}  listener to the list of listeners.
	 * @param listener The listener to add
	 */
	public void addDisconnectListener(DisconnectMessageInterface listener){
		disconnectListeners.add(listener);
	}
	
	/**
	 * Used to remove a {@link TransformationMessageInterface} listener from the list of listeners.
	 * @param listener The listener to add
	 */
	public void removeTransformationMessageListener(TransformationMessageInterface listener) {
		segwayListeners.remove(listener);
	}
	
	/**
	 * Used to notify any {@link DriveMessageInterface} listeners.
	 */
	protected void notifyDriveListeners(SearchArray cmds){
		for (DriveMessageInterface listener : driveListeners)
			listener.driveMessageReceived(cmds);
	}
	
	/**
	 * Used to notify any {@link TransformationMessageInterface}  listeners.
	 */
	protected void notifySegwayListeners(SearchArray cmds){
		for (TransformationMessageInterface balance : segwayListeners)
			balance.transformMessageReceived(cmds);

	}
}
