package godroid.bluetooth.communication;

import godroid.NotifyAndroid;
import godroid.bluetooth.MessageHandler;
import lejos.nxt.LCD;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import standard.Flags;
/**
 * Class used to maintain a Bluetooth connection to the Android device.
 * @author Eirik Heskja
 *
 */
public class BluetoothConnection implements Runnable {
	/**
	 * Used to write "connected" on the LCD screen.
	 */
	private String connected = "Connected";
	
	/**
	 * Used to write "Please connect" on the LCD screen.
	 */
	private String waiting = "Please connect..";
	
	/**
	 * Used to write "disconnected" on the LCD screen.
	 */
	private String disconnected = "Disconnected";

	/**
	 * The thread doing the creating of new connections.
	 */
	private Thread thread;
	
	/**
	 * Boolean variable telling if the thread is running.
	 */
	private boolean isRunning;
	
	/**
	 * Boolean variable telling of there is a connection.
	 */
	private boolean isConnected;

	/**
	 * {@link BTConnection} used to wait for incoming connections and maintain a connection.
	 */
	private BTConnection connection;

	/**
	 * The thread handling all outgoing communication.
	 */
	private BluetoothOutputThread output;
	
	/**
	 * The thread handling all incoming communication.
	 */
	private BluetoothInputThread input;

	/**
	 * Default constructor. Will create a new thread object.
	 * @param notify The {@link NotifyAndroid} object used to notify the Android device.
	 */
	public BluetoothConnection(){
		isRunning = isConnected = false;
		thread = new Thread(this);
	}

	/**
	 * Will start the thread.
	 */
	public void start(){
		if (thread != null && !isRunning){
			isRunning = true;
			thread.start();
		}
	}

	/**
	 * Blocked wait while waiting for connection. Will write text to the screen and create new objects for input and output.
	 */
	@Override
	public void run() {
		while (isRunning){
			if (!isConnected){
				try{
					LCD.clear();
					LCD.drawString(waiting,0,0);
					LCD.refresh();

					connection = Bluetooth.waitForConnection();
					connection.setIOMode(NXTConnection.RAW);
					isConnected = true;

					LCD.clear();
					LCD.drawString(connected,0,0);
					LCD.refresh();	

					input = new BluetoothInputThread(this);
					output = new BluetoothOutputThread(this);

					input.start();
					output.start();
				}catch (NullPointerException e){
					MessageHandler.getInstance().putMessage(Flags.MSG_DISCONNECT, null);
				}
			}
		}
	}

	/**
	 * Used to stop the output and input threads and reset the {@link Bluetooth} object.
	 * Closes the connection.
	 * @throws Exception Will be thrown if there is no connection.
	 */
	public void disconnect() throws Exception{
		if (isConnected){
			LCD.clear();
			LCD.drawString(disconnected,0,0);
			LCD.refresh();	
			input.stop();
			output.stop();
			isConnected = false;
			connection.close();
			Bluetooth.reset();
		} else
			throw new Exception("You must be connected before you can disconnect");
	}

	/**
	 * Gets the object used to maintain the connection.
	 * @return the used {@link BTConnection}.
	 */
	public BTConnection getConnection() {
		return connection;
	}

	/**
	 * Gets the current thread that handles output.
	 * @return The used {@link BluetoothOutputThread}.
	 */
	public BluetoothOutputThread getOutput(){
		return output;
	}

	/**
	 * Is the device connected?
	 * @return true if connected.
	 */
	public boolean isConnected() {
		return isConnected;
	}

}
