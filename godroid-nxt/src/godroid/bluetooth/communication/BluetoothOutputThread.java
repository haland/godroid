package godroid.bluetooth.communication;

import godroid.bluetooth.MessageHandler;

import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.comm.BTConnection;
/**
 * Class used to send messages to the Android device.
 * @author Eirik Heskja
 *
 */
public class BluetoothOutputThread implements Runnable {
	/**
	 * Variable used  to track the state of the thread.
	 */
	private boolean isRunning, active;
	
	/**
	 * The thread used to send messages.
	 */
	private Thread thread;

	/**
	 * The connection that holds the {@link BTConnection} between the NXT and the Android device.
	 */
	private BluetoothConnection connection;
	
	/**
	 * The {@link DataOutputStream} that does the sending.
	 */
	private DataOutputStream out;

	/**
	 * Default constructor. Will create a new thread object and instantiate the output stream.
	 * @param connection The {@link BluetoothConnection} used between the NXT and the Android device.
	 */
	public BluetoothOutputThread(BluetoothConnection connection) {
		thread = new Thread(this);
		isRunning = false;
		this.connection = connection;
		out = connection.getConnection().openDataOutputStream();
	}

	/**
	 * Starts the thread.
	 */
	public void start() {
		isRunning = true;
		active = true;
		if (!thread.isAlive())
			thread.start();
	}
	
	/**
	 * Blocked waiting thread. Will get the next message from the {@link MessageHandler} and send it to the Android device using the {@link DataOutputStream}.
	 */
	public void run() {
		while (isRunning) {
			while (connection.isConnected() && active) {
				try {
					String output = MessageHandler.getInstance().getNextMessage();
					out.writeUTF(output);
					out.flush();
				} catch (IOException e1) {
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Stopsthe thread and closes the stream.
	 * @throws IOException from {@link DataOutputStream#close()}
	 */
	public void stop() throws IOException {
		active = false;
		isRunning = false;
		out.close();
	}

}
