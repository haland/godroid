package godroid.bluetooth.communication;

import godroid.bluetooth.MessageHandler;

import java.io.DataInputStream;
import java.io.IOException;

import lejos.nxt.comm.BTConnection;
import standard.Flags;
import standard.Parameters;
import standard.Values;
import array.SearchArray;
/**
 * Class used to receive messages to the Android device.
 * @author Eirik Heskja
 *
 */
public class BluetoothInputThread implements Runnable {
	/**
	 * Variable used  to track the state of the thread.
	 */
	private boolean isRunning, active;
	
	/**
	 * The thread used to send messages.
	 */
	private Thread thread;

	/**
	 * The connection that holds the {@link BTConnection} between the NXT and the Android device.
	 */
	private BluetoothConnection connection;
	
	/**
	 * The {@link DataInputStream} that does the receiving.
	 */
	private DataInputStream dis;
	
	/**
	 * Default constructor. Will create a new thread object and instantiate the input stream.
	 * @param connection The {@link BluetoothConnection} used between the NXT and the Android device.
	 */
	public BluetoothInputThread(BluetoothConnection connection) {
		thread = new Thread(this);
		isRunning = false;
		this.connection = connection;
		dis = connection.getConnection().openDataInputStream();
	}

	/**
	 * Starts the thread.
	 */
	public void start() {
		isRunning = true;
		active = true;
		if (!thread.isAlive())
			thread.start();
	}

	/**
	 * Blocked waiting thread. Will get the next message from the Android device and sets it to the {@link MessageHandler}.
	 */
	public void run() {
		while (isRunning) {
			while (connection.isConnected() && active) {
				try {
					MessageHandler.getInstance().setIncommingMessage(dis.readUTF());
					MessageHandler.getInstance().putMessage(Flags.MSG_OK, null);
				} catch (IOException e1) {
					SearchArray params = new SearchArray();
					params.set("status", Values.STATUS_SEND_ERROR);
					params.set(Parameters.STATUS, Values.STATUS_SEND_ERROR);
					MessageHandler.getInstance().putMessage(Flags.MSG_ERROR, params);
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Stops the thread and closes the input stream.
	 * @throws IOException from {@link DataInputStream#close()}.
	 */
	public void stop() throws IOException {
		isRunning = false;
		active = false;
		dis.close();
	}

}
