package godroid.bluetooth.interfaces;

import array.SearchArray;
/**
 * Interface used in Bluetooth communication.
 * @author Eirik Heskja
 *
 */
public interface SettingsReceivedInterface {
	/**
	 * Indicating that any settings has been received.
	 * @param settings The parameters holding the settings.
	 */
	public void settingsReceived(SearchArray settings);
}
