package godroid.bluetooth.interfaces;

import array.SearchArray;
/**
 * Interface used in Bluetooth communication.
 * @author Eirik Heskja
 *
 */
public interface DisplayMessageInterface {
	/**
	 * Indicating that the message should be written to screen.
	 * @param cmds The parameters to write.
	 */
	public void displayMessage(SearchArray cmds);
}
