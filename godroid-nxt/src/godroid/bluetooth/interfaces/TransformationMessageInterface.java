package godroid.bluetooth.interfaces;

import array.SearchArray;
/**
 * Interface used in Bluetooth communication.
 * @author Eirik Heskja
 *
 */
public interface TransformationMessageInterface {
	/**
	 * Indicating that the NXT should do some kind of transformation.
	 * @param cmds The parameters telling the NXT how to transform.
	 */
	public void transformMessageReceived(SearchArray cmds);

}
