package godroid.bluetooth.interfaces;
/**
 * Interface used in Bluetooth communication.
 * @author Eirik Heskja
 *
 */
public interface DisconnectMessageInterface {
	/**
	 * Indicating that the device has received a disconnect message.
	 */
	public void disconnectMessageReceived();
	
}
