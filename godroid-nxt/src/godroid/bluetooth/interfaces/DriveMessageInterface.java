package godroid.bluetooth.interfaces;

import array.SearchArray;
/**
 * Interface used in Bluetooth communication.
 * @author Eirik Heskja
 *
 */
public interface DriveMessageInterface {
	/**
	 * Indicating that the message contains information about how to drive.
	 * @param cmds The speed and direction.
	 */
	public void driveMessageReceived(SearchArray cmds);
}
