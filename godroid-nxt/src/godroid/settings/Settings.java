package godroid.settings;

import godroid.bluetooth.interfaces.SettingsReceivedInterface;
import godroid.settings.interfaces.AccelerometerSettingsChangeInterface;
import godroid.settings.interfaces.DegreeMotorSettingsChangeInterface;
import godroid.settings.interfaces.DriveMotorLeftSettingsChangeInterface;
import godroid.settings.interfaces.DriveMotorRightSettingsChangeInterface;
import godroid.settings.interfaces.GyroscopeSettingsChangeInterface;
import godroid.settings.interfaces.SegwaySettingsChangeInterface;
import godroid.settings.interfaces.UltraSonicSensorSettingsChangeInterface;

import java.util.ArrayList;
import java.util.Iterator;

import standard.Parameters;
import standard.Values;
import array.SearchArray;

/**
 * Contains all the settings used by the NXT
 * @author Chris H�land
 */

public class Settings implements SettingsReceivedInterface {
	/**
	 * The settings instance
	 */
	private static Settings instance;
	
	/**
	 * The segway settings
	 */
	private SearchArray segwaySettings;
	
	/**
	 * the gyroscope settings
	 */
	private SearchArray gyroscopeSettings;
	
	/**
	 * The degree/middle motor settings
	 */
	private SearchArray degreeMotorSettings;
	
	/**
	 * The accelerometer settings
	 */
	private SearchArray accelerometerSettings;
	
	/**
	 * The left drive motor settings
	 */
	private SearchArray driveMotorLeftSettings;
	
	/**
	 * The right drive motor settings
	 */
	private SearchArray driveMotorRightSettings;
	
	/**
	 * The ultrasonic sensor settings
	 */
	private SearchArray ultraSonicSensorSettings;

	/**
	 * The segway settings change listeners
	 */
	private ArrayList<SegwaySettingsChangeInterface> segwayListeners;
	
	/**
	 * The gyroscope settings change listeners
	 */
	private ArrayList<GyroscopeSettingsChangeInterface> gyroscopeListeners;
	
	/**
	 * The degree/middle motor settings change listeners
	 */
	private ArrayList<DegreeMotorSettingsChangeInterface> degreeMotorListeners;
	
	/**
	 * The left drive motor settings change listeners
	 */
	private ArrayList<DriveMotorLeftSettingsChangeInterface> motorLeftListeners;
	
	/**
	 * The right drive motor settings change listeners
	 */
	private ArrayList<DriveMotorRightSettingsChangeInterface> motorRightListeners;
	
	/**
	 * The accelerometer settings change listeners
	 */
	private ArrayList<AccelerometerSettingsChangeInterface> accelerometerListeners;
	
	/**
	 * The ultrasonic sensor settings change listeners
	 */
	private ArrayList<UltraSonicSensorSettingsChangeInterface> ultraSonicSensorListeners;
	
	/**
	 * Settings constructor
	 */
	private Settings() {
		segwayListeners = new ArrayList<SegwaySettingsChangeInterface>();
		gyroscopeListeners = new ArrayList<GyroscopeSettingsChangeInterface>();
		degreeMotorListeners = new ArrayList<DegreeMotorSettingsChangeInterface>();
		motorLeftListeners = new ArrayList<DriveMotorLeftSettingsChangeInterface>();
		motorRightListeners = new ArrayList<DriveMotorRightSettingsChangeInterface>();
		accelerometerListeners = new ArrayList<AccelerometerSettingsChangeInterface>();
		ultraSonicSensorListeners = new ArrayList<UltraSonicSensorSettingsChangeInterface>();
	}

	/**
	 * Gets the segway settings
	 * @return Segway settings
	 */
	public SearchArray getSegwaySettings() {
		return segwaySettings;
	}
	
	/**
	 * Sets the segway settings and notifies the listeners
	 * @param segwaySettings - The new segway settings
	 */
	private void setSegwaySettings(SearchArray segwaySettings) {
		this.segwaySettings = segwaySettings;
		segwayEvent(segwaySettings);
	}
	
	/**
	 * Notifies the segway settings listeners
	 * @param segwaySettings - The new settings
	 */
	private void segwayEvent(SearchArray segwaySettings) {
		Iterator<SegwaySettingsChangeInterface> i = segwayListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(segwaySettings);
	}
	
	/**
	 * Adds a segway settings change listener
	 * @param l - The listener
	 */
	public void addSegwaySettingsChangeListener(SegwaySettingsChangeInterface l) {
		if (!segwayListeners.contains(l))
			segwayListeners.add(l);
	}
	
	/**
	 * Removes a segway settings change listener
	 * @param l - The listener
	 */
	public void removeSegwaySettingsChangeListener(SegwaySettingsChangeInterface l) {
		if (segwayListeners.contains(l))
			segwayListeners.remove(l);
	}
	
	/**
	 * Gets the gyroscope settings
	 * @return Gyroscope settings
	 */
	public SearchArray getGyroscopeSettings() {
		return gyroscopeSettings;
	}
	
	/**
	 * Sets the gyroscope settings and notifies the listeners
	 * @param gyroscopeSettings - The new gyroscope settings
	 */
	private void setGyroscopeSettings(SearchArray gyroscopeSettings) {
		this.gyroscopeSettings = gyroscopeSettings;
		gyroscopeEvent(gyroscopeSettings);
	}
	
	/**
	 * Notifies the gyroscope settings listeners
	 * @param gyroscopeSettings - The new settings
	 */
	private void gyroscopeEvent(SearchArray gyroscopeSettings) {
		Iterator<GyroscopeSettingsChangeInterface> i = gyroscopeListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(gyroscopeSettings);
	}
	
	/**
	 * Adds a gyroscope settings change listener
	 * @param l - The listener
	 */
	public void addGyroscopeSettingsChangeListener(GyroscopeSettingsChangeInterface l) {
		if (!gyroscopeListeners.contains(l))
			gyroscopeListeners.add(l);
	}
	
	/**
	 * Removes a gyroscope settings change listener
	 * @param l - The listener
	 */
	public void removeGyroscopeSettingsChangeListener(GyroscopeSettingsChangeInterface l) {
		if (gyroscopeListeners.contains(l))
			gyroscopeListeners.remove(l);
	}

	/**
	 * Gets the degree/middle motor settings
	 * @return Degree/middle motor settings
	 */
	public SearchArray getDegreeMotorSettings() {
		return degreeMotorSettings;
	}
	
	/**
	 * Sets the degree/middle motor settings and notifies the listeners
	 * @param degreeMotorSettings - The new degree/middle motor settings
	 */
	private void setDegreeMotorSettings(SearchArray degreeMotorSettings) {
		this.degreeMotorSettings = degreeMotorSettings;
		degreeMotorEvent(degreeMotorSettings);
	}
	
	/**
	 * Notifies the degree/middle motor settings listeners
	 * @param degreeMotorSettings - The new settings
	 */
	private void degreeMotorEvent(SearchArray degreeMotorSettings) {
		Iterator<DegreeMotorSettingsChangeInterface> i = degreeMotorListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(degreeMotorSettings);
	}
	
	/**
	 * Adds a degree/middle motor settings change listener
	 * @param l - The listener
	 */
	public void addDegreeMotorSettingsChangeListener(DegreeMotorSettingsChangeInterface l) {
		if (!degreeMotorListeners.contains(l))
			degreeMotorListeners.add(l);
	}
	
	/**
	 * Removes a degree/middle motor settings change listener
	 * @param l - The listener
	 */
	public void removeDegreeMotorSettingsChangeListener(DegreeMotorSettingsChangeInterface l) {
		if (degreeMotorListeners.contains(l))
			degreeMotorListeners.remove(l);
	}
	
	/**
	 * Gets the accelerometer settings
	 * @return Segway settings
	 */
	public SearchArray getAccelerometerSettings() {
		return accelerometerSettings;
	}
	
	/**
	 * Sets the accelerometer settings and notifies the listeners
	 * @param accelerometerSettings - The new accelerometer settings
	 */
	private void setAccelerometerSettings(SearchArray accelerometerSettings) { 
		this.accelerometerSettings = accelerometerSettings;
		accelerometerEvent(accelerometerSettings);
	}
	
	/**
	 * Notifies the accelerometer settings listeners
	 * @param accelerometerSettings - The new settings
	 */
	private void accelerometerEvent(SearchArray accelerometerSettings) {
		Iterator<AccelerometerSettingsChangeInterface> i = accelerometerListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(accelerometerSettings);
	}
	
	/**
	 * Adds a accelerometer settings change listener
	 * @param l - The listener
	 */
	public void addAccelerometerSettingsChangeListener(AccelerometerSettingsChangeInterface l) {
		if (!accelerometerListeners.contains(l))
			accelerometerListeners.add(l);
	}
	
	/**
	 * Removes a accelerometer settings change listener
	 * @param l - The listener
	 */
	public void removeAccelerometerSettingsChangeListener(AccelerometerSettingsChangeInterface l) {
		if (accelerometerListeners.contains(l))
			accelerometerListeners.remove(l);
	}

	/**
	 * Gets the left drive motor settings
	 * @return Left drive motor settings
	 */
	public SearchArray getDriveMotorLeftSettings() {
		return driveMotorLeftSettings;
	}

	/**
	 * Sets the left drive motor settings and notifies the listeners
	 * @param driveMotorLeftSettings - The new left drive motor settings
	 */
	private void setDriveMotorLeftSettings(SearchArray driveMotorLeftSettings) {
		this.driveMotorLeftSettings = driveMotorLeftSettings;
		driveMotorLeftEvent(driveMotorLeftSettings);
	}

	/**
	 * Notifies the left drive motor settings listeners
	 * @param driveMotorLeftSettings - The new settings
	 */
	private void driveMotorLeftEvent(SearchArray driveMotorLeftSettings) {
		Iterator<DriveMotorLeftSettingsChangeInterface> i = motorLeftListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(driveMotorLeftSettings);
	}
	
	/**
	 * Adds a left drive motor settings change listener
	 * @param l - The listener
	 */
	public void addDriveMotorLeftSettingsChangeListener(DriveMotorLeftSettingsChangeInterface l) {
		if (!motorLeftListeners.contains(l))
			motorLeftListeners.add(l);
	}
	
	/**
	 * Removes a left drive motor settings change listener
	 * @param l - The listener
	 */
	public void removeDriveMotorLeftSettingsChangeListener(DriveMotorLeftSettingsChangeInterface l) {
		if (motorLeftListeners.contains(l))
			motorLeftListeners.remove(l);
	}

	/**
	 * Gets the right drive motor settings
	 * @return Right drive motor settings
	 */
	public SearchArray getDriveMotorRightSettings() {
		return driveMotorRightSettings;
	}

	/**
	 * Sets the right drive motor settings and notifies the listeners
	 * @param driveMotorLeftSettings - The new right drive motor settings
	 */
	private void setDriveMotorRightSettings(SearchArray driveMotorRightSettings) {
		this.driveMotorRightSettings = driveMotorRightSettings;
		driveMotorRightEvent(driveMotorRightSettings);
	}

	/**
	 * Notifies the right drive motor settings listeners
	 * @param driveMotorRightSettings - The new settings
	 */
	private void driveMotorRightEvent(SearchArray driveMotorRightSettings) {
		Iterator<DriveMotorRightSettingsChangeInterface> i = motorRightListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(driveMotorRightSettings);
	}
	
	/**
	 * Adds a right drive motor settings change listener
	 * @param l - The listener
	 */
	public void addDriveMotorRightSettingsChangeListener(DriveMotorRightSettingsChangeInterface l) {
		if (!motorRightListeners.contains(l))
			motorRightListeners.add(l);
	}
	
	/**
	 * Removes a right drive motor settings change listener
	 * @param l - The listener
	 */
	public void removeDriveMotorRightSettingsChangeListener(DriveMotorRightSettingsChangeInterface l) {
		if (motorRightListeners.contains(l))
			motorRightListeners.remove(l);
	}
	
	/**
	 * Gets the ultrasonic sensor settings
	 * @return Ultrasonic sensor settings
	 */
	public SearchArray getUltraSonicSensorSettings() {
		return ultraSonicSensorSettings;
	}
	
	/**
	 * Sets the ultrasonic sensor settings and notifies the listeners
	 * @param ultraSonicSensorSettings - The new ultrasonic sensor settings
	 */
	private void setUltraSonicSensorSettings(SearchArray ultraSonicSensorSettings) {
		this.ultraSonicSensorSettings = ultraSonicSensorSettings;
		ultraSonicSensorSettingsEvent(ultraSonicSensorSettings);
	}
	
	/**
	 * Notifies the ultrasonic sensor settings listeners
	 * @param ultraSonicSensorSettings - The new settings
	 */
	private void ultraSonicSensorSettingsEvent(SearchArray ultraSonicSensorSettings) {
		Iterator<UltraSonicSensorSettingsChangeInterface> i = ultraSonicSensorListeners.iterator();
		while(i.hasNext())
			i.next().settingsChanged(ultraSonicSensorSettings);
	}
	
	/**
	 * Adds a ultrasonic sensor settings change listener
	 * @param l - The listener
	 */
	public void addUltraSonicSensorSettingsChangeListener(UltraSonicSensorSettingsChangeInterface l) {
		if (!ultraSonicSensorListeners.contains(l))
			ultraSonicSensorListeners.add(l);
	}
	
	/**
	 * Removes a ultrasonic sensor settings change listener
	 * @param l - The listener
	 */
	public void removeUltraSonicSensorSettingsChangeListener(UltraSonicSensorSettingsChangeInterface l) {
		if (ultraSonicSensorListeners.contains(l))
			ultraSonicSensorListeners.remove(l);
	}

	/**
	 * Updates the incoming setting
	 * @param settings - The setting received from the Android unit
	 */
	public void settingsReceived(SearchArray settings) {
		switch (settings.get(Parameters.TYPE)) {
			case Values.SETTING_SEGWAY:
				setSegwaySettings(settings);
				break;
			case Values.SETTING_GYROSCOPE:
				setGyroscopeSettings(settings);
				break;
			case Values.SETTING_MIDDLE_MOTOR:
				setDegreeMotorSettings(settings);
				break;			
			case Values.SETTING_ACCELEROMETER:
				setAccelerometerSettings(settings);
				break;
			case Values.SETTING_MOTOR_LEFT:
				setDriveMotorLeftSettings(settings);
				break;
			case Values.SETTING_MOTOR_RIGHT:
				setDriveMotorRightSettings(settings);
				break;
			case Values.SETTING_ULTRA_SONIC_SENSOR:
				setUltraSonicSensorSettings(settings);
				break;
			default:
				break;
		}
	}	

	/**
	 * Creates an instance of Settings if it does not already exist and returns it
	 * @return Instance of Settings
	 */
	public static Settings getInstance() {
		if (instance == null)
			instance = new Settings();
		return instance;
	}
}
