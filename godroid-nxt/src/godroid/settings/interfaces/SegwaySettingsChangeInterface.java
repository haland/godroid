package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the segway settings
 * @author Chris H�land
 */

public interface SegwaySettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The Segway settings
	 */
	public void settingsChanged(SearchArray settings);
}
