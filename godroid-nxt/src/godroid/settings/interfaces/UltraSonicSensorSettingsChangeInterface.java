package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the ultrasonic sensor settings
 * @author Chris H�land
 */
public interface UltraSonicSensorSettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The UltraSonicSensor settings
	 */
	public void settingsChanged(SearchArray settings);
}
