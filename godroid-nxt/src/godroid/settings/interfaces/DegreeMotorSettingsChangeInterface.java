package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the degree/middle motor settings
 * @author Chris H�land
 */

public interface DegreeMotorSettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The DegreeMotor settings
	 */
	public void settingsChanged(SearchArray settings);
}
