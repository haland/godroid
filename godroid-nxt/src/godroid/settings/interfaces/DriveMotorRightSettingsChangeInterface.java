package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the right drive motor settings
 * @author Chris H�land
 */

public interface DriveMotorRightSettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The DriveMotorRight settings
	 */
	public void settingsChanged(SearchArray settings);
}
