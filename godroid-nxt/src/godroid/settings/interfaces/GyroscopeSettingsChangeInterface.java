package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the gyroscope settings
 * @author Chris H�land
 */

public interface GyroscopeSettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The Gyroscope settings
	 */
	public void settingsChanged(SearchArray settings);
}
