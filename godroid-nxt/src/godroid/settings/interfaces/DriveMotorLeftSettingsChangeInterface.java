package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the left drive motor settings
 * @author Chris H�land
 */

public interface DriveMotorLeftSettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The DriveMotorLeft settings
	 */
	public void settingsChanged(SearchArray settings);
}
