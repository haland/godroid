package godroid.settings.interfaces;

import array.SearchArray;

/**
 * The interface for listening for changes to the accelerometer settings
 * @author Chris H�land
 */

public interface AccelerometerSettingsChangeInterface {
	
	/**
	 * Method run when the setting changes
	 * @param e - The Accelerometer settings
	 */
	public void settingsChanged(SearchArray settings);
}
