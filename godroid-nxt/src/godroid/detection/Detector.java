package godroid.detection;

import godroid.PrimitiveDecoder;
import godroid.bluetooth.interfaces.TransformationMessageInterface;
import godroid.detection.interfaces.DetectorInterface;
import godroid.drive.interfaces.TransformedInterface;
import godroid.settings.Settings;
import godroid.settings.interfaces.UltraSonicSensorSettingsChangeInterface;

import java.util.ArrayList;
import java.util.Iterator;

import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.util.Delay;
import standard.Parameters;
import standard.Values;
import array.SearchArray;
import concurrent.Semaphore;

/**
 * Detects the distance and nofities observers with the current distance. Runs own thread.
 * @author Anders Hole
 */
public class Detector extends UltrasonicSensor implements Runnable, UltraSonicSensorSettingsChangeInterface, TransformedInterface, TransformationMessageInterface {
	private int distance;
	private Thread thread;
	private boolean running;
	private Semaphore semaphore;
	private boolean waitForCorrectMode;

	private ArrayList<DetectorInterface> observers = new ArrayList<DetectorInterface>();

	/**
	 * Creates the distance detector
	 * @param settings - {@link = Settings} Contains settings for the ultrasonic sensor.
	 * @param port - The sensor port that the ultrasonic sensor(distance detection) is connected to.
	 */
	private Detector(Settings settings, SensorPort port){
		super(port);
		thread = new Thread(this);
		waitForCorrectMode = false;
		semaphore = new Semaphore();
		settings.addUltraSonicSensorSettingsChangeListener(this);
	}

	/**
	 * Creates the distance detector by decoding which sensor port the ultrasonic sensor is attached to, then uses {@link Detector}{@link #Detector(Settings, SensorPort)}.
	 * @param settings - {@link = Settings} contains settings for the Ultrasonic sensor.
	 */
	public Detector(Settings settings){
		this(settings, PrimitiveDecoder.decodePort(settings.getUltraSonicSensorSettings().get(Parameters.DETECTION_PORT)));
	}

	/**
	 * Starts detection thread
	 */
	public void start(){
		if (!thread.isAlive()){
			running = true;
			thread.start();
		}
	}

	/**
	 * Updates distance and runs {@link Detector}{@link #notifyObservers()}
	 */
	private void updateDistance(){
		distance = getDistance();
		notifyObservers();
	}

	/**
	 * Returns a string with distance to an object(if any)
	 */
	public String toString(){
		return distance + " m";
	}

	/**
	 * Notifies observers with the distance
	 */
	private void notifyObservers(){
		Iterator<DetectorInterface> i = observers.iterator();
		while(i.hasNext())
			i.next().obstacleDetected(distance);
	}

	/**
	 * Adds distance observer
	 * @param observer
	 */
	public void addObserver(DetectorInterface observer) {
		if(!observers.contains(observer))
			observers.add(observer);		
	}

	/**
	 * Updates the distance and waits 20ms if the semaphore is available. If the semaphore is unavailable the thread waits for it to become available.
	 */
	public void run() {
		while (running) {
			if (waitForCorrectMode)
				semaphore.obtain();
			updateDistance();
			Delay.msDelay(20);
		}
	}

	/**
	 * Changes the port that the ultrasonic sensor is connected to.
	 */
	public void settingsChanged(SearchArray settings) {
		SensorPort port = PrimitiveDecoder.decodePort(settings.get(Parameters.DETECTION_PORT));
		if (this.port != port)
			this.port = port;
	}

	/**
	 * @see godroid.drive.interfaces.TransformedInterface#hasTransformed(godroid.drive.listeners.TransformedEvent)
	 */
	@Override
	public void hasTransformed(String type) {
		if (type.equals(Parameters.TRANSFORMATION_TRICYCLE_COMPLETE))
			semaphore.release();
	}

	/**
	 * @see godroid.bluetooth.interfaces.TransformationMessageInterface#transformMessageReceived(array.SearchArray)
	 */
	public void transformMessageReceived(SearchArray cmds) {
		int mode = cmds.get(Parameters.TYPE);
		switch (mode) {
		case Values.SEGWAY_DOWN:
			waitForCorrectMode = true;
			break;
		case Values.SEGWAY_UP:
			waitForCorrectMode = false;
			break;
		}
	}
}