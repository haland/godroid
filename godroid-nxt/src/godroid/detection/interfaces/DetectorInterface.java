package godroid.detection.interfaces;

/**
 * Interface used to notify observers with the length of the object(if any) in front of the robot.
 * @author Anders Hole
 */
public interface DetectorInterface {

	/**
	 * Notify observers with the length to the object in front
	 * @param length - Length to the object
	 */
	public void obstacleDetected(int length);
	
}
