package godroid;

import godroid.bluetooth.MessageHandler;
import godroid.detection.Detector;
import godroid.drive.Maneuvering;
import godroid.drive.Transformation;
import godroid.settings.Settings;

public class GOdroid {
	/**
	 * Universal constant used for logging
	 */
	public static final boolean LOG = true;
	
	/**
	 * Universal constant used for debugging
	 */
	public static final boolean DEBUG = true;
	
	/**
	 * Instance of the singleton
	 */
	private static GOdroid instance;
	
	/**
	 * @see godroid.detection.Detector 
	 */
	private Detector detector;
	
	/**
	 * @see godroid.settings.Settings
	 */
	private Settings settings;
	
	/**
	 * @see godroid.bluetooth.MessageHandler
	 */
	private MessageHandler handler;
	
	/**
	 * @see godroid.drive.Maneuvering
	 */
	private Maneuvering maneuvering;
	
	/**
	 * @see godroid.NotifyAndroid
	 */
	private NotifyAndroid androidNotifier;
	
	/**
	 * @see godroid.drive.Transformation
	 */
	private Transformation transformation;
	
	/**
	 * Creates the singleton 
	 */
	private GOdroid() {
		androidNotifier = new NotifyAndroid();
		handler = MessageHandler.getInstance();
		
		handler.start();
		handler.waitForSettingsReceived();
		
		androidNotifier.sendVersion();
		settings = Settings.getInstance();
		
		maneuvering = new Maneuvering(settings);

		detector = new Detector(settings);
		detector.addObserver(androidNotifier);
		detector.addObserver(maneuvering.getDistanceObserver());

		transformation = new Transformation();
		transformation.addTranformedChangeListener(detector);
		transformation.addTranformedChangeListener(maneuvering);
		transformation.addTranformedChangeListener(androidNotifier);
		
		handler.addDriveListener(maneuvering);
		handler.addTransformationMessageListener(detector);
		handler.addTransformationMessageListener(maneuvering);
		handler.addTransformationMessageListener(transformation);
		handler.addDisconnectListener(maneuvering);
		handler.addDisconnectListener(transformation);

		detector.start();
	}

	/**
	 * Gets a instance of the class
	 * @return
	 */
	public static GOdroid getInstance() {
		if (instance == null)
			instance = new GOdroid();
		return instance;
	}
	
	/**
	 * Starts the program
	 * @param args
	 */
	public static void main(String[] args) {
		Thread.setDefaultUncaughtExceptionHandler(FileLogger.getInstance());
		getInstance();
	}
}
